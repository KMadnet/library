import type { NetworkProvider } from 'cashscript';
import type { ElectrumCluster } from 'electrum-cash';

export type ContractVersion = 'AnyHedge v0.12';

/**
 * Constructor parameters for setting up an anyhedge manager.
 */
export type AnyHedgeManagerConfig =
{
	/** authentication token used to authenticate network requests to the settlement service */
	authenticationToken?: string;

	/** string denoting which AnyHedge contract version to use. */
	contractVersion?: ContractVersion;

	/** fully qualified domain name for the settlement service provider. */
	serviceDomain?: string;

	/** network port number for the settlement service provider. */
	servicePort?: number;

	/** network scheme for the settlement service provider, either 'http' or 'https'. */
	serviceScheme?: 'http' | 'https';

	/** electrum cluster to use in BCH network operations. */
	electrumCluster?: ElectrumCluster;

	/** network provider to use for BCH network operations. */
	networkProvider?: NetworkProvider;
};

export interface SimulationOutput
{
	shortPayoutSatsSafe: bigint;
	longPayoutSatsSafe: bigint;
	totalPayoutSatsSafe: bigint;
	satsForNominalUnits: bigint;
	minerFeeSats: bigint;
}
