import type { Recipient, Utxo } from 'cashscript';

export interface UnsignedTransactionProposal
{
	inputs: Utxo[];
	outputs: Recipient[];
	locktime?: number;
}

export type RedemptionDataHex = { [key: string]: string };
export type RedemptionDataBin = { [key: string]: Uint8Array };

export interface SignedTransactionProposal extends UnsignedTransactionProposal
{
	redemptionDataList: RedemptionDataHex[];
}
