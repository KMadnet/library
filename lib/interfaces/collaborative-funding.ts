import type { Utxo } from 'cashscript';
import type { ContractData } from './contract-data.js';

export interface UnsignedFundingProposal
{
	contractData: ContractData;
	utxo: Utxo;
}

export interface SignedFundingProposal extends UnsignedFundingProposal
{
	publicKey: string;
	signature: string;
}
