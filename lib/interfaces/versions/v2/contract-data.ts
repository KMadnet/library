// Set up an enum of valid contract sides.
export type ContractSideV2 = 'short' | 'long';

// Note: For context, derivation and explanation of the AnyHedge numbers and values,
//       please refer to createContract() in anyhedge.ts.

/**
 * Information about service fees.
 */
export type ContractFeeV2 =
{
	/** name or identifier for the fee in form presentable to an end-user. */
	name: string;

	/** description explaining the fee in form presentable to an end-user. */
	description: string;

	/** bitcoin cash address where service fees are to be paid. */
	address: string;

	/** how many satoshis are required to be paid as service fee. */
	satoshis: bigint;
};

/* */
export enum SettlementTypeV2
{
	MATURATION = 'maturation',
	LIQUIDATION = 'liquidation',
	MUTUAL = 'mutual',
}

export interface ContractPayoutV2
{
	settlementType: SettlementTypeV2;
	settlementTransactionHash: string;
	shortPayoutInSatoshis: bigint;
	longPayoutInSatoshis: bigint;
}

export interface ContractAutomatedPayoutV2 extends ContractPayoutV2
{
	/** Oracle message containing the price used to determine the payout values for this settlement.  */
	settlementMessage: string;

	/** Oracle signature for the message containing the price used to determine the payout values for this settlement.  */
	settlementSignature: string;

	/** Oracle message sequentially before the settlement message used to prove that the settlement message is the first valid message to settle with.  */
	previousMessage: string;

	/** Oracle signature for the message used to prove that the settlement message is the first valid message to settle with.  */
	previousSignature: string;

	/** The price in oracle units that this settlement used to determine its payout value, stored for convenience. */
	settlementPrice: bigint;
}

export interface ContractMaturationV2 extends ContractAutomatedPayoutV2
{
	settlementType: SettlementTypeV2.MATURATION;
}

export interface ContractLiquidationV2 extends ContractAutomatedPayoutV2
{
	settlementType: SettlementTypeV2.LIQUIDATION;
}

export interface ContractMutualRedemptionV2 extends ContractPayoutV2
{
	settlementType: SettlementTypeV2.MUTUAL;
}
export type ContractSettlementV2 = ContractMaturationV2 | ContractLiquidationV2 | ContractMutualRedemptionV2;

/**
 * Funding and settlement information relating to an AnyHedge contract.
 */
export interface ContractFundingV2
{
	/** Funding transaction hash used to identify a funding of this contract. */
	fundingTransactionHash: string;

	/** Output in the funding transaction that sends to this contract. */
	fundingOutputIndex: bigint;

	/** Satoshis that is sent in this funding transaction to this contract. */
	fundingSatoshis: bigint;

	/** Settlement information if this funding has been settled on-chain. */
	settlement?: ContractSettlementV2;
}

/**
 * Parameters used directly inside the AnyHedge contract.
 */
export interface ContractParametersV2
{
	/** Public key of the oracle that provide price information to this contract. */
	oraclePublicKey: string;

	/** Lower price at which the contract liquidates at. Effectively defines long leverage. */
	lowLiquidationPrice: bigint;

	/** Upper price at which the contract liquidates at. */
	highLiquidationPrice: bigint;

	/**  Unix timestamps of the earliest data and time the contract can be liquidated. */
	startTimestamp: bigint;

	/** Unix timestamp of the date and time at which the contract matures. */
	maturityTimestamp: bigint;

	/**
	 * Nominal contract value scaled by satoshis-per-bch in order to retain precision of internal calculations.
	 * Note that like other bigint script values, this must never be interpreted as a javascript Number. However, especially
	 * this value must not be misinterpreted as its valid range is far outside of javascript integer precision.
	 * */
	nominalUnitsXSatsPerBch: bigint;

	/** Cost in satoshis for the nominal contract value at the high liquidation price. Effectively defines short leverage. */
	satsForNominalUnitsAtHighLiquidation: bigint;

	/** Total satoshis sent when funding, minus a predetermined miner-fee, used to determine which satoshis to pay out on settlement. */
	payoutSats: bigint;

	/** Payout lockscript used to encumber funds paid out from the contract to the short position. */
	shortLockScript: string;

	/** Payout lockscript used to encumber funds paid out from the contract to the long position. */
	longLockScript: string;

	/** Integer flag that can be used to enable or disable mutual redemption support in the contract. (0 = disabled, not 0 = enabled) */
	enableMutualRedemption: bigint;

	/** Public key for the short positions keypair used for mutual redemption. */
	shortMutualRedeemPublicKey: string;

	/** Public key for the long positions keypair used for mutual redemption. */
	longMutualRedeemPublicKey: string;
}

/**
 * Metadata not directly used by the AnyHedge contract, but used in debugging and in many utility functions that require parameters closer to intent.
 */
export interface ContractMetadataV2
{
	/** Which side of the contract that the initiating party takes on. */
	takerSide: ContractSideV2;

	/** Which side of the contract that the responding party takes on. */
	makerSide: ContractSideV2;

	/** Payout address where the short position of the contract pays out to on settlement. */
	shortPayoutAddress: string;

	/** Payout address where the long position of the contract pays out to on settlement. */
	longPayoutAddress: string;

	/** Oracle message used as the starting point for the contract. */
	startingOracleMessage: string;

	/** Oracle signature for the message used as the starting point for the contract. */
	startingOracleSignature: string;

	/** Oracle price used to determine starting conditions for the contract. */
	startPrice: bigint;

	/** Duration in seconds in relation to the starting timestamp used to determine the contracts maturity time. */
	durationInSeconds: bigint;

	/** Nominal size of the contract, equivalent to the simple hedge value when short leverage = 1. */
	nominalUnits: number;

	/** Multiplier for startPrice that determines the lower liquidation price, and therefor both the leverage and price protection of the contract. */
	lowLiquidationPriceMultiplier: number;

	/** Multiplier for startPrice that determines the upper liquidation price. */
	highLiquidationPriceMultiplier: number;

	/** Integer flag that indicates whether the contract is a simple hedge (short leverage = 1x) (0 = simple, not 0 = leveraged) */
	isSimpleHedge: bigint;

	/**  Initial size of the short position in the contract, in oracle units. */
	shortInputInOracleUnits: number;

	/**  Initial size of the long position in the contract, in oracle units. */
	longInputInOracleUnits: number;

	/**  Initial size of the short position in the contract, in satoshis. */
	shortInputInSatoshis: bigint;

	/**  Initial size of the long position in the contract, in satoshis. */
	longInputInSatoshis: bigint;

	/** Number of satoshis to reserve for settlement miner fees. */
	minerCostInSatoshis: bigint;
}

/**
 * Detailed description of an AnyHedge contract, including all necessary parameters to recreate and validate the contract.
 */
export interface ContractDataV2
{
	/** Contract version, used to identify which contract lockscript is used. */
	version: string;

	/** Contract address, used to identify contract interactions in block explorers and acts as a checksum for contract parameterization. */
	address: string;

	/** Contract fees, used for coordination and follow-up by the various services that helped the contract parties fund and/or redeem contracts. */
	fees: ContractFeeV2[];

	/** Contract parameters, used in the lock and unlock script and defines the configurable parts of the contract behaviour. */
	parameters: ContractParametersV2;

	/** Contract metadata, used to store the original contract intent and environment as well as in many utility functions. */
	metadata: ContractMetadataV2;

	/** Contract funding, used to track all funded instances of the contract. */
	fundings: ContractFundingV2[];
}
