import type { ContractMetadataV2, ContractParametersV2, ContractFundingV2, ContractSideV2 } from './contract-data.js';

/**
 * Parameters required to create an AnyHedge contract.
 */
export interface ContractCreationParametersV2
{
	/** Which side of the contract that the initiating party takes on. */
	takerSide: ContractSideV2;

	/** Which side of the contract that the responding party takes on. */
	makerSide: ContractSideV2;

	/** Intended nominal hedge size of the contract. */
	nominalUnits: number;

	/** Public key of the oracle that provide price information to this contract. */
	oraclePublicKey: string;

	/** Oracle message used as the starting point for the contract. */
	startingOracleMessage: string;

	/** Oracle signature for the message used as the starting point for the contract. */
	startingOracleSignature: string;

	/** Unix timestamp of the date and time at which the contract matures. */
	maturityTimestamp: bigint;

	/** Multiplier for startPrice that determines the lower liquidation price, and therefor both the leverage and price protection of the contract. */
	lowLiquidationPriceMultiplier: number;

	/** Multiplier for startPrice that determines the upper liquidation price. */
	highLiquidationPriceMultiplier: number;

	/** Indicate whether the short position is a simple hedge (short leverage = 1) or if the high liquidation price should be used to establish short leverage > 1. */
	isSimpleHedge: bigint;

	/** Payout address where the short position of the contract pays out to on settlement. */
	shortPayoutAddress: string;

	/** Payout address where the long position of the contract pays out to on settlement. */
	longPayoutAddress: string;

	/** Integer flag that can be used to enable or disable mutual redemption support in the contract. (0 = disabled, 1 = enabled) */
	enableMutualRedemption: bigint;

	/** Public key for the short positions keypair used for mutual redemption. */
	shortMutualRedeemPublicKey: string;

	/** Public key for the long positions keypair used for mutual redemption. */
	longMutualRedeemPublicKey: string;
}

/**
 * Parameters required when registering contracts with a settlement service.
 */
export type ContractRegistrationParametersV2 = ContractCreationParametersV2;

/**
 * Parameters required when validating that a contract address corresponds to the given contract creation parameters.
 */
export interface ContractValidationParametersV2 extends ContractCreationParametersV2
{
	/** Address of the contract to verify parameterization for. */
	contractAddress: string;
}

/**
 * Parameters required to mature or liquidate an AnyHedge contract.
 */
export interface ContractSettlementParametersV2
{
	/** Public key of the oracle that provide price information to this contract. */
	oraclePublicKey: string;

	/** Oracle message that should be used to settle this contract. */
	settlementMessage: string;

	/** Oracle signature for the message that should be used to settle this contract. */
	settlementSignature: string;

	/** Oracle message used to verify that the settlement message is the first possible message to settle with. */
	previousMessage: string;

	/** Oracle signature for the message used to verify that the settlement message is the first possible message to settle with. */
	previousSignature: string;

	/** The specific contract funding for this contract that should be settled. */
	contractFunding: ContractFundingV2;

	/** Contract metadata necessary to calculate the payout satoshis. */
	contractMetadata: ContractMetadataV2;

	/** Contract parameters necessary to execute and instance of the contract. */
	contractParameters: ContractParametersV2;
}

export interface AutomatedPayoutParametersV2
{
	/** Public key of the oracle that provide price information to this contract. */
	oraclePublicKey: string;

	/** Oracle message that should be used to settle this contract. */
	settlementMessage: string;

	/** Oracle signature for the message that should be used to settle this contract. */
	settlementSignature: string;

	/** Oracle message used to verify that the settlement message is the first possible message to settle with. */
	previousMessage: string;

	/** Oracle signature for the message used to verify that the settlement message is the first possible message to settle with. */
	previousSignature: string;

	/**  Number of satoshis to pay out to the short side of the contract. */
	shortPayoutSats: bigint;

	/**  Number of satoshis to pay out to the long side of the contract. */
	longPayoutSats: bigint;

	/** The specific contract funding for this contract that should be settled. */
	contractFunding: ContractFundingV2;

	/** Contract parameters necessary to execute and instance of the contract. */
	contractParameters: ContractParametersV2;

	/** Number of satoshis to use for the miner fee for the payout transaction */
	minerFeeSats: bigint;
}
