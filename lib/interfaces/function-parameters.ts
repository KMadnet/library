import type { AutomatedPayoutParametersV2, ContractCreationParametersV2, ContractRegistrationParametersV2, ContractSettlementParametersV2, ContractValidationParametersV2 } from './versions/v2/index.js';

// Combine the interfaces of the different versions
export type ContractCreationParameters = ContractCreationParametersV2;
export type ContractRegistrationParameters = ContractRegistrationParametersV2;
export type ContractValidationParameters = ContractValidationParametersV2;
export type ContractSettlementParameters = ContractSettlementParametersV2;
export type AutomatedPayoutParameters = AutomatedPayoutParametersV2;
