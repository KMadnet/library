import type { ContractDataV2, ContractMetadataV2, ContractParametersV2, ContractFundingV2, ContractSettlementV2, ContractFeeV2, ContractSideV2, ContractAutomatedPayoutV2, ContractMaturationV2, ContractLiquidationV2 } from './versions/v2/index.js';
import { SettlementTypeV2 } from './versions/v2/index.js';

// Combine the interface of different versions
export type ContractFee = ContractFeeV2;
export type ContractParameters = ContractParametersV2;
export type ContractMetadata = ContractMetadataV2;
export type ContractFunding = ContractFundingV2;
export type ContractSettlement = ContractSettlementV2;
export type ContractData = ContractDataV2;

// ...
export type ContractSide = ContractSideV2;
export type ContractMaturation = ContractMaturationV2;
export type ContractLiquidation = ContractLiquidationV2;
export type ContractAutomatedPayout = ContractAutomatedPayoutV2;

// Re-export the settlement types as an object.
export const SettlementType = SettlementTypeV2;
