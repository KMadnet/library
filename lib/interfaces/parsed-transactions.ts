import type { ContractParameters, ContractSettlement, ContractFunding } from './contract-data.js';
import { SettlementType } from './contract-data.js';

// Parsing a transaction gives us no information about the omitted properties.
export type ParsedFunding = Omit<ContractFunding, 'fundingSatoshis'>;

// Parsing a transaction can contain any of the fields of a ContractSettlement.
// Technically it should always contain settlementType and settlementTransactionHash though.
export type ParsedSettlement = Pick<ContractSettlement, 'settlementTransactionHash' | 'settlementType'> & Partial<ContractSettlement>;

export interface ParsedSettlementData
{
	address: string;
	funding: ParsedFunding;
	settlement: ParsedSettlement;
	parameters: ContractParameters;
}

// Determine if the type of the parsed settlement data is a mutual redemption.
export const isParsedMutualRedemptionData = function(data: ParsedSettlementData): boolean
{
	return data.settlement.settlementType === SettlementType.MUTUAL;
};

// Determine if the type of the parsed settlement data is a payout.
export const isParsedPayoutData = function(data: ParsedSettlementData): boolean
{
	return data.settlement.settlementType === SettlementType.MATURATION || data.settlement.settlementType === SettlementType.LIQUIDATION;
};
