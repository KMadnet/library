/* Import anyhedge related interfaces. */
// NOTE: SettlementType is imported as an object because we need to use SettlementType enum as an object.
import type { ContractMetadata, ContractParameters, ContractFee, SimulationOutput, ContractData, ContractFunding, ContractVersion, UnsignedTransactionProposal, SignedTransactionProposal, ParsedSettlementData, AnyHedgeManagerConfig, ContractRegistrationParameters, ContractCreationParameters, ContractValidationParameters, ContractSettlementParameters, AutomatedPayoutParameters, UnsignedFundingProposal, SignedFundingProposal, ContractAutomatedPayout, ContractMaturation, ContractLiquidation } from './interfaces/index.js';
import { SettlementType } from './interfaces/index.js';
import { binToHex, encodeTransaction, hexToBin, secp256k1, utf8ToBin, sha256 } from '@bitauth/libauth';
import { OracleData } from '@generalprotocols/price-oracle';
import type { Argument, ContractOptions, NetworkProvider, Recipient, Transaction, Utxo } from 'cashscript';
import { Contract, ElectrumNetworkProvider } from 'cashscript';
import type { RequestInit } from 'node-fetch';
import fetch from 'node-fetch';
import { DUST_LIMIT, SATS_PER_BCH, DEFAULT_SERVICE_DOMAIN, DEFAULT_SERVICE_PORT, DEFAULT_SERVICE_SCHEME, DEFAULT_CONTRACT_VERSION, VM_FALSE, VM_1_AS_TRUE } from './constants.js';
import { debug, bigIntMin, bigIntMax } from './util/javascript-util.js';
import { contractCoinToFunding, contractFundingToCoin, buildPayoutTransaction, estimatePayoutTransactionFee, parseSettlementTransaction, contractFundingToOutpoint } from './util/anyhedge-util.js';
import { addressToLockScript, broadcastTransaction, decodeWIF, derivePublicKey, lockScriptToAddress } from './util/bitcoincash-util.js';
import { extractRedemptionDataList, attemptTransactionGeneration, mergeSignedProposals, unlockingDataFromWIF, unlockingDataFromRedemptionData } from './util/mutual-redemption-util.js';
import { AnyHedgeArtifacts } from '@generalprotocols/anyhedge-contracts';
import { MissingAuthenticationTokenError } from './errors.js';
import { calculateRequiredFundingSatoshis, decodeUnlockingBytecodeP2PKH, generateTransactionFromFundingProposal, mergeTransactionInputs, signTransactionP2PKH } from './util/funding-util.js';
import { validateContractExecutionSafetyConstraints } from './util/constraints-validation-util.js';
import equals from 'shallow-equals';

// Load support for bigint in JSON
import { JSONParse, JSONStringify } from './util/json-explicit-bigint.js';

/**
 * Class that manages AnyHedge contract operations, such as creation, validation, and payout of AnyHedge contracts.
 */
// Disable ESLint prefer-default-export rule because 'export default' does not play well with commonjs
// eslint-disable-next-line import/prefer-default-export
export class AnyHedgeManager
{
	contractVersion: ContractVersion;
	serviceScheme: string;
	serviceDomain: string;
	servicePort: number;
	networkProvider: NetworkProvider;
	authenticationToken?: string;

	get serviceUrl(): string
	{
		return `${this.serviceScheme}://${this.serviceDomain}:${this.servicePort}`;
	}

	/**
	 * Initializes an AnyHedge Manager using the specified config options. Note that the `networkProvider` and `electrumCluster`
	 * options are mutually exclusive and the `networkProvider` takes precedence. The default network provider automatically
	 * connects and disconnects between network requests, so if you need a persistent connection, please use a custom provider.
	 *
	 * @param {AnyHedgeManagerConfig} config             config object containing configuration options for the AnyHedge Manager.
	 *
	 * @see {@link https://gitlab.com/GeneralProtocols/anyhedge/contracts|AnyHedge contracts repository} for a list of contract versions.
	 *
	 * @example const anyHedgeManager = new AnyHedgeManager({ authenticationToken: '<token>' });
	 * @example
	 * const config =
	 * {
	 * 	authenticationToken: '<token>',
	 * 	serviceDomain: 'localhost',
	 * 	servicePort: 6572,
	 * 	serviceScheme: 'http',
	 * 	networkProvider: new ElectrumNetworkProvider('mainnet')
	 * };
	 * const anyHedgeManager = new AnyHedgeManager(config);
	 */
	constructor(config: AnyHedgeManagerConfig = {})
	{
		// Extract service URL components from the config object
		this.serviceScheme = config.serviceScheme || DEFAULT_SERVICE_SCHEME;
		this.serviceDomain = config.serviceDomain || DEFAULT_SERVICE_DOMAIN;
		this.servicePort = config.servicePort || DEFAULT_SERVICE_PORT;

		// Store the contract version
		this.contractVersion = config.contractVersion || DEFAULT_CONTRACT_VERSION;

		// Store the authentication token
		this.authenticationToken = config.authenticationToken;

		if(config.networkProvider)
		{
			// Use the provided network provider for BCH network operations if one is provided.
			this.networkProvider = config.networkProvider;
		}
		else
		{
			// If a custom cluster is provided, connection management is handled by the user.
			const manualConnectionManagement = config.electrumCluster !== undefined;

			// Create a new ElectrumNetworkProvider for BCH network operations, optionally using the provided electrum cluster.
			this.networkProvider = new ElectrumNetworkProvider('mainnet', config.electrumCluster, manualConnectionManagement);
		}
	}

	/*
	// External library functions
	*/

	/**
	 * Request an authentication token from the settlement service. This token
	 * is used to authenticate all requests to the settlement services. Only a single
	 * token needs to be generated per consuming application.
	 *
	 * @param name   name to communicate an identity to the settlement service.
	 *
	 * @throws {Error} if the request failed.
	 * @returns a new authentication token
	 */
	async requestAuthenticationToken(name: string): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'requestAuthenticationToken() <=', arguments ]);

		// Request a new authentication token from the settlement service
		const requestParameters: RequestInit =
		{
			method: 'post',
			body: JSONStringify({ name }),
			headers: { 'Content-Type': 'application/json' },
		};
		const requestResponse = await fetch(`${this.serviceUrl}/api/v2/requestToken`, requestParameters);

		// Throw the returned error if the request failed
		if(!requestResponse.ok)
		{
			throw(new Error(await requestResponse.text()));
		}

		// Retrieve the authentication from the response if the request succeeded
		const authenticationToken = await requestResponse.text();

		// Output function result for easier collection of test data.
		debug.result([ 'requestAuthenticationToken() =>', authenticationToken ]);

		return authenticationToken;
	}

	/**
	 * Register an early maturation for a previously registered contract funding.
	 *
	 * @param contractAddress                contract address to submit funding for.
	 * @param settlementMessage              price message used to calculate the early maturation outcome
	 * @param settlementSignature            signature for the price message used for validation
	 * @param takerSettlementFeeInSatoshis   taker fee that should be moved from the taker to the maker side of the payout
	 * @param takerEarlySettlementProposal   signed settlement proposal from the taker
	 * @param makerEarlySettlementProposal   signed settlement proposal from the maker
	 *
	 * @throws {Error} if no authentication token is provided or the authentication token is invalid.
	 * @throws {Error} if the API call failed in any way (e.g. the transaction failed to broadcast).
	 * @returns transaction hash of the transaction that matured the contract early.
	 */
	async submitEarlyMaturation(
		contractAddress: string,
		settlementMessage: string,
		settlementSignature: string,
		takerSettlementFeeInSatoshis: bigint,
		takerEarlySettlementProposal: SignedTransactionProposal,
		makerEarlySettlementProposal: SignedTransactionProposal,
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'submitEarlyMaturation() <=', arguments ]);

		// Check that an authentication token is set
		if(!this.authenticationToken)
		{
			throw(new MissingAuthenticationTokenError());
		}

		// Submit the funding transaction to the automatic settlement service provider.
		const requestParameters =
		{
			method: 'post',
			body: JSONStringify({ contractAddress, settlementMessage, settlementSignature, takerSettlementFeeInSatoshis, takerEarlySettlementProposal, makerEarlySettlementProposal }),
			headers: { 'Content-Type': 'application/json', Authorization: this.authenticationToken },
		};
		const requestResponse = await fetch(`${this.serviceUrl}/api/v2/submitEarlyMaturation`, requestParameters);

		// If the API call was not successful..
		if(!requestResponse.ok)
		{
			// .. pass through the error to the developers.
			throw(new Error(await requestResponse.text()));
		}

		// If no error is returned, the response is the settlement transaction hash.
		const settlementTransactionHash = await requestResponse.text();

		// Output function result for easier collection of test data.
		debug.result([ 'submitEarlyMaturation() =>', settlementTransactionHash ]);

		// Return the settlement transaction hash.
		return settlementTransactionHash;
	}

	/**
	 * Register a new contract for external management.
	 *
	 * @param contractAddress            contract address to submit funding for.
	 * @param transactionHex             funding transaction as a hex-encoded string.
	 * @param [dependencyTransactions]   list of transaction hex strings of transactions that the funding transaction depends on.
	 *
	 * @throws {Error} if no authentication token is provided or the authentication token is invalid.
	 * @throws {Error} if the API call failed in any way (e.g. the transaction failed to broadcast).
	 * @returns funding information for the registered contract.
	 */
	async submitFundingTransaction(
		contractAddress: string,
		transactionHex: string,
		dependencyTransactions?: string[],
	): Promise<ContractFunding>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'submitFundingTransaction() <=', arguments ]);

		// Check that an authentication token is set
		if(!this.authenticationToken)
		{
			throw(new MissingAuthenticationTokenError());
		}

		// Submit the funding transaction to the automatic settlement service provider.
		const requestParameters: RequestInit =
		{
			method: 'post',
			body: JSONStringify({ contractAddress, transactionHex, dependencyTransactions }),
			headers: { 'Content-Type': 'application/json', Authorization: this.authenticationToken },
		};
		const requestResponse = await fetch(`${this.serviceUrl}/api/v2/submitFunding`, requestParameters);

		// If the API call was not successful..
		if(!requestResponse.ok)
		{
			// .. pass through the error to the developers.
			throw(new Error(await requestResponse.text()));
		}

		// If no error is returned, the response can be parsed as JSON.
		const contractFunding = JSONParse(await requestResponse.text()) as ContractFunding;

		// Output function result for easier collection of test data.
		debug.result([ 'submitFundingTransaction() =>', contractFunding ]);

		// Return the funding information.
		return contractFunding;
	}

	/**
	 * Register a new contract for external management.
	 *
	 * @param contractRegistrationParameters   object containing the parameters to register this contract.
	 * @param optionalServiceFees              optional service fees.
	 *
	 * @throws {Error} if no authentication token is provided or the authentication token is invalid.
	 * @returns contract information for the registered contract.
	 */
	async registerContractForSettlement(contractRegistrationParameters: ContractRegistrationParameters, optionalServiceFees: Array<ContractFee> = []): Promise<ContractData>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'registerContractForSettlement() <=', arguments ]);

		// Check that an authentication token is set
		if(!this.authenticationToken)
		{
			throw(new MissingAuthenticationTokenError());
		}

		// Register the contract with the automatic settlement service provider.
		const requestParameters: RequestInit =
		{
			method: 'post',
			body: JSONStringify({ ...contractRegistrationParameters, fees: optionalServiceFees }),
			headers: { 'Content-Type': 'application/json', Authorization: this.authenticationToken },
		};
		const contractResponse = await fetch(`${this.serviceUrl}/api/v2/registerContract`, requestParameters);

		// If the API call was not successful..
		if(!contractResponse.ok)
		{
			// .. pass through the error to the developers.
			throw(new Error(await contractResponse.text()));
		}

		// If no error is returned, the response can be parsed as JSON.
		const contractData = JSONParse(await contractResponse.text()) as ContractData;

		// Write log entry for easier debugging.
		debug.action(`Registered contract '${contractData.address}' with ${this.serviceDomain} for automatic settlement.`);

		// Validate that the contract returned is identical to a contract created locally.
		const contractValidationParameters: ContractValidationParameters =
		{
			contractAddress: contractData.address,
			...contractRegistrationParameters,
		};
		const contractValidity = await this.validateContract(contractValidationParameters);

		// If the contract is invalid..
		if(!contractValidity)
		{
			// Write a log entry explaining the problem and throw the error.
			const errorMsg = `Contract registration for '${contractData.address}' with ${this.serviceDomain} resulted in an invalid contract.`;
			debug.errors(errorMsg);

			throw(new Error(errorMsg));
		}

		// Output function result for easier collection of test data.
		debug.result([ 'registerContractForSettlement() =>', contractData ]);

		// Return the contract information.
		return contractData;
	}

	/**
	 * Request the contract data and status of a contract with the settlement service.
	 *
	 * @param contractAddress address to retrieve status for
	 * @param [privateKeyWIF] private key WIF of one of the contract's parties.
	 *
	 * @throws {Error} if no authentication token is provided or the authentication token is invalid.
	 * @throws {Error} if no private key WIF was provided *and* the authentication token is different than the one used for registration.
	 * @throws {Error} if an invalid WIF was provided.
	 * @throws {Error} if a private key WIF was provided that does not belong to either of the contract parties.
	 * @throws {Error} if no contract is registered at the settlement service for the given address.
	 * @throws {Error} if the API call is unsuccessful.
	 * @returns the contract data and status of the contract
	 */
	async getContractStatus(
		contractAddress: string,
		privateKeyWIF?: string,
	): Promise<ContractData>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'getContractStatus() <=', arguments ]);

		// Check that an authentication token is set.
		if(!this.authenticationToken)
		{
			throw(new MissingAuthenticationTokenError());
		}

		// Initialize empty public key and signature.
		let publicKey = '';
		let signature = '';

		// Generate a public key and signature if a private key WIF was provided.
		if(privateKeyWIF)
		{
			// Decode the private key WIF if it was provided.
			const privateKey = decodeWIF(privateKeyWIF);

			// Derive the corresponding public key.
			publicKey = derivePublicKey(privateKey);

			// Hash the contract address to be signed.
			const messageHash = await sha256.hash(utf8ToBin(contractAddress));

			// Sign the message hash using the provided private key.
			const signatureBin = secp256k1.signMessageHashSchnorr(hexToBin(privateKey), messageHash);
			if(typeof signatureBin === 'string')
			{
				throw new Error(signatureBin);
			}

			// Convert the signature to a hex string.
			signature = binToHex(signatureBin);
		}

		// Request the contract's status from the settlement service
		const queryParameters = `contractAddress=${contractAddress}&signature=${signature}&publicKey=${publicKey}`;
		const requestParameters: RequestInit =
		{
			method: 'get',
			headers: { Authorization: this.authenticationToken },
		};
		const contractResponse = await fetch(`${this.serviceUrl}/api/v2/contractStatus?${queryParameters}`, requestParameters);

		// If the API call was not successful..
		if(!contractResponse.ok)
		{
			// .. pass through the error to the developers.
			throw(new Error(await contractResponse.text()));
		}

		// If no error is returned, the response can be parsed as JSON.
		const receivedContractData = JSONParse(await contractResponse.text()) as ContractData;

		// Verify that the received contract address matches the requested address.
		if(receivedContractData.address !== contractAddress)
		{
			throw(new Error(`Received contract data for contract ${receivedContractData.address} while requesting contract data for contract ${contractAddress}`));
		}

		// Detect if this was created as a simple hedge or not
		const isSimpleHedge = (receivedContractData.parameters.satsForNominalUnitsAtHighLiquidation === BigInt('0') ? VM_1_AS_TRUE : VM_FALSE);

		// Combine the received contracts metadata and parameters to form a valid set of contract creation parameters.
		const creationParameters: ContractCreationParameters = {
			oraclePublicKey: receivedContractData.parameters.oraclePublicKey,
			maturityTimestamp: receivedContractData.parameters.maturityTimestamp,
			enableMutualRedemption: receivedContractData.parameters.enableMutualRedemption,
			shortMutualRedeemPublicKey: receivedContractData.parameters.shortMutualRedeemPublicKey,
			longMutualRedeemPublicKey: receivedContractData.parameters.longMutualRedeemPublicKey,
			takerSide: receivedContractData.metadata.takerSide,
			makerSide: receivedContractData.metadata.makerSide,
			nominalUnits: receivedContractData.metadata.nominalUnits,
			startingOracleMessage: receivedContractData.metadata.startingOracleMessage,
			startingOracleSignature: receivedContractData.metadata.startingOracleSignature,
			lowLiquidationPriceMultiplier: receivedContractData.metadata.lowLiquidationPriceMultiplier,
			highLiquidationPriceMultiplier: receivedContractData.metadata.highLiquidationPriceMultiplier,
			isSimpleHedge: isSimpleHedge,
			shortPayoutAddress: receivedContractData.metadata.shortPayoutAddress,
			longPayoutAddress: receivedContractData.metadata.longPayoutAddress,
		};

		// Use the received contract data to independently reconstruct the contract data.
		const reconstructedContractData = await this.createContract(creationParameters);

		// Verify that the reconstructed data matches the received data.
		const addressMatches = receivedContractData.address === reconstructedContractData.address;
		const metadataMatches = equals(receivedContractData.metadata, reconstructedContractData.metadata);
		const parametersMatch = equals(receivedContractData.parameters, reconstructedContractData.parameters);

		if(!addressMatches || !metadataMatches || !parametersMatch)
		{
			throw(new Error(`Invalid contract data received for contract ${contractAddress}`));
		}

		debug.action(`Retrieved contract status for '${contractAddress}' from ${this.serviceDomain}.`);

		// Output function result for easier collection of test data.
		debug.result([ 'getContractStatus() =>', receivedContractData ]);

		return receivedContractData;
	}

	/**
	 * Validates that a given contract address matches specific contract parameters.
	 *
	 * @param contractValidationParameters   object containing the parameters to validate this contract.
	 *
	 * @returns true if the contract address and parameters match, otherwise false.
	 */
	async validateContract(contractValidationParameters: ContractValidationParameters): Promise<boolean>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'validateContract() <=', arguments ]);

		// Extract the relevant parameters
		const { contractAddress } = contractValidationParameters;

		// Prepare the contract.
		const contractData = await this.createContract(contractValidationParameters);

		// Build the contract.
		const contract = await this.compileContract(contractData.parameters);

		// Calculate contract validity.
		const contractValidity = (contractAddress === contract.address);

		if(contractValidity)
		{
			// Write log entry for easier debugging.
			debug.action(`Validated a contract address (${contractAddress}) against provided contract parameters.`);
			// eslint-disable-next-line prefer-rest-params
			debug.object(arguments);
		}
		else
		{
			// Write log entry for easier debugging.
			debug.errors(`Failed to validate the provided contract address (${contractAddress}) against provided contract parameters generated address (${contract.address}).`);
			// eslint-disable-next-line prefer-rest-params
			debug.object(arguments);
		}

		// Output function result for easier collection of test data.
		debug.result([ 'validateContract() =>', contractValidity ]);

		// Return the validity of the contract.
		return contractValidity;
	}

	/**
	 * Build and broadcast a custodial mutual redemption transaction with arbitrary transaction details.
	 *
	 * @param shortPrivateKeyWIF    short's private key WIF.
	 * @param longPrivateKeyWIF     long's private key WIF.
	 * @param transactionProposal   unsigned transaction proposal for the mutual redemption.
	 * @param contractParameters    contract parameters of the relevant contract.
	 *
	 * @throws {Error} if any of the private key WIF strings is not valid.
	 * @throws {Error} if any of the private key WIFs does not belong to a party of the contract.
	 * @throws {Error} if the generated transaction could not successfully be broadcasted.
	 * @returns transaction ID of the broadcasted mutual redemption transaction.
	 */
	async custodialMutualArbitraryPayout(
		shortPrivateKeyWIF: string,
		longPrivateKeyWIF: string,
		transactionProposal: UnsignedTransactionProposal,
		contractParameters: ContractParameters,
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'custodialMutualArbitraryPayout() <=', arguments ]);

		// Sign the proposal using both keys.
		const shortProposal = await this.signMutualArbitraryPayout(shortPrivateKeyWIF, transactionProposal, contractParameters);
		const longProposal = await this.signMutualArbitraryPayout(longPrivateKeyWIF, transactionProposal, contractParameters);

		// Build and broadcast the mutual redemption transaction with both signed proposals.
		const transactionID = await this.completeMutualRedemption(shortProposal, longProposal, contractParameters);

		// Output function result for easier collection of test data.
		debug.result([ 'custodialMutualArbitraryPayout() =>', transactionID ]);

		return transactionID;
	}

	/**
	 * Build and broadcast a custodial mutual redemption transaction that mimics a
	 * maturation before the actual maturation timestamp.
	 *
	 * @param shortPrivateKeyWIF   short's private key WIF.
	 * @param longPrivateKeyWIF    long's private key WIF.
	 * @param contractFunding      the specific Contract Funding to use in the custodial early maturation.
	 * @param settlementPrice      price to use in settlement.
	 * @param contractParameters   contract parameters of the relevant contract.
	 * @param contractMetadata     contract metadata of the relevant contract.
	 *
	 * @throws {Error} if any of the private key WIF strings is not valid.
	 * @throws {Error} if any of the private key WIFs does not belong to a party of the contract.
	 * @throws {Error} if the generated transaction could not successfully be broadcasted.
	 * @returns transaction ID of the broadcasted mutual redemption transaction.
	 */
	async custodialMutualEarlyMaturation(
		{
			shortPrivateKeyWIF,
			longPrivateKeyWIF,
			contractFunding,
			contractParameters,
			contractMetadata,
			settlementPrice,
		}
		:
		{
			shortPrivateKeyWIF: string;
			longPrivateKeyWIF: string;
			contractFunding: ContractFunding;
			contractParameters: ContractParameters;
			contractMetadata: ContractMetadata;
			settlementPrice: bigint;
		},
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'custodialMutualEarlyMaturation() <=', arguments ]);

		// Sign the settlement using both keys.
		const shortProposal = await this.signMutualEarlyMaturation(
			{
				privateKeyWIF: shortPrivateKeyWIF,
				contractFunding,
				contractParameters,
				contractMetadata,
				settlementPrice,
			},
		);
		const longProposal = await this.signMutualEarlyMaturation(
			{
				privateKeyWIF: longPrivateKeyWIF,
				contractFunding,
				contractParameters,
				contractMetadata,
				settlementPrice,
			},
		);

		// Build and broadcast the mutual redemption transaction with both signed proposals.
		const transactionID = await this.completeMutualRedemption(shortProposal, longProposal, contractParameters);

		// Output function result for easier collection of test data.
		debug.result([ 'custodialMutualEarlyMaturation() =>', transactionID ]);

		return transactionID;
	}

	/**
	 * Build and broadcast a custodial mutual redemption transaction that refunds
	 * the contract's funds based on the provided contract metadata. Optionally
	 * allows you to provide separate refund addresses. If these are omitted,
	 * the mutual redemption public keys are used to receive the refunds.
	 *
	 * @param shortPrivateKeyWIF     short's private key WIF.
	 * @param longPrivateKeyWIF      long's private key WIF.
	 * @param contractFunding        the specific Contract Funding to use in the custodial refund.
	 * @param contractParameters     contract parameters of the relevant contract.
	 * @param contractMetadata       contract metadata of the relevant contract.
	 * @param [shortRefundAddress]   short's address to receive the refund.
	 * @param [longRefundAddress]    long's address to receive the refund.
	 *
	 * @throws {Error} if any of the private key WIF strings is not valid.
	 * @throws {Error} if any of the private key WIFs does not belong to a party of the contract.
	 * @throws {Error} if the generated transaction could not successfully be broadcasted.
	 * @returns transaction ID of the broadcasted mutual redemption transaction.
	 */
	async custodialMutualRefund({
		shortPrivateKeyWIF,
		longPrivateKeyWIF,
		contractFunding,
		contractParameters,
		contractMetadata,
		shortRefundAddress,
		longRefundAddress,
	}:
	{
		shortPrivateKeyWIF: string;
		longPrivateKeyWIF: string;
		contractFunding: ContractFunding;
		contractParameters: ContractParameters;
		contractMetadata: ContractMetadata;
		shortRefundAddress?: string;
		longRefundAddress?: string; }): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'custodialMutualRefund() <=', arguments ]);

		// Sign the refund using short key.
		const shortProposal = await this.signMutualRefund(
			{
				privateKeyWIF: shortPrivateKeyWIF,
				contractFunding,
				contractParameters,
				contractMetadata,
				shortRefundAddress,
				longRefundAddress,
			},
		);

		// Sign the refund using long key.
		const longProposal = await this.signMutualRefund(
			{
				privateKeyWIF: longPrivateKeyWIF,
				contractFunding,
				contractParameters,
				contractMetadata,
				shortRefundAddress,
				longRefundAddress,
			},
		);

		// Build and broadcast the mutual redemption transaction with both signed proposals.
		const transactionID = await this.completeMutualRedemption(shortProposal, longProposal, contractParameters);

		// Output function result for easier collection of test data.
		debug.result([ 'custodialMutualRefund() =>', transactionID ]);

		return transactionID;
	}

	/**
	 * Sign a mutual redemption transaction that mimics a maturation before the
	 * actual maturation timestamp. Both parties need to call this function with
	 * the same input and settlement price. Both signed transaction proposals must then
	 * be passed into the completeMutualRedemption() function to broadcast the transaction.
	 *
	 * @param privateKeyWIF                      private key WIF of one of the contract's parties.
	 * @param contractFunding                    the specific Contract Funding to use in the mutual early maturation.
	 * @param settlementPrice                    price to use in settlement.
	 * @param contractParameters                 contract parameters of the relevant contract.
	 * @param contractMetadata                   contract metadata of the relevant contract.
	 * @param [takerSettlementFeeInSatoshis=0]   fee in satoshis that taker pays for the early settlement.
	 *
	 * @throws {Error} if the private key WIF string is not valid.
	 * @throws {Error} if the private key WIF does not belong to a party of the contract.
	 * @returns a signed settlement transaction proposal.
	 */
	async signMutualEarlyMaturation(
		{
			privateKeyWIF,
			contractFunding,
			contractParameters,
			contractMetadata,
			settlementPrice,
			takerSettlementFeeInSatoshis = BigInt('0'),
		}:
		{
			privateKeyWIF: string;
			contractFunding: ContractFunding;
			contractParameters: ContractParameters;
			contractMetadata: ContractMetadata;
			settlementPrice: bigint;
			takerSettlementFeeInSatoshis?: bigint;
		},
	): Promise<SignedTransactionProposal>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'signMutualEarlyMaturation() <=', arguments ]);

		// Check that the provided settlement price is within the contract's bounds.
		if(settlementPrice < contractParameters.lowLiquidationPrice || settlementPrice > contractParameters.highLiquidationPrice)
		{
			throw(new Error('Settlement price is out of liquidation bounds, which is unsupported by a mutual early maturation.'));
		}

		// Calculate settlement outcome.
		const { shortPayoutSatsSafe, longPayoutSatsSafe } = await this.calculateSettlementOutcome(contractParameters, contractFunding.fundingSatoshis, settlementPrice);

		// Determine which satoshis the taker would normally be paid out.
		const takerPayoutSatsSafe = (contractMetadata.takerSide === 'short' ? shortPayoutSatsSafe : longPayoutSatsSafe);

		// Determine the largest number of satoshis the taker could pay in fee.
		// NOTE: We require there to exist a payout output in the contract, so we cannot the last dust satoshis out of the taker position.
		const maxTakerSettlementFeeInSatoshis = takerPayoutSatsSafe - DUST_LIMIT;

		// Determine the largest amount of fee we will shift inside the contract.
		const takerSettlementFeeInSatoshisSafe = bigIntMin(takerSettlementFeeInSatoshis, maxTakerSettlementFeeInSatoshis);

		// Calculate short and long payouts, when earning or paying fees respectively.
		const shortPayoutSatsWhenPayingFee = shortPayoutSatsSafe - takerSettlementFeeInSatoshisSafe;
		const shortPayoutSatsWhenEarningFee = shortPayoutSatsSafe + takerSettlementFeeInSatoshisSafe;
		const longPayoutSatsWhenPayingFee = longPayoutSatsSafe - takerSettlementFeeInSatoshisSafe;
		const longPayoutSatsWhenEarningFee = longPayoutSatsSafe + takerSettlementFeeInSatoshisSafe;

		// Shift payout satoshis from the taker to the maker.
		const shortPayoutSatsAfterFee = (contractMetadata.takerSide === 'short' ?  shortPayoutSatsWhenPayingFee : shortPayoutSatsWhenEarningFee);
		const longPayoutSatsAfterFee  = (contractMetadata.takerSide === 'long' ?  longPayoutSatsWhenPayingFee : longPayoutSatsWhenEarningFee);

		// Derive short/long settlement addresses.
		const shortAddress = lockScriptToAddress(contractParameters.shortLockScript);
		const longAddress = lockScriptToAddress(contractParameters.longLockScript);

		// Build transaction proposal based on these parameters.
		const inputs: Utxo[] = [ contractFundingToCoin(contractFunding) ];
		const outputs: Recipient[] =
		[
			{ to: shortAddress, amount: shortPayoutSatsAfterFee },
			{ to: longAddress, amount: longPayoutSatsAfterFee },
		];
		const unsignedProposal: UnsignedTransactionProposal = { inputs, outputs };

		// Sign the proposal.
		const signedProposal = await this.signMutualArbitraryPayout(privateKeyWIF, unsignedProposal, contractParameters);

		// Output function result for easier collection of test data.
		debug.result([ 'signMutualEarlyMaturation() =>', signedProposal ]);

		return signedProposal;
	}

	/**
	 * Sign a mutual redemption transaction that refunds the contract's funds based on
	 * the provided contract metadata. Optionally allows you to provide separate
	 * refund addresses. If these are omitted, the mutual redemption public keys
	 * are used to receive the refunds. Both parties need to call this function with
	 * the same contract funding, contract metadata and refund addresses. Both signed
	 * transaction proposals must then be passed into the completeMutualRedemption()
	 * function to broadcast the transaction.
	 *
	 * @param privateKeyWIF          private key WIF of one of the contract's parties.
	 * @param contractFunding        the specific Contract Funding to use in the mutual refund.
	 * @param contractParameters     contract parameters of the relevant contract.
	 * @param contractMetadata       contract metadata of the relevant contract.
	 * @param [shortRefundAddress]   short's address to receive the refund.
	 * @param [longRefundAddress]    long's address to receive the refund.
	 *
	 * @throws {Error} if the private key WIF string is not valid.
	 * @throws {Error} if the private key WIF does not belong to a party of the contract.
	 * @returns a signed refund transaction proposal.
	 */
	async signMutualRefund(
		{
			privateKeyWIF,
			contractFunding,
			contractParameters,
			contractMetadata,
			shortRefundAddress,
			longRefundAddress,
		}:
		{
			privateKeyWIF: string;
			contractFunding: ContractFunding;
			contractParameters: ContractParameters;
			contractMetadata: ContractMetadata;
			shortRefundAddress?: string;
			longRefundAddress?: string; },
	): Promise<SignedTransactionProposal>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'signMutualRefund() <=', arguments ]);

		// If no refund addresses are provided, derive them from the contracts payout addresses.
		const shortAddress = shortRefundAddress || lockScriptToAddress(contractParameters.shortLockScript);
		const longAddress = longRefundAddress || lockScriptToAddress(contractParameters.longLockScript);

		// To ensure the transaction is valid, we put the minimum output value to DUST
		const shortAmount = bigIntMax(contractMetadata.shortInputInSatoshis, DUST_LIMIT);
		const longAmount = bigIntMax(contractMetadata.longInputInSatoshis, DUST_LIMIT);

		// Build transaction proposal based on the provided parameters.
		const inputs: Utxo[] = [ contractFundingToCoin(contractFunding) ];
		const outputs: Recipient[] =
		[
			{ to: shortAddress, amount: shortAmount },
			{ to: longAddress, amount: longAmount },
		];
		const unsignedProposal: UnsignedTransactionProposal = { inputs, outputs };

		// Sign the proposal.
		const signedProposal = await this.signMutualArbitraryPayout(privateKeyWIF, unsignedProposal, contractParameters);

		// Output function result for easier collection of test data.
		debug.result([ 'signMutualRefund() =>', signedProposal ]);

		return signedProposal;
	}

	/**
	 * Sign a mutual redemption transaction proposal with arbitrary transaction details.
	 * Both parties need to call this function with the same transaction details.
	 * Both signed transaction proposals must then be passed into the
	 * completeMutualRedemption() function to broadcast the transaction.
	 *
	 * @param privateKeyWIF         private key WIF of one of the contract's parties.
	 * @param transactionProposal   An unsigned proposal for a transaction.
	 * @param contractParameters    contract parameters for the relevant contract.
	 *
	 * @throws {Error} if the private key WIF string is not valid.
	 * @throws {Error} if the private key WIF does not belong to a party of the contract.
	 * @throws {Error} if a valid transaction is generated during the preparation.
	 * @returns updated transaction proposal with the resolved variables added in.
	 */
	async signMutualArbitraryPayout(
		privateKeyWIF: string,
		transactionProposal: UnsignedTransactionProposal,
		contractParameters: ContractParameters,
	): Promise<SignedTransactionProposal>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'signMutualArbitraryPayout() <=', arguments ]);

		// Throw an error if mutual redemption is not enabled
		if(contractParameters.enableMutualRedemption === BigInt('0'))
		{
			throw(new Error('Mutual redemption is disabled for this contract.'));
		}

		// Check that there are no obvious errors in the transaction proposal.

		// Check that there are no outputs below the DUST amount.
		if(transactionProposal.outputs.find((output) => output.amount < DUST_LIMIT))
		{
			throw(new Error(`One of the outputs in the transaction proposal is below the DUST amount of ${DUST_LIMIT}.`));
		}

		// Get the contract's redeem script.
		const contract = await this.compileContract(contractParameters);
		const redeemScriptHex = contract.bytecode;

		// Generate unlocking data from the private key WIF
		const unlockingData = await unlockingDataFromWIF(privateKeyWIF, contractParameters);

		// Create a list of unlocking data for every input (same data for every input).
		const unlockingDataPerInput = transactionProposal.inputs.map(() => unlockingData);

		// Attempt to generate a transaction using the passed parameters.
		const attempt = await attemptTransactionGeneration(transactionProposal, redeemScriptHex, unlockingDataPerInput);

		// Check that the attempt was not successful (should never happen).
		if(attempt.success)
		{
			throw(new Error('Internal Error: should not be able to generate valid mutual redemption without both parties'));
		}

		// Extract the relevant redemption data list from the transaction generation attempt.
		const redemptionDataList = extractRedemptionDataList(attempt);

		// Update the transaction proposal by adding the new redemption data list.
		const signedProposal: SignedTransactionProposal = { ...transactionProposal, redemptionDataList };

		// Output function result for easier collection of test data.
		debug.result([ 'signMutualArbitraryPayout() =>', signedProposal ]);

		return signedProposal;
	}

	/**
	 * Complete a mutual redemption by generating a valid transaction from both parties'
	 * signed proposals and broadcasting it. Both parties need to generate and sign the same
	 * transaction proposal using signMutualEarlyMaturation(), signMutualRefund() or
	 * signMutualArbitraryPayout().
	 *
	 * @param signedProposal1      transaction proposal signed by one of the two parties.
	 * @param signedProposal2      transaction proposal signed by the other party.
	 * @param contractParameters   contract parameters for the relevant contract.
	 *
	 * @throws {Error} if any proposal is unsigned.
	 * @throws {Error} if the transaction details of both proposals don't match.
	 * @throws {Error} if the redemption data lists of the proposals have different lengths.
	 * @throws {Error} if both proposals are signed by the same party.
	 * @throws {Error} if the generated transaction could not successfully be broadcasted.
	 * @returns transaction ID of the broadcasted mutual redemption transaction.
	 */
	async completeMutualRedemption(
		signedProposal1: SignedTransactionProposal,
		signedProposal2: SignedTransactionProposal,
		contractParameters: ContractParameters,
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'completeMutualRedemption() <=', arguments ]);

		// Throw an error if mutual redemption is not enabled
		if(contractParameters.enableMutualRedemption === BigInt('0'))
		{
			throw(new Error('Mutual redemption is disabled for this contract.'));
		}

		// Check that the transaction proposal includes redemption data
		if(!signedProposal1.redemptionDataList || !signedProposal2.redemptionDataList)
		{
			const errorMsg = 'Transaction proposal does not include any redemption data. '
				+ 'Make sure that both parties signed their transaction proposals using '
				+ 'signMutualRedemption(), signSettlement() or signRefund().';
			throw(new Error(errorMsg));
		}

		// Merge both proposals, combining their redemption data.
		const mergedProposal = mergeSignedProposals(signedProposal1, signedProposal2);

		// Get the contract's redeem script.
		const contract = await this.compileContract(contractParameters);
		const redeemScriptHex = contract.bytecode;

		// Generate unlocking data for all inputs of the transaction using the passed redemption data
		const unlockingDataPerInput = mergedProposal.redemptionDataList.map(unlockingDataFromRedemptionData);

		// Attempt to generate a transaction using the passed parameters.
		const attempt = await attemptTransactionGeneration(mergedProposal, redeemScriptHex, unlockingDataPerInput);

		// Check that the transaction generation didn't fail (happens if the proposal was only signed by a single party)
		if(!attempt.success)
		{
			const errorMsg = 'Mutual redemption could not successfully be completed. '
				+ 'Make sure that the passed proposals are signed by different parties.';
			throw(new Error(errorMsg));
		}

		// Hex encode the generated transaction.
		const transactionHex = binToHex(encodeTransaction(attempt.transaction));

		// Broadcast the transaction.
		const broadcastResult = await broadcastTransaction(transactionHex, this.networkProvider);

		// Output function result for easier collection of test data.
		debug.result([ 'completeMutualRedemption() =>', broadcastResult ]);

		return broadcastResult;
	}

	/**
	 * Calculate the total satoshis required as input to a collaborative funding transaction.
	 * This includes the contract input satoshis, contract safety fees, potential settlement
	 * service fees and funding transaction fees. This assumes 2 P2PKH inputs to the funding
	 * transaction.
	 *
	 * @param contractData   contract data for which to calculate funding satoshis
	 *
	 * @returns total required funding satoshis
	 */
	calculateTotalRequiredFundingSatoshis(contractData: ContractData): bigint
	{
		return calculateRequiredFundingSatoshis(contractData);
	}

	/**
	 * Create an unsigned funding proposal.
	 * @param contractData              contract data for the proposal
	 * @param outpointTransactionHash   outpoint transaction hash of the UTXO for the proposal
	 * @param outpointIndex             outpoint index of the UTXO for the proposal
	 * @param satoshis                  satoshi amount of the UTXO for the proposal
	 * @returns unsigned funding proposal created from the provided data.
	 */
	createFundingProposal(
		contractData: ContractData,
		outpointTransactionHash: string,
		outpointIndex: number,
		satoshis: bigint,
	): UnsignedFundingProposal
	{
		// Construct an unsigned funding proposal using the provided data
		const proposal: UnsignedFundingProposal =
		{
			contractData,
			utxo:
			{
				txid: outpointTransactionHash,
				vout: outpointIndex,
				satoshis: satoshis,
			},
		};

		return proposal;
	}

	/**
	 * Sign a partial funding transaction for a contract and add the signature and public key to the proposal.
	 *
	 * @param privateKeyWIF     private key WIF of one of the contract's parties.
	 * @param fundingProposal   An unsigned proposal for a contract funding.
	 *
	 * @throws {Error} if the private key WIF string is not valid.
	 * @throws {Error} if a valid transaction is generated during the preparation.
	 * @returns updated transaction proposal with the resolved variables added in.
	 */
	async signFundingProposal(
		privateKeyWIF: string,
		fundingProposal: UnsignedFundingProposal,
	): Promise<SignedFundingProposal>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'signFundingProposal() <=', arguments ]);

		// Deterministically generate a partial, unsigned transaction from the funding proposal.
		const unsignedTransaction = generateTransactionFromFundingProposal(fundingProposal);

		// Sign the unsigned transaction.
		const signedTransaction = await signTransactionP2PKH(privateKeyWIF, unsignedTransaction);

		// Extract the signature from the signed transaction.
		const decodedUnlockingBytecode = decodeUnlockingBytecodeP2PKH(signedTransaction.inputs[0].unlockingBytecode);

		// Construct a signed funding proposal object.
		const signedFundingProposal: SignedFundingProposal = { ...fundingProposal, ...decodedUnlockingBytecode };

		// Output function result for easier collection of test data.
		debug.result([ 'signFundingProposal() =>', signedFundingProposal ]);

		return signedFundingProposal;
	}

	/**
	 * Complete a funding by combining two signed funding proposals and returning the resulting transaction hex.
	 *
	 * @param signedProposal1   first signed proposal.
	 * @param signedProposal2   second signed proposal.
	 *
	 * @throws {Error} if total provided satoshis is not exactly the total required satoshis.
	 * @throws {Error} if the two funding proposals belong to different contracts.
	 * @returns transaction ID for the broadcasted funding transaction.
	 */
	async completeFundingProposal(
		signedProposal1: SignedFundingProposal,
		signedProposal2: SignedFundingProposal,
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'completeFundingProposal() <=', arguments ]);

		// Check that the two proposals are for the same contract by comparing the contractData objects by value
		// Note: this requires a deep equality so we use a json string comparison, depending on exact ordering.
		if(JSONStringify(signedProposal1.contractData) !== JSONStringify(signedProposal2.contractData))
		{
			throw(new Error(`Funding proposals do not use the same contract data (1: ${signedProposal1.contractData.address}, 2: ${signedProposal2.contractData.address})`));
		}

		// Calculate the required and provided satoshis (including transaction fees for the funding transaction)
		const totalRequiredSatoshis = calculateRequiredFundingSatoshis(signedProposal1.contractData);
		const totalProvidedSatoshis = signedProposal1.utxo.satoshis + signedProposal2.utxo.satoshis;

		// Check that the provided satoshis exactly the required satoshis
		if(totalProvidedSatoshis !== totalRequiredSatoshis)
		{
			throw(new Error(`Total provided satoshis (${totalProvidedSatoshis}) should be EXACTLY the required satoshis (${totalRequiredSatoshis})`));
		}

		// Generate two partial transactions
		const partialTransaction1 = generateTransactionFromFundingProposal(signedProposal1);
		const partialTransaction2 = generateTransactionFromFundingProposal(signedProposal2);

		// Merge the inputs of transaction 2 into transaction 1
		// Note: we know that the rest of transaction 1 & 2 match because we checked that the contract data matched
		const transaction = mergeTransactionInputs(partialTransaction1, partialTransaction2);

		// Hex encode the generated transaction.
		const transactionHex = binToHex(encodeTransaction(transaction));

		// Output function result for easier collection of test data.
		debug.result([ 'completeFundingProposal() =>', transactionHex ]);

		return transactionHex;
	}

	/**
	 * Liquidates a contract.
	 *
	 * @param contractLiquidationParameters   object containing the parameters to liquidate this contract.
	 *
	 * @returns ContractSettlement object containing the details of the liquidation.
	 */
	async liquidateContractFunding(contractLiquidationParameters: ContractSettlementParameters): Promise<ContractLiquidation>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'liquidateContractFunding() <=', arguments ]);

		// Extract relevant parameters.
		const { contractFunding, settlementMessage, contractParameters } = contractLiquidationParameters;

		// Write log entry for easier debugging.
		debug.action(`Attempting to liquidate contract funding '${contractFundingToOutpoint(contractFunding)}'.`);

		// Parse the settlement message.
		const { messageTimestamp, priceValue } = await OracleData.parsePriceMessage(hexToBin(settlementMessage));

		// Validate that the settlement message timestamp is not at or after the maturation timestamp.
		// Note that comparison between number and bigint is fine.
		if(messageTimestamp >= contractParameters.maturityTimestamp)
		{
			// Define an error message
			const errorMessage = `Cannot liquidate contract funding '${contractFundingToOutpoint(contractFunding)}' at or after its maturation timestamp.`;

			// Log the error message.
			debug.errors(errorMessage);

			// Throw the error.
			throw(new Error(errorMessage));
		}

		// Validate that the settlement price is strictly outside liquidation boundaries.
		if((priceValue > contractParameters.lowLiquidationPrice) && (priceValue < contractParameters.highLiquidationPrice))
		{
			// Define an error message
			const errorMessage = `Cannot liquidate contract funding '${contractFundingToOutpoint(contractFunding)}' at a price within the contract boundaries.`;

			// Log the error message.
			debug.errors(errorMessage);

			// Throw the error.
			throw(new Error(errorMessage));
		}

		// Settle the contract funding.
		const settlementData = await this.settleContractFunding(contractLiquidationParameters) as ContractLiquidation;

		// Output function result for easier collection of test data.
		debug.result([ 'liquidateContractFunding() =>', settlementData ]);

		// Return the liquidation result.
		return settlementData;
	}

	/**
	 * Matures a contract.
	 *
	 * @param contractMaturationParameters   object containing the parameters to mature this contract.
	 *
	 * @returns ContractSettlement object containing the details of the maturation.
	 */
	async matureContractFunding(contractMaturationParameters: ContractSettlementParameters): Promise<ContractMaturation>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'matureContractFunding() <=', arguments ]);

		// Extract relevant parameters.
		const { contractFunding, settlementMessage, contractParameters } = contractMaturationParameters;

		// Write log entry for easier debugging.
		debug.action(`Attempting to mature contract funding '${contractFundingToOutpoint(contractFunding)}'.`);

		// Parse the settlement message.
		const { messageTimestamp } = await OracleData.parsePriceMessage(hexToBin(settlementMessage));

		// Validate that the settlement message's timestamp is not before the contract's maturity timestamp.
		// Note that comparison between number and bigint is fine.
		if(messageTimestamp < contractParameters.maturityTimestamp)
		{
			// Define an error message
			const errorMessage = `Cannot mature contract funding '${contractFundingToOutpoint(contractFunding)}' before its maturity height.`;

			// Log the error message.
			debug.errors(errorMessage);

			// Throw the error.
			throw(new Error(errorMessage));
		}

		// Settle the contract funding.
		const settlementData = await this.settleContractFunding(contractMaturationParameters) as ContractMaturation;

		// Output function result for easier collection of test data.
		debug.result([ 'matureContractFunding() =>', settlementData ]);

		// Return the liquidation result.
		return settlementData;
	}

	/**
	 * Settles a contract (this includes both maturation and liquidation).
	 *
	 * @param contractSettlementParameters   object containing the parameters to settle this contract.
	 *
	 * @returns ContractSettlement object containing the details of the settlement.
	 */
	async settleContractFunding(contractSettlementParameters: ContractSettlementParameters): Promise<ContractAutomatedPayout>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'settleContractFunding() <=', arguments ]);

		// Extract relevant parameters.
		const {
			oraclePublicKey,
			settlementMessage,
			settlementSignature,
			previousMessage,
			previousSignature,
			contractFunding,
			contractParameters,
		} = contractSettlementParameters;

		// Write log entry for easier debugging.
		debug.action(`Attempting to settle contract funding '${contractFundingToOutpoint(contractFunding)}'.`);

		// Parse the settlement and previous messages.
		const settlementMessageData = await OracleData.parsePriceMessage(hexToBin(settlementMessage));
		const previousMessageData = await OracleData.parsePriceMessage(hexToBin(previousMessage));

		// Validate that the settlement message timestamp is not before contract's start timestamp.
		// Note that "<" comparison between number and bigint is fine.
		if(settlementMessageData.messageTimestamp < contractParameters.startTimestamp)
		{
			// Define an error message
			const errorMessage = `Cannot settle contract funding '${contractFundingToOutpoint(contractFunding)}' before its start timestamp.`;

			// Log the error message.
			debug.errors(errorMessage);

			// Throw the error.
			throw(new Error(errorMessage));
		}

		// Validate that the settlement message is immediately after the previous message
		// Note that all values here are JS numbers.
		if(settlementMessageData.priceSequence - 1 !== previousMessageData.priceSequence)
		{
			// Define an error message
			const errorMessage = `Cannot settle contract funding '${contractFundingToOutpoint(contractFunding)}' when the provided settlement message does not immediately follow the provided previous message.`;

			// Log the error message.
			debug.errors(errorMessage);

			// Throw the error.
			throw(new Error(errorMessage));
		}

		// Validate that the previous settlement message timestamp is not at or after the contract's maturity timestamp.
		// Note that ">=" comparison between number and bigint is fine.
		if(previousMessageData.messageTimestamp >= contractParameters.maturityTimestamp)
		{
			// Define an error message
			const errorMessage = `Cannot settle contract funding '${contractFundingToOutpoint(contractFunding)}' with a previous message that is at or after its maturity timestamp.`;

			// Log the error message.
			debug.errors(errorMessage);

			// Throw the error.
			throw(new Error(errorMessage));
		}

		// Validate that the settlement price is not zero or negative.
		// Note that all values here are JS numbers.
		if(settlementMessageData.priceValue <= 0)
		{
			// Define an error message
			const errorMessage = `Cannot settle contract funding '${contractFundingToOutpoint(contractFunding)}' at a price of <= 0.`;

			// Log the error message.
			debug.errors(errorMessage);

			// Throw the error.
			throw(new Error(errorMessage));
		}

		// Calculate contract outcomes.
		const outcome = await this.calculateSettlementOutcome(contractParameters, contractFunding.fundingSatoshis, BigInt(settlementMessageData.priceValue));
		const { shortPayoutSatsSafe, longPayoutSatsSafe, minerFeeSats } = outcome;

		// Redeem the contract.
		const settlementTransactionHash = await this.automatedPayout({
			oraclePublicKey,
			settlementMessage,
			settlementSignature,
			previousMessage,
			previousSignature,
			shortPayoutSats: shortPayoutSatsSafe,
			longPayoutSats: longPayoutSatsSafe,
			contractFunding,
			contractParameters,
			minerFeeSats,
		});

		// Determine whether this is a maturation or a liquidation.
		const isMaturation = settlementMessageData.messageTimestamp >= contractParameters.maturityTimestamp;
		const settlementType = isMaturation ? SettlementType.MATURATION : SettlementType.LIQUIDATION;

		// Assemble a ContractSettlement object representing the settlement.
		const settlementData: ContractAutomatedPayout =
		{
			settlementType: settlementType,
			settlementTransactionHash: settlementTransactionHash,
			shortPayoutInSatoshis: shortPayoutSatsSafe,
			longPayoutInSatoshis: longPayoutSatsSafe,
			settlementMessage: settlementMessage,
			settlementSignature: settlementSignature,
			previousMessage: previousMessage,
			previousSignature: previousSignature,
			settlementPrice: BigInt(settlementMessageData.priceValue),
		};

		// Output function result for easier collection of test data.
		debug.result([ 'settleContractFunding() =>', settlementData ]);

		// Return the settlement result.
		return settlementData;
	}

	/**
	 * Redeems the contract with arbitrary numbers.
	 *
	 * @param automatedPayoutParameters   object containing the parameters to payout this contract.
	 *
	 * @returns the transaction ID of a successful redemption.
	 *
	 * @private
	 */
	async automatedPayout(automatedPayoutParameters: AutomatedPayoutParameters): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'automatedPayout() <=', arguments ]);

		// Extract relevant parameters.
		const {
			settlementMessage,
			settlementSignature,
			previousMessage,
			previousSignature,
			shortPayoutSats,
			longPayoutSats,
			contractFunding,
			contractParameters,
			minerFeeSats,
		} = automatedPayoutParameters;

		// Write log entry for easier debugging.
		debug.action(`Attempting to payout contract funding '${contractFundingToOutpoint(contractFunding)}'.`);

		try
		{
			// Build the contract instance.
			const contract = await this.compileContract(contractParameters);

			// Build the payout transaction.
			const payoutTransaction = await buildPayoutTransaction({
				contract,
				contractParameters,
				contractFunding,
				settlementMessage,
				settlementSignature,
				previousMessage,
				previousSignature,
				shortPayoutSats,
				longPayoutSats,
				minerFeeSats,
			});

			// Broadcast the transaction.
			const broadcastResult = await this.broadcastTransaction(payoutTransaction);

			// Output function result for easier collection of test data.
			debug.result([ 'automatedPayout() =>', broadcastResult ]);

			// Return the broadcast result.
			return broadcastResult;
		}
		catch(error: any)
		{
			// Define a base error message.
			const baseErrorMessage = `Failed to payout contract funding '${contractFundingToOutpoint(contractFunding)}'`;

			// Log an error message.
			debug.errors(`${baseErrorMessage}: `, error);

			// If the error includes a meep command, we remove it before passing it on.
			const errorMessageExcludingMeep = error.message ? error.message.split('\nmeep')[0] : error;

			// Throw the error.
			throw(new Error(`${baseErrorMessage}: ${errorMessageExcludingMeep}`));
		}
	}

	/*
	// Internal library functions
	*/

	/**
	 * Wrapper that broadcasts a prepared transaction using the CashScript SDK.
	 *
	 * @param transactionBuilder   fully prepared transaction builder ready to execute its broadcast() function.
	 *
	 * @returns the transaction ID of a successful transaction.
	 *
	 * @private
	 */
	async broadcastTransaction(transactionBuilder: Transaction): Promise<string>
	{
		try
		{
			// Broadcast the raw transaction
			const { txid } = await transactionBuilder.send();

			return txid;
		}
		catch(error)
		{
			// Log an error message.
			debug.errors('Failed to broadcast transaction: ', error);

			// Set locktime to avoid long network call
			debug.errors('Failed to broadcast transaction: setting locktime to 0 to avoid network call for blockheight');
			transactionBuilder.withTime(0);

			// Build and log raw transaction hex
			const rawTransactionHex = await transactionBuilder.build();
			debug.errors(rawTransactionHex);

			// Throw the error.
			throw(error);
		}
	}

	/**
	 * Retrieve a list of all ContractFunding instances for a contract.
	 *
	 * @param contractParameters   Contract parameters for the relevant contract.
	 *
	 * @returns list of contract fundings for a contract.
	 */
	async getContractFundings(contractParameters: ContractParameters): Promise<ContractFunding[]>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'getContractFundings() <=', arguments ]);

		// Build the contract.
		const contract = await this.compileContract(contractParameters);

		// Retrieve contract's coins as CashScript UTXOs.
		const coins = await contract.getUtxos();

		// Format the CashScript UTXOs as ContractFunding interfaces.
		const fundings = coins.map(contractCoinToFunding);

		// Output function result for easier collection of test data.
		debug.result([ 'getContractFundings() =>', fundings ]);

		return fundings;
	}

	/**
	 * Simulates contract settlement outcome based on contract parameters, total satoshis in the contract and the redemption price.
	 *
	 * @param contractParameters   contract parameters including price boundaries.
	 * @param fundingSats          total number of satoshis in the UTXO used for this calculation.
	 * @param redeemPrice          price (units/BCH) to base the redemption simulation on.
	 *
	 * @returns simulationOutput   the payout results of the simulation.
	 *
	 * @private
	 */
	async calculateSettlementOutcome(
		contractParameters: ContractParameters,
		fundingSats: bigint,
		redeemPrice: bigint,
	): Promise<SimulationOutput>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'calculateSettlementOutcome() <=', arguments ]);

		// calculate the clamped price for the contract outcomes.
		// In the contract:
		// int clampedPrice = max(min(oraclePrice, highLiquidationPrice), lowLiquidationPrice);
		const clampedPrice = bigIntMax(bigIntMin(redeemPrice, contractParameters.highLiquidationPrice), contractParameters.lowLiquidationPrice);

		// Calculate the cost of the nominal hedge in sats at the current price.
		// Note that this calculation is repeated below for shortPayoutSatsUnsafe so that the contract math has an exact equivalent in code.
		const satsForNominalUnits = contractParameters.nominalUnitsXSatsPerBch / clampedPrice;

		// Calculate short payout and dust protection
		// Bigint division floors toward zero, matching the reality of the contract math at least for the all-positive numbers we are using
		// In the contract:
		// int shortSats = max(DUST, (nominalUnitsXSatsPerBch / clampedPrice) - satsForNominalUnitsAtHighLiquidation);
		const shortPayoutSatsUnsafe = (contractParameters.nominalUnitsXSatsPerBch / clampedPrice) - contractParameters.satsForNominalUnitsAtHighLiquidation;
		const shortPayoutSatsSafe = bigIntMax(DUST_LIMIT, shortPayoutSatsUnsafe);

		// Calculate long payout and dust protection, and total payout
		// In the contract:
		// int longSats = max(DUST, payoutSats - shortSats);
		const longPayoutSatsUnsafe = contractParameters.payoutSats - shortPayoutSatsSafe;
		const longPayoutSatsSafe = bigIntMax(DUST_LIMIT, longPayoutSatsUnsafe);

		// Calculate total payout with dust safety and final miner fee
		// These are not contract calculations - just useful context
		const totalPayoutSatsSafe = shortPayoutSatsSafe + longPayoutSatsSafe;
		const minerFeeSats = fundingSats - totalPayoutSatsSafe;

		const result: SimulationOutput = { shortPayoutSatsSafe, longPayoutSatsSafe, totalPayoutSatsSafe, satsForNominalUnits, minerFeeSats };

		// Write log entry for easier debugging.
		debug.action('Simulating contract outcomes.');

		// Output function result for easier collection of test data.
		debug.result([ 'calculateSettlementOutcome() =>', result ]);

		// Return the results of the calculation.
		return result;
	}

	/**
	 * Builds a contract instance from contract parameters.
	 *
	 * @param contractParameters   contract parameters required to build the contract instance.
	 *
	 * @returns a contract instance.
	 *
	 * @private
	 */
	async compileContract(contractParameters: ContractParameters): Promise<Contract>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'compileContract() <=', arguments ]);

		// Write log entry for easier debugging.
		debug.action('Creating contract instance.');

		// Retrieve the correct artifact.
		const artifact = AnyHedgeArtifacts[this.contractVersion];

		// Convert contract parameters object into ordered list of contract constructor arguments
		const parameters: Argument[] =
		[
			contractParameters.shortMutualRedeemPublicKey,
			contractParameters.longMutualRedeemPublicKey,
			contractParameters.enableMutualRedemption,
			contractParameters.shortLockScript,
			contractParameters.longLockScript,
			contractParameters.oraclePublicKey,
			contractParameters.nominalUnitsXSatsPerBch,
			contractParameters.satsForNominalUnitsAtHighLiquidation,
			contractParameters.payoutSats,
			contractParameters.lowLiquidationPrice,
			contractParameters.highLiquidationPrice,
			contractParameters.startTimestamp,
			contractParameters.maturityTimestamp,
		];

		// Instantiate the contract options and contract
		const contractOptions: ContractOptions = { provider: this.networkProvider, addressType: 'p2sh32' };
		const contract = new Contract(artifact, parameters, contractOptions);

		// Output function result for easier collection of test data.
		debug.result([ 'compileContract() =>', `${contract.name} contract with address ${contract.address}` ]);

		// Pass back the contract to the caller.
		return contract;
	}

	/**
	 * Creates a new contract.
	 *
	 * @param contractCreationParameters  {ContractCreationParameters}   object containing the parameters to create this contract.
	 *
	 * @returns {Promise<ContractData>} the contract parameters and metadata.
	 */
	async createContract(contractCreationParameters: ContractCreationParameters): Promise<ContractData>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'createContract() <=', arguments ]);

		// Write log entry for easier debugging.
		debug.action('Preparing a new contract.');

		// Extract relevant parameters.
		const {
			takerSide,
			makerSide,
			oraclePublicKey,
			shortPayoutAddress,
			longPayoutAddress,
			enableMutualRedemption,
			nominalUnits,
			startingOracleMessage,
			startingOracleSignature,
			maturityTimestamp,
			isSimpleHedge,
			highLiquidationPriceMultiplier,
			lowLiquidationPriceMultiplier,
			shortMutualRedeemPublicKey,
			longMutualRedeemPublicKey,
		} = contractCreationParameters;

		// Throw an error if the taker is invalid.
		if(!(takerSide === 'short' || takerSide === 'long'))
		{
			throw(new Error(`Taker (${takerSide}) must be either 'short' or ' long'.`));
		}

		// Throw an error if maker side if not opposite to taker side.
		if(makerSide !== (takerSide === 'short' ? 'long' : 'short'))
		{
			throw(new Error(`Maker (${makerSide}) must be on the opposite side of the taker (${takerSide})`));
		}

		// Validate the starting oracle message.
		const messageSignatureIsValid = await OracleData.verifyMessageSignature(hexToBin(startingOracleMessage), hexToBin(startingOracleSignature), hexToBin(oraclePublicKey));
		if(!messageSignatureIsValid)
		{
			throw(new Error(`Provided starting oracle message signature (${startingOracleSignature}) is not valid for the starting message (${startingOracleMessage}) and oracle (${oraclePublicKey}).`));
		}

		// Extract starting time and price from the starting oracle message.
		/* eslint-disable @typescript-eslint/naming-convention */
		const { messageTimestamp: _startTimestamp, priceValue: _startPrice } = await OracleData.parsePriceMessage(hexToBin(startingOracleMessage));
		const startTimestamp: bigint = BigInt(_startTimestamp);
		const startPrice: bigint = BigInt(_startPrice);
		/* eslint-enable @typescript-eslint/naming-convention */

		// Before calculating key values below, this is an outline of the setup, using relevant variable names.
		//
		// AnyHedge 0.12 with leveraged shorts works on the same concept as previous "simple/nominal hedge" versions.
		// By including one more number, explained below, the contract can now provide independent leverage to Short and Long.

		// In summary, Long uses AnyHedge to "buy" an asset now, "selling" it later for hopefully a higher price, and
		// Short uses AnyHedge to "sell" an asset now, "buying" it later for hopefully a lower price.

		// The hedge value / simple hedge / nominal hedge is a fixed asset value that the rest of the contract centers around.
		// The original idea of AnyHedge is that at any price within the liquidation (more commonly "margin call") range,
		// Short receives exactly the nominal hedge value at the end, paid in Bch. This is the definition of behavior for
		// short leverage = 1, i.e. virtually holding another asset, using BCH as the vehicle instead of the asset itself.

		// The setting of liquidation values is mathematically equivalent to setting leverage for Short and Long.
		// In the original setup, only Long was mathematically bound to the liquidation price, establishing leverage.
		// Short on the other hand had a liquidation price for safety purposes, but the behavior of Short payout was not bound
		// mathematically to that liquidation price. The contract simply always paid out Short for the full hedge value,
		// which at the absurd extreme can be achieved with one satoshi at a price approaching infinity.

		// The current arrangement described below allows Short to achieve leverage by tying behavior to the high
		// liquidation price if desired, or to retain original behavior by disconnecting from the high liquidation price.
		// Given this setup at start, the contract is fully collateralized to handle any outcome within the liquidation range.

		// The relationship between liquidation prices and effective leverage are not explained in depth here,
		// but the summary is:
		//     shortLeverage = 1 + (1 / ((highLiquidationPrice / startPrice) - 1))
		//     longLeverage  = 1 / (1 - (lowLiquidationPrice / startPrice))

		//  *Price in Asset/Bch*            *Cost of Hedge at Price (higher price ==> lower cost)*
		// --------------------------------------------------------------------------------------------------------------------
		//                        ^
		//                        |
		//  highLiquidationPrice  -    ---- satsForHedgeAtHighLiq (this is the new reference that allows short leverage)
		//                        |    ||||                       (in the past, the assumption was zero cost at infinity)
		//                        |    ||||
		//                        |    |||| } shortInputSats = worst case "sell low buy high" case for short
		//                        |    ||||                  = satsForHedgeAtStart - satsForHedgeAtHighLiq
		//                        |    ||||
		//                        |    ||||
		//            startPrice  -    ---- satsForHedgeAtStart
		//                        |    ||||
		//                        |    |||| } longInputSats = worst case "buy high sell low" case for long
		//                        |    ||||                 = satsForHedgeAtLowLiq - satsForHedgeAtStart
		//                        |    ||||
		//   lowLiquidationPrice  -    ---- satsForHedgeAtLowLiq (to reiterate, lowest price ==> highest cost)
		//                        |
		//                        |
		//                        - 0

		// Payout at the end of the contract is the same picture, except based on the end price instead of start Price.

		//  *Price in Asset/Bch*            *Cost of Hedge at Price (higher price ==> lower cost)*
		// --------------------------------------------------------------------------------------------------------------------
		//                        ^
		//                        |
		//  highLiquidationPrice  -    ---- satsForHedgeAtHighLiq (zero at infinity for simple hedge)
		//                        |    ||||
		//                        |    |||| } shortPayoutSats = satsForHedgeAtEnd - satsForHedgeAtHighLiq
		//                        |    ||||
		//              endPrice  -    ---- satsForHedgeAtEnd (price moved up, Short gets more Bch, Long gets less)
		//                        |    ||||
		//                        |    ||||
		//            startPrice  -    ||||
		//                        |    ||||
		//                        |    |||| } longPayinSats = satsForHedgeAtLowLiq - satsForHedgeAtEnd
		//                        |    ||||
		//                        |    ||||
		//   lowLiquidationPrice  -    ---- satsForHedgeAtLowLiq
		//                        |
		//                        |
		//                        - 0

		// 1. Low liquidation price: the low price that triggers liquidation of the Long party.
		// The value is rounded to achieve a result as close as possible to intent.
		// There should be no integer-level loss of precision given the 32-bit range of oracle prices.
		const lowLiquidationPrice: bigint = BigInt(Math.round(lowLiquidationPriceMultiplier * Number(startPrice)));

		// 2. High liquidation price: the high price that triggers liquidation of the Short party.
		// The value is rounded to achieve a result as close as possible to intent.
		// There should be no integer-level loss of precision given the 32-bit range of oracle prices.
		const highLiquidationPrice: bigint = BigInt(Math.round(highLiquidationPriceMultiplier * Number(startPrice)));

		// 3. Composite number representing the nominal hedge value in asset terms.
		// In the diagrams above, this is the value discussed as "Hedge"
		// Note: Rather than using the nominal hedge value alone, the number is calculated as
		//       (nominal hedge units * 1e8 sats/bch). This allows the critical calculation in the contract to be simple
		//       division, and is also a carryover from previous BCH VM versions (before May 2022) that did not have
		//       multiplication. I.e. this value divided by the oracle price directly yields satoshis for
		//       nominal hedge value at the given price, which is the final units we need to establish payouts.
		// Note: Due to the potential for very large numbers that break the javascript 32-bit boundary
		//       for integer-level precision, this value forces either introduction of BigInt or strict boundaries
		//       that severely limit the values possible in a contract. This library chooses BigInt.
		// Note: DO NOT CONVERT THE COMPOSITE VALUE TO A JAVASCRIPT NUMBER FOR ANY REASON.
		//       Any such javascript Number calculations or transmissions (e.g. through simple JSON) must be
		//       considered to introduce undefined behavior and be rejected.
		//       The initial Number calculation is a one way translation from floating precision
		//       nominalUnits input to BigInt.
		// Validation requires other values and is done below.
		const nominalUnitsXSatsPerBch: bigint = BigInt(Math.round(nominalUnits * Number(SATS_PER_BCH)));

		// 4. Cost in sats of the nominal hedge at high liquidation price.
		// This is the value satsForHedgeAtHighLiq in the diagrams above.
		// In summary, this value is needed to calculate Short's payout.
		// Validation requires other values and is done below.
		// Calculation of the value is discontinuous as follows:
		let satsForNominalUnitsAtHighLiquidation: bigint;
		if(isSimpleHedge)
		{
			// a) For a simple hedge (short leverage = 1), the value is exactly 0. This
			// represents the concept and calculation that Long must cover the full range
			// of Short payouts from current price to infinity (where the cost becomes zero, this value).
			satsForNominalUnitsAtHighLiquidation = BigInt('0');
		}
		else
		{
			// b) For a leveraged short (short leverage > 1), the value is calculated from the
			// other parameters in a simple cost relationship.
			// Note: cost is floored for safety by bigint division in order to ensure that the
			//       total sats in the contract cover a range that may be at worst +1 or +2 from
			//       the "real" full precision value. This is valuable to ensure that contract
			//       calculations never result in a value more than the total sats available.
			satsForNominalUnitsAtHighLiquidation = nominalUnitsXSatsPerBch / highLiquidationPrice;
		}

		// 5. Cost in sats of the nominal hedge at low liquidation.
		// This is the value satsForHedgeAtLowLiq in the diagrams above.
		// In summary, this value is needed to calculate total input.
		// Note: Here we use simple integer division and accept the loss of rounding.
		//       Safety is ensured outside the zero case by validation.
		// Note: We do zero testing here because this happens before parameter validation
		//       which would catch it otherwise.
		if(lowLiquidationPrice === BigInt('0'))
		{
			throw(new Error('low liquidation price must be greater than zero'));
		}
		const satsForNominalUnitsAtLowLiquidation: bigint = nominalUnitsXSatsPerBch / lowLiquidationPrice;

		// 6. Total input satoshis: the difference between worst case long and short outcomes.
		const totalInputInSatoshis: bigint = satsForNominalUnitsAtLowLiquidation - satsForNominalUnitsAtHighLiquidation;

		// Next, we derive the remaining values for the contract parameters and metadata.

		// Cost in sats of the nominal hedge at start price: This is satsForHedgeAtStart in the diagram above.
		// It is mostly only useful for calculating how much short needs to put into the contract in the
		// case of leveraged shorts (short leverage > 1). BigInt division is naturally floored and is a reasonable
		// integer adjustment considering unavailability of round() due to presence of the compound value which
		// must not be converted to javascript Number for any reason. We could recover precision using modulo
		// calculations, but it's probably not worth the complexity cost, especially regarding formal validation.
		const satsForNominalUnitsAtStart: bigint = nominalUnitsXSatsPerBch / startPrice;

		// Short input satoshis
		// The definition of Short's input is that it needs to fully cover the worst case outcome
		// of Long buying low and selling high as in the diagrams above.
		const shortInputInSatoshis: bigint = satsForNominalUnitsAtStart - satsForNominalUnitsAtHighLiquidation;

		// Long input satoshis
		// The definition of Long's input is that it needs to fully cover the worst case outcome
		// of Short selling high and buying low as in the diagrams above.
		// However, in order to be extra clear about exact match with total input sats,
		// we calculate it as everything left over from total sats minus Short input.
		// It's equal to (satsForNominalUnitsAtLowLiquidation - satsForNominalUnitsAtStart)
		const longInputInSatoshis: bigint = totalInputInSatoshis - shortInputInSatoshis;

		// Calculate the effective oracle unit values for metadata
		// Note: These are derivative values, so we use float to get a general result
		const longInputInOracleUnits: number = (Number(longInputInSatoshis) / Number(SATS_PER_BCH)) * Number(startPrice);
		const shortInputInOracleUnits: number = (Number(shortInputInSatoshis) / Number(SATS_PER_BCH)) * Number(startPrice);

		// In addition to money-related numbers, we derive time-related numbers for the contract and metadata.

		// Calculate the contracts duration in seconds.
		const durationInSeconds: bigint = maturityTimestamp - startTimestamp;

		// We also package keys and other fixed values for the contract and metadata.

		// Create short and long lock scripts from the provided addresses.
		const shortLockScript = addressToLockScript(shortPayoutAddress);
		const longLockScript = addressToLockScript(longPayoutAddress);

		// Assemble the contract parameters and validate for safety constraints.
		const contractParameters: ContractParameters =
		{
			maturityTimestamp,
			startTimestamp,
			highLiquidationPrice,
			lowLiquidationPrice,
			payoutSats: totalInputInSatoshis,
			nominalUnitsXSatsPerBch,
			satsForNominalUnitsAtHighLiquidation,
			oraclePublicKey,
			longLockScript,
			shortLockScript,
			enableMutualRedemption,
			longMutualRedeemPublicKey,
			shortMutualRedeemPublicKey,
		};
		await validateContractExecutionSafetyConstraints(startPrice, contractParameters);

		// Build the corresponding contract
		const contract = await this.compileContract(contractParameters);

		// Estimate the miner cost for the payout transaction size (paying 1.0 sats/b).
		const minerCostInSatoshis: bigint = await estimatePayoutTransactionFee(contract, contractParameters, 1.0);

		// Assemble the contract metadata.
		const contractMetadata: ContractMetadata =
		{
			takerSide,
			makerSide,
			shortPayoutAddress,
			longPayoutAddress,
			startingOracleMessage,
			startingOracleSignature,
			durationInSeconds,
			highLiquidationPriceMultiplier,
			lowLiquidationPriceMultiplier,
			isSimpleHedge,
			startPrice,
			nominalUnits,
			shortInputInOracleUnits,
			longInputInOracleUnits,
			shortInputInSatoshis,
			longInputInSatoshis,
			minerCostInSatoshis,
		};

		// Assemble the final contract data.
		const contractData: ContractData =
		{
			version: this.contractVersion,
			address: contract.address,
			parameters: contractParameters,
			metadata: contractMetadata,
			fundings: [],
			fees: [],
		};

		// Output function result for easier collection of test data.
		debug.result([ 'createContract() =>', contractData ]);

		// Pass back the contract data to the caller.
		return contractData;
	}

	/**
	 * Adds a new fee to a set of contract data, allowing services to define their fees before registering with a settlement service.
	 *
	 * @param contractData   contract data to add a fee to.
	 * @param contractFee    the fee to add to the contract data.
	 *
	 * @returns an updated contractData including the added fee.
	 */
	async addContractFee(contractData: ContractData, contractFee: ContractFee): Promise<ContractData>
	{
		// Extract fee details for legibility.
		const { address, satoshis, name, description } = contractFee;

		// Throw an error if the fee is below the dust limit.
		if(satoshis < DUST_LIMIT)
		{
			throw(new Error(`Cannot add the '${name}' fee of ${satoshis} satoshis, as it is below the dust limit (${DUST_LIMIT}).`));
		}

		// Throw an error if there is already an existing fee matching the name and address.
		// TODO: need to compare lockscript, not address which is not normalized
		const existingFee = contractData.fees.find((fee) => (fee.name === name && fee.address === address));
		if(existingFee)
		{
			throw(new Error(`Cannot add the '${name}' fee to '${address}' as a fee for that name and address already exist.`));
		}

		// Add the fee to the contract data. Using sanitized data. Don't send in contractFee.
		contractData.fees.push({ address, satoshis, name, description });

		// Return the updated contract data.
		return contractData;
	}

	/**
	 * Parse a settlement transaction to extract as much data as possible, ending up with
	 * ContractParameters, ContractSettlement and ContractFunding objects, depending on what data
	 * could be retrieved.
	 *
	 * @param settlementTransactionHex   hex string for the settlement transaction
	 *
	 * @throws {Error} when the passed transaction hex cannot be parsed by Libauth.
	 * @throws {SettlementParseError} if the transaction does not have exactly one input.
	 * @throws {SettlementParseError} if the transaction does not have exactly two outputs.
	 * @throws {SettlementParseError} if the unlocking script does not include exactly 6 or 10 input parameters.
	 * @throws {SettlementParseError} if the redeem script does not match expectations for an AnyHedge contract.
	 * @returns ContractParameters, ContractSettlement, and ContractFunding objects. See {@link https://gitlab.com/GeneralProtocols/anyhedge/library/-/blob/development/examples/parse-settlement-transaction.js|examples/parse-settlement-transaction.js} to inspect the data that this function returns.
	 */
	async parseSettlementTransaction(settlementTransactionHex: string): Promise<ParsedSettlementData>
	{
		return parseSettlementTransaction(settlementTransactionHex);
	}
}
