import { debug } from './javascript-util.js';
import { DUST_LIMIT, CONSTRUCTOR_PARAMETER_COUNT, SATS_PER_BCH } from '../constants.js';
import type { ContractFunding, ContractMetadata, ContractParameters, ParsedFunding, ParsedSettlementData, ParsedSettlement } from '../interfaces/index.js';
import { SettlementType } from '../interfaces/index.js';
import type { AuthenticationInstructionPush, Output, AuthenticationInstruction, AuthenticationInstructionMalformed, AuthenticationInstructionMaybeMalformed, AuthenticationInstructionOperation } from '@bitauth/libauth';
import { deriveHdPrivateNodeFromSeed, hexToBin, binToHex, decodeTransactionUnsafe, OpcodesBCH, decodeAuthenticationInstructions, hashTransactionUiOrder } from '@bitauth/libauth';
import { addressToLockScript, buildLockScriptP2SH32, lockScriptToAddress } from './bitcoincash-util.js';
import type { Contract, Transaction, Utxo } from 'cashscript';
import { utils as cashscriptUtils } from 'cashscript';
import { OracleData } from '@generalprotocols/price-oracle';
import { SettlementParseError } from '../errors.js';
import { AnyHedgeArtifacts } from '@generalprotocols/anyhedge-contracts';

// Load support for bigint in JSON
import { JSONStringify } from './json-explicit-bigint.js';

/**
* Helper function to calculate the total sats for a contract (inputs, miner fees, dust cost)
*
* @param contractMetadata   a ContractMetadata object including the sats information for a contract.
*
* @returns the number of total sats that are contained in the contract.
* @private
*/
export const calculateTotalSats = function(contractMetadata: ContractMetadata): bigint
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'calculateTotalSats() <=', arguments ]);

	// Calculate total sats
	const { shortInputInSatoshis, longInputInSatoshis, minerCostInSatoshis } = contractMetadata;
	const totalSats: bigint = shortInputInSatoshis + longInputInSatoshis + minerCostInSatoshis + DUST_LIMIT;

	// Output function result for easier collection of test data.
	debug.result([ 'calculateTotalSats() =>', totalSats ]);

	return totalSats;
};

/**
 * Generates a private key hex string used in the redemption of an AnyHedge contract
 * with the provided address
 *
 * @param address   the contract's address
 *
 * @returns the private key used to redeem the AnyHedge contract
 * @private
 */
export const deriveRedemptionKeyFromAddress = function(address: string): string
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'deriveRedemptionKeyFromAddress() <=', arguments ]);

	// Use the contract's lock script as the wallet seed
	const seed = hexToBin(addressToLockScript(address));

	// Derive an HD node using the seed and extract its private key
	const hdNode = deriveHdPrivateNodeFromSeed(seed, true);
	const privateKey = binToHex(hdNode.privateKey);

	// Output function result for easier collection of test data.
	debug.result([ 'deriveRedemptionKeyFromAddress() =>', privateKey ]);

	return privateKey;
};

/**
 * Convert a CashScript UTXO to a ContractFunding interface.
 *
 * @param coin   CashScript UTXO to be converted.
 *
 * @returns ContractFunding interface.
 * @private
 */
export const contractCoinToFunding = function(coin: Utxo): ContractFunding
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'contractCoinToFunding() <=', arguments ]);

	// Format the CashScript UTXO to match the ContractFunding interface
	const funding: ContractFunding =
	{
		fundingTransactionHash: coin.txid,
		fundingOutputIndex: BigInt(coin.vout),
		fundingSatoshis: coin.satoshis,
	};

	// Output function result for easier collection of test data.
	debug.result([ 'contractCoinToFunding() =>', funding ]);

	return funding;
};

/**
 * Convert a ContractFunding interface to a CashScript UTXO.
 *
 * @param funding   contract funding interface.
 *
 * @returns CashScript UTXO.
 * @private
 */
export const contractFundingToCoin = function(funding: ContractFunding): Utxo
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'contractFundingToCoin() <=', arguments ]);

	// Format the ContractFunding to match the CashScript UTXO interface
	const coin: Utxo =
	{
		txid: funding.fundingTransactionHash,
		vout: Number(funding.fundingOutputIndex),
		satoshis: funding.fundingSatoshis,
	};

	// Output function result for easier collection of test data.
	debug.result([ 'contractFundingToCoin() =>', coin ]);

	return coin;
};

/**
 * @param funding   contract funding.
 *
 * @returns contract funding UTXO's outpoint as a hex string.
 * @private
 */
export const contractFundingToOutpoint = function(funding: ContractFunding): string
{
	return `${funding.fundingTransactionHash}:${funding.fundingOutputIndex}`;
};

/**
 * Build a CashScript Transaction instance for a payout transaction.
 *
 * @param contract              CashScript contract to use for building the transaction.
 * @param contractParameters    contract parameters to use in the payout transaction.
 * @param contractFunding       contract funding to spend from in the payout transaction.
 * @param settlementMessage     oracle price message used in the payout transaction.
 * @param settlementSignature   signature over the oracle message.
 * @param previousMessage       oracle price message used in the payout transaction.
 * @param previousSignature     signature over the oracle message.
 * @param shortPayoutSats       amount to be paid out to the short.
 * @param longPayoutSats        amount to be paid out to the long.
 * @param minerFeeSats          amount to be used for miner fee.
 *
 * @returns a CashScript Transaction for a payout transaction with the correct details.
 * @private
 */
export const buildPayoutTransaction = async function(
	{
		contract,
		contractFunding,
		contractParameters,
		shortPayoutSats,
		longPayoutSats,
		previousMessage,
		previousSignature,
		settlementMessage,
		settlementSignature,
		minerFeeSats,
	}: {
		contract: Contract;
		contractParameters: ContractParameters;
		contractFunding: ContractFunding;
		settlementMessage: string;
		settlementSignature: string;
		previousMessage: string;
		previousSignature: string;
		shortPayoutSats: bigint;
		longPayoutSats: bigint;
		minerFeeSats: bigint;
	},
): Promise<Transaction>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'buildPayoutTransaction() <=', arguments ]);

	// Convert the contract funding to a CashScript UTXO to be used as an input
	const input = contractFundingToCoin(contractFunding);

	// Derive the short and long addresses from the contract parameters.
	const shortAddress = lockScriptToAddress(contractParameters.shortLockScript);
	const longAddress = lockScriptToAddress(contractParameters.longLockScript);

	// Build the payout transaction with locktime specifically set to 0 to override
	// CashScript's default behavior to set locktime to the latest block.
	// NOTE: We set the miner fee manually since cashscript fee estimation is too conservative which causes off-by-one satoshi errors.
	const payoutTransaction = contract.functions
		.payout(
			// Oracle data
			settlementMessage,
			settlementSignature,
			previousMessage,
			previousSignature,
		)
		.from(input)
		.withoutChange()
		.to(shortAddress, shortPayoutSats)
		.to(longAddress, longPayoutSats)
		.withTime(0)
		.withHardcodedFee(minerFeeSats);

	// Output function result for easier collection of test data.
	debug.result([ 'buildPayoutTransaction() =>', payoutTransaction ]);

	return payoutTransaction;
};

/**
 * Estimate the required miner fee to execute a redemption transaction for a contract.
 *
 * @param contract             CashScript contract instance to use in the estimation.
 * @param contractParameters   contract parameters to use in the estimation.
 * @param feeRate              amount of satoshis to pay per byte of transaction data.
 *
 * @returns an estimate of the payout transaction size for this contract.
 * @private
 */
export const estimatePayoutTransactionFee = async function(
	contract: Contract,
	contractParameters: ContractParameters,
	feeRate: number = 1.0,
): Promise<bigint>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'estimatePayoutTransactionFee() <=', arguments ]);

	// Create placeholder oracle data.
	const placeholderOracleMessage = binToHex(await OracleData.createPriceMessage(1, 1, 1, 1));
	const placeholderOracleSignature = '00'.repeat(64);

	// Create a placeholder 10 BCH funding (input/output values do not matter for the size calculation).
	const placeholderFunding: ContractFunding =
	{
		fundingTransactionHash: '00'.repeat(32),
		fundingOutputIndex: BigInt('0'),
		fundingSatoshis: BigInt('10') * SATS_PER_BCH,
	};

	// Send 4 BCH to both short and long (input/output values do not matter for the size calculation).
	const placeholderShortPayoutSats = BigInt('4') * SATS_PER_BCH;
	const placeholderLongPayoutSats = BigInt('4') * SATS_PER_BCH;

	// Use sensible 1000 sats placeholder for the miner fee.
	const placeholderMinerFeeInSats = BigInt('1000');

	// Build the placeholder payout transaction.
	const payoutTransaction = await buildPayoutTransaction(
		{
			contract,
			contractParameters,
			contractFunding: placeholderFunding,
			settlementMessage: placeholderOracleMessage,
			settlementSignature: placeholderOracleSignature,
			previousMessage: placeholderOracleMessage,
			previousSignature: placeholderOracleSignature,
			shortPayoutSats: placeholderShortPayoutSats,
			longPayoutSats: placeholderLongPayoutSats,
			minerFeeSats: placeholderMinerFeeInSats,
		},
	);

	// Set locktime to some non-undefined number to avoid triggering build()'s
	// network query to get current block height. This ensures a correct transaction sizing.
	const locktimeWithSameByteSizeAsCurrent = 800000;
	payoutTransaction.withTime(locktimeWithSameByteSizeAsCurrent);

	// Build the payout transaction as a raw hex string.
	const rawPayoutTransaction = await payoutTransaction.build();

	// The built transaction is a hex string, so we divide its length by 2.
	const transactionSize = rawPayoutTransaction.length / 2;

	// Calculate the transaction fee based on size and fee rate. Ceil eliminates risk of being under by one.
	const transactionFee = BigInt(Math.ceil(transactionSize * feeRate));

	// Output function result for easier collection of test data.
	debug.result([ 'estimatePayoutTransactionFee() =>', transactionFee ]);

	return transactionFee;
};

/**
 * Decodes a script parameter as an integer, whether it is an OP_X or OP_PUSHDATA.
 *
 * @param {AuthenticationInstruction} parameter
 *
 * @returns {bigint}
 * @private
 */
export const decodeParameterAsInteger = function(parameter: AuthenticationInstruction): bigint
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'decodeParameterAsInteger() <=', arguments ]);

	// Attempt to decode first as a data push
	const parameterAsDataPush = parameter as AuthenticationInstructionPush;
	if('data' in parameterAsDataPush)
	{
		// Convert the pushed data to an integer
		const value = cashscriptUtils.decodeInt(parameterAsDataPush.data);

		// Output function result for easier collection of test data.
		debug.result([ 'decodeParameterAsInteger() =>', value ]);

		return value;
	}

	// Attempt to decode as an OP_X number
	const parameterAsOpX = parameter as AuthenticationInstructionOperation;
	if('opcode' in parameterAsOpX)
	{
		const isZeroOpcode = (parameterAsOpX.opcode === OpcodesBCH.OP_0);
		if(isZeroOpcode)
		{
			// Make a simple return value since we know it is zero
			const value = BigInt('0');

			// Output function result for easier collection of test data.
			debug.result([ 'decodeParameterAsInteger() =>', value ]);

			return value;
		}

		const isNonZeroNumericOpcode = (parameterAsOpX.opcode >= OpcodesBCH.OP_1) && (parameterAsOpX.opcode <= OpcodesBCH.OP_16);
		if(isNonZeroNumericOpcode)
		{
			// Calculate the value of op_x from the opcode index.
			// E.g. OP_1 ==> OP_1(81) - OP_1(81) + 1 = 1
			// E.g. OP_16 ==> OP_16(96) - OP_1(81) + 1 = 16
			const value = BigInt(parameterAsOpX.opcode - OpcodesBCH.OP_1 + 1);

			// Output function result for easier collection of test data.
			debug.result([ 'decodeParameterAsInteger() =>', value ]);

			return value;
		}
	}

	// If we haven't been able to parse the instruction yet, then we have an undefined situation
	throw(new Error(`expected an integer as op_x or op_push but got ${parameter}`));
};

/**
 * Decodes a script parameter as the pushed bytes of OP_PUSHDATA.
 *
 * @param {AuthenticationInstruction} parameter
 *
 * @returns {string}
 * @private
 */
export const decodeParameterAsData = function(parameter: AuthenticationInstruction): string
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'decodeParameterAsData() <=', arguments ]);

	// Attempt to decode as a data push
	const parameterAsDataPush = parameter as AuthenticationInstructionPush;
	if('data' in parameterAsDataPush)
	{
		// Convert the data to hex in line with common pattern in this library
		const value = binToHex(parameterAsDataPush.data);

		// Output function result for easier collection of test data.
		debug.result([ 'decodeParameterAsData() =>', value ]);

		return value;
	}

	// If we haven't been able to parse the instruction yet, then we have an undefined situation
	throw(new Error(`expected an op_push with data but got (${typeof parameter}) ${JSONStringify(parameter)}`));
};

/**
 * Confirm that the parameters are not malformed and return the cleanly typed result.
 *
 * @param {AuthenticationInstructionMaybeMalformed[]} parametersMaybeMalformed
 *
 * @returns {AuthenticationInstruction[]}
 * @private
 */
export const confirmParametersNotMalformed = function(parametersMaybeMalformed: AuthenticationInstructionMaybeMalformed[]): AuthenticationInstruction[]
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'confirmParametersNotMalformed() <=', arguments ]);

	// Interpret each instruction as a malformed instruction and throw if any are actually malformed.
	for(const parameter of parametersMaybeMalformed as AuthenticationInstructionMalformed[])
	{
		if(parameter.malformed)
		{
			throw(new Error(`unable to interpret parameter: (${typeof parameter}) ${JSONStringify(parameter)}`));
		}
	}

	// Output function result for easier collection of test data.
	debug.result([ 'confirmParametersNotMalformed() => same as input but confirmed valid' ]);

	return parametersMaybeMalformed as AuthenticationInstruction[];
};

/**
 * Extract the constructor parameters from a redeem script.
 *
 * @param redeemScript   redeem script extracted from a settlement transaction.
 *
 * @throws {Error} if the redeem script does not match expectations for an AnyHedge contract.
 * @returns ContractParameters object.
 * @private
 */
export const extractConstructorParameters = function(redeemScript: Uint8Array): ContractParameters
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'extractConstructorParameters() <=', arguments ]);

	// Parse redeem script
	// Note: the order and type of these can be confirmed by reading artifact.json["constructorInputs"] of the target contract in reverse order
	//       Issue for making this programmatic: https://gitlab.com/GeneralProtocols/anyhedge/library/-/issues/333
	const constructorParametersMaybeMalformed = decodeAuthenticationInstructions(redeemScript)
		.slice(0, CONSTRUCTOR_PARAMETER_COUNT);

	// Confirm parameters are not malformed
	const constructorParameters = confirmParametersNotMalformed(constructorParametersMaybeMalformed);

	// Extract named constructor parameters
	const [
		maturityTimestamp,
		startTimestamp,
		highLiquidationPrice,
		lowLiquidationPrice,
		payoutSats,
		satsForNominalUnitsAtHighLiquidation,
		nominalUnitsXSatsPerBch,
		oraclePublicKey,
		longLockScript,
		shortLockScript,
		enableMutualRedemption,
		longMutualRedeemPublicKey,
		shortMutualRedeemPublicKey,
	] = constructorParameters;

	// Now we know these are all valid AuthenticationInstructions that we can interpret.
	// Convert available data into a ContractParameters object.
	const contractParameters: ContractParameters =
	{
		maturityTimestamp: decodeParameterAsInteger(maturityTimestamp),
		startTimestamp: decodeParameterAsInteger(startTimestamp),
		highLiquidationPrice: decodeParameterAsInteger(highLiquidationPrice),
		lowLiquidationPrice: decodeParameterAsInteger(lowLiquidationPrice),
		payoutSats: decodeParameterAsInteger(payoutSats),
		satsForNominalUnitsAtHighLiquidation: decodeParameterAsInteger(satsForNominalUnitsAtHighLiquidation),
		nominalUnitsXSatsPerBch: decodeParameterAsInteger(nominalUnitsXSatsPerBch),
		oraclePublicKey: decodeParameterAsData(oraclePublicKey),
		longLockScript: decodeParameterAsData(longLockScript),
		shortLockScript: decodeParameterAsData(shortLockScript),
		enableMutualRedemption: decodeParameterAsInteger(enableMutualRedemption),
		longMutualRedeemPublicKey: decodeParameterAsData(longMutualRedeemPublicKey),
		shortMutualRedeemPublicKey: decodeParameterAsData(shortMutualRedeemPublicKey),
	};

	// Output function result for easier collection of test data.
	debug.result([ 'extractConstructorParameters() =>', contractParameters ]);

	return contractParameters;
};

/**
 * Checks whether a passed redeem script hex string belongs to an AnyHedge contract.
 *
 * @param redeemScriptHex   redeem script extracted from a settlement transaction.
 *
 * @returns true if it matches AnyHedge bytecode, false if not.
 * @private
 */
export const isAnyHedgeRedeemScript = function(redeemScriptHex: string): boolean
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'isAnyHedgeRedeemScript() <=', arguments ]);

	// Retrieve artifact object. Note that this is hardcoded for AnyHedge v0.12.
	const artifact = AnyHedgeArtifacts['AnyHedge v0.12'];

	// Convert artifact's ASM to hex bytecode.
	const contractBytecode = binToHex(cashscriptUtils.asmToBytecode(artifact.bytecode));

	// Check that the passed redeem script includes AnyHedge bytecode.
	const redeemScriptIncludesAnyHedgeBytecode = redeemScriptHex.endsWith(contractBytecode);

	// Output function result for easier collection of test data.
	debug.result([ 'isAnyHedgeRedeemScript() =>', redeemScriptIncludesAnyHedgeBytecode ]);

	return redeemScriptIncludesAnyHedgeBytecode;
};

/**
 * Given a list of transaction outputs, and a short and long input script, return
 * the payout satoshis for the short and long parties.
 *
 * @param outputs              list of transaction outputs to parse
 * @param shortLockScriptHex   short lock script
 * @param longLockScriptHex    long lock script
 *
 * @returns object containing short and long satoshis if they could be parsed from the outputs.
 * @throws if the outputs argument does not contain exactly 1 output match for each of short and long
 * @private
 */
export const parseTransactionOutputs = function(
	outputs: Output[],
	shortLockScriptHex: string,
	longLockScriptHex: string,
): { shortPayoutInSatoshis: bigint; longPayoutInSatoshis: bigint }
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'parseTransactionOutputs() <=', arguments ]);

	// Counters for discoveries of short, long, and other lockscripts
	let shortCount = 0;
	let longCount = 0;
	let otherCount = 0;

	// Declare variables for short and long satoshis.
	// Note: the initial values are irrelevant and will either be overwritten or an error will be thrown.
	let shortPayoutInSatoshis = BigInt('-1');
	let longPayoutInSatoshis = BigInt('-1');

	// Loop over all outputs.
	for(const output of outputs)
	{
		// Check all possible matches
		const matchesShort = binToHex(output.lockingBytecode) === shortLockScriptHex;
		const matchesLong = binToHex(output.lockingBytecode) === longLockScriptHex;
		const matchesNeither = (!matchesShort) && (!matchesLong);

		// Note: The code below can incorrectly re-assign short payout and double count matches.
		//       This is intentional to catch all counts. If the counts are not exact, it will throw.

		// Assign the output's satoshis to short satoshis if the lock script matches.
		if(matchesShort)
		{
			shortPayoutInSatoshis = output.valueSatoshis;
			shortCount += 1;
		}

		// Assign the output's satoshis to long satoshis if the lock script matches.
		if(matchesLong)
		{
			longPayoutInSatoshis = output.valueSatoshis;
			longCount += 1;
		}

		//
		if(matchesNeither)
		{
			otherCount += 1;
		}
	}

	// Confirm we found exactly one instance of short and long or else this function has undefined behavior
	const foundUniqueMatchup = (shortCount === 1) && (longCount === 1);
	if(!foundUniqueMatchup)
	{
		throw(new Error(`Expected to find exactly one lockscript of each party but found Short:${shortCount}, Long:${longCount}, Other:${otherCount}`));
	}

	// Assemble the extracted satoshis.
	const extractedSatoshis = { shortPayoutInSatoshis, longPayoutInSatoshis };

	// Output function result for easier collection of test data.
	debug.result([ 'parseTransactionOutputs() =>', extractedSatoshis ]);

	return extractedSatoshis;
};

/**
 * Extract the input parameters for a mutual redemption transaction and build
 * ContractParameters and ContractSettlement objects.
 *
 * @param inputParameters      list of input parameters as PUSH operations.
 * @param transactionHashHex   transaction hash hex string for the settlement transaction.
 * @param outputs              list of transaction outputs.
 * @param funding              contract funding that was already parsed.
 *
 * @throws {SettlementParseError} if the redeem script does not match expectations for an AnyHedge contract.
 * @returns ContractParameters and ContractSettlement objects.
 * @private
 */
export const parseMutualRedemptionTransaction = async function(
	transactionHashHex: string,
	inputParameters: AuthenticationInstruction[],
	outputs: Output[],
	funding: ParsedFunding,
): Promise<ParsedSettlementData>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'parseMutualRedemptionTransaction() <=', arguments ]);

	// Extract mutual redemption redeem script.
	// Note that input arguments are in push (reverse) order compared to the contract.
	/* eslint-disable @typescript-eslint/naming-convention */
	const [
		_longMutualRedeemSignature,
		_hedgeMutualRedeemSignature,
		_functionSelector,
		redeemScript,
	] = inputParameters;
	/* eslint-enable @typescript-eslint/naming-convention */

	// Extract the content of the redeemScript parameter
	const redeemScriptHex = decodeParameterAsData(redeemScript);

	// Check that the redeem script matches AnyHedge bytecode, or throw an error.
	if(!isAnyHedgeRedeemScript(redeemScriptHex))
	{
		throw(new SettlementParseError('Settlement redeem script does not match AnyHedge contract bytecode'));
	}

	// Extract constructor parameters from the redeem script.
	const parameters = extractConstructorParameters(hexToBin(redeemScriptHex));

	// Parse the transaction outputs to determine the short and long payout satoshis.
	// NOTE: Mutual redemptions can pay out to any address, but currently we are only able to determine the payout amounts if they pay out to the contract defined lockscripts.
	const { shortPayoutInSatoshis, longPayoutInSatoshis } = parseTransactionOutputs(outputs, parameters.shortLockScript, parameters.longLockScript);

	// Define settlement data.
	const settlement: ParsedSettlement =
	{
		settlementType: SettlementType.MUTUAL,
		settlementTransactionHash: transactionHashHex,
		shortPayoutInSatoshis,
		longPayoutInSatoshis,
	};

	// Derive the contract's address.
	const contractLockScript = buildLockScriptP2SH32(redeemScriptHex);
	const address = lockScriptToAddress(contractLockScript);

	// Assemble the extracted data.
	const extractedData: ParsedSettlementData = { address, funding, parameters, settlement };

	// Output function result for easier collection of test data.
	debug.result([ 'parseMutualRedemptionTransaction() =>', extractedData ]);

	return extractedData;
};

/**
 * Extract the input parameters for a payout transaction and build
 * ContractParameters and ContractSettlement objects.
 *
 * @param transactionHashHex   transaction hash hex string for the settlement transaction.
 * @param inputParameters      list of input parameters as PUSH operations.
 * @param outputs              list of transaction outputs.
 * @param funding              contract funding that was already parsed.
 *
 * @throws {SettlementParseError} if the redeem script does not match expectations for an AnyHedge contract.
 * @returns ContractParameters and ContractSettlement objects.
 * @private
 */
export const parsePayoutTransaction = async function(
	transactionHashHex: string,
	inputParameters: AuthenticationInstruction[],
	outputs: Output[],
	funding: ParsedFunding,
): Promise<ParsedSettlementData>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'parsePayoutTransaction() <=', arguments ]);

	// Extract payout input parameters.
	// Note that input arguments are in push (reverse) order compared to the contract.
	/* eslint-disable @typescript-eslint/naming-convention */
	const [
		previousSignature,
		previousMessage,
		settlementSignature,
		settlementMessage,
		_functionSelector,
		redeemScript,
	] = inputParameters;
	/* eslint-enable @typescript-eslint/naming-convention */

	// Extract the content of the redeemScript parameter
	const redeemScriptHex = decodeParameterAsData(redeemScript);

	// Check that the redeem script matches AnyHedge bytecode, or throw an error.
	if(!isAnyHedgeRedeemScript(redeemScriptHex))
	{
		throw(new SettlementParseError('Settlement redeem script does not match AnyHedge contract bytecode'));
	}

	// Extract constructor parameters from the redeem script.
	const parameters = extractConstructorParameters(hexToBin(redeemScriptHex));

	// Extract the content of the oracle related parameters
	const settlementMessageHex = decodeParameterAsData(settlementMessage);
	const settlementSignatureHex = decodeParameterAsData(settlementSignature);
	const previousMessageHex = decodeParameterAsData(previousMessage);
	const previousSignatureHex = decodeParameterAsData(previousSignature);

	// Parse the settlement message and convert the javascript safe (32 bit) oracle numbers to bigint
	/* eslint-disable @typescript-eslint/naming-convention */
	const { messageTimestamp: _messageTimestamp, priceValue: _priceValue } = await OracleData.parsePriceMessage(hexToBin(settlementMessageHex));
	const messageTimestamp = BigInt(_messageTimestamp);
	const priceValue = BigInt(_priceValue);
	/* eslint-enable @typescript-eslint/naming-convention */

	// Determine whether this is a maturation or a liquidation.
	const isMaturation = messageTimestamp >= parameters.maturityTimestamp;
	const settlementType = isMaturation ? SettlementType.MATURATION : SettlementType.LIQUIDATION;

	// Parse the transaction outputs to determine the short and long payout satoshis.
	const shortLockScriptHex = parameters.shortLockScript;
	const longLockScriptHex = parameters.longLockScript;
	const { shortPayoutInSatoshis, longPayoutInSatoshis } = parseTransactionOutputs(outputs, shortLockScriptHex, longLockScriptHex);

	// Assemble the settlement data.
	const settlement: ParsedSettlement =
	{
		settlementType,
		settlementTransactionHash: transactionHashHex,
		shortPayoutInSatoshis,
		longPayoutInSatoshis,
		settlementMessage: settlementMessageHex,
		settlementSignature: settlementSignatureHex,
		previousMessage: previousMessageHex,
		previousSignature: previousSignatureHex,
		settlementPrice: priceValue,
	};

	// Derive the contract's address.
	const contractLockScript = buildLockScriptP2SH32(redeemScriptHex);
	const address = lockScriptToAddress(contractLockScript);

	// Assemble the extracted data
	const extractedData: ParsedSettlementData = { address, funding, parameters, settlement };

	// Output function result for easier collection of test data.
	debug.result([ 'parsePayoutTransaction() =>', extractedData ]);

	return extractedData;
};

/**
 * Parse a settlement transaction to extract as much data as possible, ending up with
 * ContractParameters, ContractSettlement and ContractFunding objects, depending on what data
 * could be retrieved.
 *
 * @param settlementTransactionHex   hex string for the settlement transaction
 *
 * @throws {Error} when the passed transaction hex cannot be parsed by Libauth.
 * @throws {SettlementParseError} if the transaction does not have exactly one input.
 * @throws {SettlementParseError} if the transaction does not have exactly two outputs.
 * @throws {SettlementParseError} if the unlocking script does not include exactly 6 or 10 input parameters.
 * @throws {SettlementParseError} if the redeem script does not match expectations for an AnyHedge contract.
 * @returns ContractParameters, ContractSettlement, and ContractFunding objects.
 * @private
 */
export const parseSettlementTransaction = async function(settlementTransactionHex: string): Promise<ParsedSettlementData>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'parseSettlementTransaction() <=', arguments ]);

	// Decode the passed transaction hex and extract the inputs and outputs
	const { inputs, outputs } = decodeTransactionUnsafe(hexToBin(settlementTransactionHex));

	// Settlement transactions can only have a single input and two outputs.
	// Note that mutual redemptions could have any number of inputs/outputs, but the most
	// common mutual redemptions (refund & early maturation) have 1/2, like payouts.
	// So arbitrary payout transactions cannot be parsed using this function.
	if(inputs.length !== 1)
	{
		throw(new SettlementParseError('Settlement transaction does not have exactly one input'));
	}
	// Note: this blocks the library from understanding valid mutual redemptions with arbitrary number of outputs
	if(outputs.length !== 2)
	{
		throw(new SettlementParseError('Settlement transaction does not have exactly two outputs'));
	}

	// Extract the only input.
	const [ input ] = inputs;

	// Assemble the funding details.
	const funding: ParsedFunding =
	{
		fundingTransactionHash: binToHex(input.outpointTransactionHash),
		fundingOutputIndex: BigInt(input.outpointIndex),
	};

	// Extract the only input's unlocking bytecode.
	const { unlockingBytecode } = input;

	// Parse the input bytecode, knowing that unlocking bytecode can only contain data pushes.
	const inputParametersMaybeMalformed = decodeAuthenticationInstructions(unlockingBytecode);

	// Confirm parameters are not malformed
	const inputParameters = confirmParametersNotMalformed(inputParametersMaybeMalformed);

	// Compute the transaction's transaction hash.
	const transactionHashHex = binToHex(hashTransactionUiOrder(hexToBin(settlementTransactionHex)));

	// The input script for a mutual redemption has 2 parameters + function selector + redeem script.
	const MUTUAL_REDEMPTION_INPUT_PARAMETERS_LENGTH = 4;

	// The input script for a payout has 4 parameters + function selector + redeem script.
	const PAYOUT_INPUT_PARAMETERS_LENGTH = 6;

	let parsedSettlementData: ParsedSettlementData;

	if(inputParameters.length === MUTUAL_REDEMPTION_INPUT_PARAMETERS_LENGTH)
	{
		// Parse the mutual redemption transaction.
		parsedSettlementData = await parseMutualRedemptionTransaction(transactionHashHex, inputParameters, outputs, funding);
	}
	else if(inputParameters.length === PAYOUT_INPUT_PARAMETERS_LENGTH)
	{
		// parse the payout transaction.
		parsedSettlementData = await parsePayoutTransaction(transactionHashHex, inputParameters, outputs, funding);
	}
	// Any other number of input parameters means that this is not an AnyHedge transaction.
	else
	{
		throw(new SettlementParseError(`Got ${inputParameters.length} input parameters but expected ${MUTUAL_REDEMPTION_INPUT_PARAMETERS_LENGTH} or ${PAYOUT_INPUT_PARAMETERS_LENGTH}`));
	}

	// Output function result for easier collection of test data.
	debug.result([ 'parseSettlementTransaction() =>', parsedSettlementData ]);

	return parsedSettlementData;
};
