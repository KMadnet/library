import { binToHex, hexToBin } from '@bitauth/libauth';
import debugLogger from 'debug';

// Initialize support for debug message management.
export const debug =
{
	action:	debugLogger('anyhedge:action'),
	object:	debugLogger('anyhedge:object'),
	errors:	debugLogger('anyhedge:errors'),
	params:	debugLogger('anyhedge:params'),
	result:	debugLogger('anyhedge:result'),
};

/**
 * Apply binToHex to all properties of an object.
 *
 * @param object   object with only Uint8Arrays as property values.
 *
 * @returns an object with all property values being hex strings
 * @private
 */
export const propertiesBinToHex = function(
	object: { [key: string]: Uint8Array },
): { [key: string]: string }
{
	// Loop over all key-value pairs in the object, apply binToHex to the values,
	// and add the new key-value pair to a result object.
	return Object.entries(object).reduce((accumulator, [ key, value ]) => ({ ...accumulator, [key]: binToHex(value) }), {});
};

/**
 * Apply hexToBin to all properties of an object
 *
 * @param object   object with only hex strings as property values
 *
 * @returns an object with all property values being Uint8Arrays
 * @private
 */
export const propertiesHexToBin = function(
	object: { [key: string]: string },
): { [key: string]: Uint8Array }
{
	// Loop over all key-value pairs in the object, apply hexToBin to the values,
	// and add the new key-value pair to a result object.
	return Object.entries(object).reduce((accumulator, [ key, value ]) => ({ ...accumulator, [key]: hexToBin(value) }), {});
};

/**
 * Abs for bigint
 *
 * @param {bigint} value
 *
 * @returns {bigint}
 * @throws {Error} for empty value sequence
 * @private
 */
export const bigIntAbs = function(value: bigint): bigint
{
	// pass through non-negative values; negate negative values
	const result = (value >= BigInt('0')) ? value : -value;

	return result;
};

/**
 * Min for bigint
 * Adapted from implementation here: https://stackoverflow.com/a/61324746/377366
 *
 * @param {...bigint[]} values
 *
 * @returns {bigint}
 * @throws {Error} for empty value sequence
 * @private
 */
export const bigIntMin = function(...values: bigint[]): bigint
{
	// error if empty
	if(values.length === 0)
	{
		throw(new Error('min is undefined for an empty sequence'));
	}

	// find and return the min
	const min = values.reduce((currentMin: bigint, value: bigint): bigint => (value < currentMin ? value : currentMin));

	return min;
};

/**
 * Max for bigint
 * Adapted from implementation here: https://stackoverflow.com/a/61324746/377366
 *
 * @param {...bigint[]} values
 *
 * @returns {bigint}
 * @throws {Error} for empty value sequence
 * @private
 */
export const bigIntMax = function(...values: bigint[]): bigint
{
	// error if empty
	if(values.length === 0)
	{
		throw(new Error('max is undefined for an empty sequence'));
	}

	// find and return the max
	const max = values.reduce((currentMax: bigint, value: bigint): bigint => (value > currentMax ? value : currentMax));

	return max;
};
