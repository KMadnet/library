// JSON Stringify with explicit BigInt behavior
const explicitBigIntReplacer = function(_key: string, value: any): any
{
	// only replace explicit bigints with an explicit custom formatted string "int:xyz"
	return (typeof value === 'bigint') ? `int:${value.toString()}` : value;
};

export const JSONStringify = function(data: any): string
{
	// stringify with the explicit bigint replacer and return
	const jsonWithExplicitBigints = JSON.stringify(data, explicitBigIntReplacer);

	return jsonWithExplicitBigints;
};

// JSON Parse with explicit BigInt behavior
const explicitBigIntReviver = function(_key: string, value: any): any
{
	// ONLY attempt to revive explicit bigints, and die if there is some false catch
	// rather than trying to be smart about interpreting it.
	const looksLikeExplicitBigint = (typeof value === 'string') && (value.startsWith('int:'));

	return looksLikeExplicitBigint ? BigInt(value.substring(4, value.length)) : value;
};

export const JSONParse = function(json: string): any
{
	// parse with the explicit bigint reviver and return
	const dataWithExplicitBigints = JSON.parse(json, explicitBigIntReviver);

	return dataWithExplicitBigints;
};
