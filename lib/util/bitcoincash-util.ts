import { hexToBin, cashAddressToLockingBytecode, encodeLockingBytecodeP2sh32, hash160, hash256, decodePrivateKeyWif, encodeCashAddress, CashAddressType, binToHex, isHex, lockingBytecodeToCashAddress, secp256k1, encodeLockingBytecodeP2pkh } from '@bitauth/libauth';
import type { NetworkProvider } from 'cashscript';
import { ElectrumNetworkProvider } from 'cashscript';
import { IncorrectWIFError } from '../errors.js';
import { debug } from './javascript-util.js';

/**
 * Decode a private key into a hex-encoded private key.
 *
 * @param privateKeyWIF   private key WIF string to be decoded.
 *
 * @throws {Error} if the WIF string has an incorrect format.
 * @returns The decoded hex encoded private key.
 * @private
 */
export const decodeWIF = function(privateKeyWIF: string): string
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'decodeWIF() <=', arguments ]);

	// Attempt to decode the private key WIF.
	const decodeResult = decodePrivateKeyWif(privateKeyWIF);

	// If the decode result is a string, it failed, so we throw an error.
	if(typeof decodeResult === 'string')
	{
		throw(new IncorrectWIFError(privateKeyWIF));
	}

	// Extract the result and convert it to hex.
	const privateKey = binToHex(decodeResult.privateKey);

	// Output function result for easier collection of test data.
	debug.result([ 'decodeWIF() =>', privateKey ]);

	return privateKey;
};

/**
 * Derive the compressed public key hex string corresponding to a private key (WIF or hex string)
 *
 * @param privateKeyOrWIF a BCH private key (WIF or hex string)
 *
 * @throws {Error} if the passed WIF string is invalid
 * @returns the compressed public key corresponding to the passed private key (WIF or hex string)
 * @private
 */
export const derivePublicKey = function(privateKeyOrWIF: string): string
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'derivePublicKey() <=', arguments ]);

	// If the passed parameter is a hex string, it is assumed to privateKey be a private,
	// otherwise it's assumed to be a WIF, and gets decoded.
	const privateKey = (isHex(privateKeyOrWIF) ? privateKeyOrWIF : decodeWIF(privateKeyOrWIF));

	// Derive the public key from the private key
	const publicKeyBin = secp256k1.derivePublicKeyCompressed(hexToBin(privateKey));
	if(typeof publicKeyBin === 'string')
	{
		throw new Error(publicKeyBin);
	}
	const publicKey = binToHex(publicKeyBin);

	// Output function result for easier collection of test data.
	debug.result([ 'derivePublicKey() =>', publicKey ]);

	return publicKey;
};

/**
 * Encode a compressed public key into a P2PKH cashaddress
 *
 * @param publicKeyHex   Compressed public key to encode into a P2PKH cashaddress
 *
 * @returns a P2PKH cashaddress corresponding to the passed compressed public key
 * @private
 */
export const encodeCashAddressP2PKH = function(publicKeyHex: string): string
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'encodeCashAddressP2PKH() <=', arguments ]);

	const publicKeyHash = hash160(hexToBin(publicKeyHex));
	const cashAddress = encodeCashAddress('bitcoincash', CashAddressType.p2pkh, publicKeyHash);

	// Output function result for easier collection of test data.
	debug.result([ 'encodeCashAddressP2PKH() =>', cashAddress ]);

	return cashAddress;
};

/**
 * Helper function to construct a P2PKH locking script hex string from a compressed public key hex string
 *
 * @param publicKeyHex   Compressed public key hex string to create a P2PKH locking script for
 *
 * @returns a P2PKH locking script hex string corresponding to the passed compressed public key hex string
 * @private
 */
export const buildLockScriptP2PKH = function(publicKeyHex: string): string
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'buildLockScriptP2PKH() <=', arguments ]);

	const publicKeyHash = hash160(hexToBin(publicKeyHex));

	// Build P2PKH lock script (DUP HASH160 PUSHBYTES_20 hash EQUALVERIFY CHECKSIG)
	const lockScript = binToHex(encodeLockingBytecodeP2pkh(publicKeyHash));

	// Output function result for easier collection of test data.
	debug.result([ 'buildLockScriptP2PKH() =>', lockScript ]);

	return lockScript;
};

// TODO: Deep review of all code related to contract address generation, contract address decoding, encoding lock script, decoding lock script
/**
 * Helper function to construct a P2SH32 locking script hex string from a script bytecode hex string
 *
 * @param {string} scriptBytecodeHex   Bytecode hex string of the script for which to create a P2SH32 locking script hex string
 *
 * @returns a P2SH32 locking script hex string corresponding to the passed script bytecode hex string
 * @private
 */
export const buildLockScriptP2SH32 = function(scriptBytecodeHex: string): string
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'buildLockScriptP2SH32() <=', arguments ]);

	// Hash the lockscript for p2sh32 (using hash256)
	const scriptHashBin = hash256(hexToBin(scriptBytecodeHex));

	// Get the lockscript
	const lockScriptBin = encodeLockingBytecodeP2sh32(scriptHashBin);

	// Convert back to the library's convention of hex
	const lockScriptHex = binToHex(lockScriptBin);

	// Output function result for easier collection of test data.
	debug.result([ 'buildLockScriptP2SH32() =>', lockScriptHex ]);

	return lockScriptHex;
};

/**
 * Helper function to convert an address to a locking script hex string
 *
 * @param address   Address to convert to locking script hex string
 *
 * @returns a locking script hex string corresponding to the passed address
 * @private
 */
export const addressToLockScript = function(address: string): string
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'addressToLockScript() <=', arguments ]);

	const result = cashAddressToLockingBytecode(address);

	// The `cashAddressToLockingBytecode()` call returns an error string OR the correct bytecode
	// so we check if it errors, in which case we throw the error, otherwise return the result
	if(typeof result === 'string') throw(new Error(result));

	const lockScript = binToHex(result.bytecode);

	// Output function result for easier collection of test data.
	debug.result([ 'addressToLockScript() =>', lockScript ]);

	return lockScript;
};

// TODO: Triple check that this works for p2sh32 also
/**
 * Helper function to convert a locking script hex to a cashaddress
 *
 * @param lockScript   lock script to be converted to cashaddress
 *
 * @returns a cashaddress corresponding to the passed lock script
 * @private
 */
export const lockScriptToAddress = function(lockScript: string): string
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'lockScriptToAddress() <=', arguments ]);

	// Convert the lock script to a cashaddress (with bitcoincash: prefix).
	const address = lockingBytecodeToCashAddress(hexToBin(lockScript), 'bitcoincash');

	// A successful conversion will result in a string, unsuccessful will return AddressContents
	if(typeof address !== 'string')
	{
		throw(new Error(`Provided lock script ${lockScript} cannot be converted to address ${JSON.stringify(address)}`));
	}

	// Output function result for easier collection of test data.
	debug.result([ 'lockScriptToAddress() =>', address ]);

	return address;
};

/**
 * Broadcast a raw transaction to the BCH blockchain.
 *
 * @param transactionHex      transaction hex to be broadcasted
 * @param [networkProvider]   custom network provider to use when broadcasting
 *
 * @throws {Error} if the transaction fails to broadcast
 * @returns the transaction ID
 * @private
 */
export const broadcastTransaction = async function(transactionHex: string, networkProvider?: NetworkProvider): Promise<string>
{
	// Use the passed provider or instantiate a new "default" ElectrumNetworkProvider.
	const provider = networkProvider || new ElectrumNetworkProvider();

	// broadcast the raw transaction and retrieve the transaction ID.
	const transactionID = await provider.sendRawTransaction(transactionHex);

	return transactionID;
};
