import type { ContractParameters } from '../interfaces';
import { DUST_LIMIT, MAX_CONTRACT_SATS, MAX_PRICE_ORACLE_UNITS_PER_BCH, MIN_INTEGER_DIVISION_PRECISION_STEPS, MIN_PRICE_ORACLE_UNITS_PER_BCH, SATS_PER_BCH, SCRIPT_INT_MAX } from '../constants.js';
import { ErrorWithProblemAndSolutions } from '../errors.js';

// We use a common error message for cases where the error may be a serious one with the library itself.
const messageForUnactionableErrors = 'Unexpected error. Please file a bug with contract parameters at https://gitlab.com/GeneralProtocols/anyhedge/library.';

// This function is purely a space-saver versus repeating the same code many times in this module.
export const validateIsBigInt = function(value: any, name: string): void
{
	if(!(typeof value === 'bigint'))
	{
		throw(new ErrorWithProblemAndSolutions(
			`${name} must be a bigint to be valid for use in contracts, but got ${typeof value}.`,
			messageForUnactionableErrors,
		));
	}
};

/**
 * Validates minimum integer division precision, which is used in various contract calculations.
 * Precision is defined in the context of integer division when the remainder portion (mod) is discarded.
 * Precision is fine when the relative distance between numbers is large, e.g. (1_000_000 / 7 = 142857 vs. 142857.142).
 * However, calculations become imprecise when the relative distance is small, e.g. (10 / 7 = 1 vs. 1.429, almost 50% error).
 *
 * @param positiveIntegerNumerator         {bigint} positive integer numerator value.
 * @param positiveIntegerDenominator       {bigint} positive integer denominator value.
 * @param minIntegerDivisionPrecisionSteps {bigint} positive integer required steps.
 *
 * @throws {Error} if validation fails.
 * @returns {Promise<void>}
 */
export const validateIntegerDivisionPrecision = async function(
	positiveIntegerNumerator: bigint,
	positiveIntegerDenominator: bigint,
	minIntegerDivisionPrecisionSteps: bigint,
): Promise<void>
{
	// This measurement is only defined when all arguments are positive integers, so we validate that first.

	// Validate numerator as a positive integer.
	validateIsBigInt(positiveIntegerNumerator, 'Numerator');
	if(!(positiveIntegerNumerator >= 1))
	{
		throw(new Error(`Numerator must be a positive integer but got ${positiveIntegerNumerator}.`));
	}

	// Also validate denominator as a positive integer.
	validateIsBigInt(positiveIntegerDenominator, 'Denominator');
	if(!(positiveIntegerDenominator >= 1))
	{
		throw(new Error(`Denominator must be a positive integer but got ${positiveIntegerDenominator}.`));
	}

	// Also validate minimum precision steps as a positive integer.
	validateIsBigInt(minIntegerDivisionPrecisionSteps, 'Division precision steps');
	if(!(minIntegerDivisionPrecisionSteps >= 1))
	{
		throw(new Error(`Division precision steps must be a positive integer but got ${minIntegerDivisionPrecisionSteps}.`));
	}

	// Before calculating steps, we do an additional validation that the numerator is greater than the denominator.
	// The following calculations would still work, but the error would look confusing.
	if(!(positiveIntegerNumerator > positiveIntegerDenominator))
	{
		throw(new Error(`Numerator (${positiveIntegerNumerator}) is unexpectedly not greater than denomintor (${positiveIntegerDenominator})`));
	}

	// Calculate Real valued division steps between numerator and denominator
	const floatDivisionSteps = Number(positiveIntegerNumerator) / Number(positiveIntegerDenominator);

	// Confirm that there are at least as many steps as the minimum requirement
	if(!(floatDivisionSteps >= minIntegerDivisionPrecisionSteps))
	{
		// The relative distance was not large enough, so prepare some helpful numbers for a message and throw an error.

		// Represent the actual precision as a percent string
		const precisionPercentString = (100 * (1 / floatDivisionSteps)).toFixed(2);

		// Represent the minimum precision as a percent string
		const minimumPrecisionPercentString = (100 * (1 / Number(minIntegerDivisionPrecisionSteps))).toFixed(2);

		// Describe the problem in a context-independent way.
		const message = `The division (${positiveIntegerNumerator} / ${positiveIntegerDenominator}) must have at most ${minimumPrecisionPercentString}% error (at least ${minIntegerDivisionPrecisionSteps} steps). Actual worst case precision is ${precisionPercentString}% (${floatDivisionSteps.toFixed(1)} steps).`;

		// Throw an error that describes the problem.
		throw(new Error(message));
	}
};

/**
 * Validates a set of contract parameters against constraints that ensure safe execution and payout.
 *
 * There is a separate formal analysis of AnyHedge that identifies a set of input guarantees
 * for which the output is guaranteed to meet certain safety and policy requirements.
 * The formal analysis can be run from here:
 * https://gitlab.com/unauth/contract-validators/-/blob/main/validate-anyhedge-v12.py?ref_type=heads
 * Here we validate the parameters against the result of the analysis.
 * Some validations may be redundant, which is not considered a problem.
 *
 * @param startPrice {bigint} start price used to create the contract parameters.
 * @param contractParameters {ContractParameters} low level parameters that define a particular AnyHedge contract.
 *
 * @throws {Error} if validation fails.
 * @returns {void}
 */
export const validateContractExecutionSafetyConstraints = async function(startPrice: bigint, contractParameters: ContractParameters): Promise<void>
{
	// This function is long, but intentionally kept in one piece to make it easier to
	// ensure completeness of the safety constraints.

	// Extract values for readability
	const { lowLiquidationPrice, highLiquidationPrice, nominalUnitsXSatsPerBch, satsForNominalUnitsAtHighLiquidation, payoutSats } = contractParameters;

	// ---------------------
	// highLiquidationPrice
	// ---------------------

	// Script-Friendly Integer
	validateIsBigInt(highLiquidationPrice, 'highLiquidationPrice');

	// In formal validation: [C]high_liquidation_price > [I]start_price
	// Minimum: No instant liquidation
	if(!(highLiquidationPrice > startPrice))
	{
		throw(new ErrorWithProblemAndSolutions(
			`High liquidation price must be greater than start price (${startPrice}) in order to avoid instant liquidation, but got ${highLiquidationPrice}.`,
			'Reduce short leverage (increase long price protection).',
		));
	}

	// In formal validation: [C]high_liquidation_price <= MAX_PRICE
	// Maximum: Max price
	if(!(highLiquidationPrice <= MAX_PRICE_ORACLE_UNITS_PER_BCH))
	{
		throw(new ErrorWithProblemAndSolutions(
			`High liquidation price must be at most ${MAX_PRICE_ORACLE_UNITS_PER_BCH}, but got ${highLiquidationPrice}.`,
			'Increase short leverage (reduce long price protection).',
		));
	}

	// ---------------------
	// lowLiquidationPrice
	// ---------------------

	// Script-Friendly Integer
	validateIsBigInt(lowLiquidationPrice, 'lowLiquidationPrice');

	// In formal validation: [C]low_liquidation_price >= MIN_PRICE
	// Minimum: Min price
	if(!(lowLiquidationPrice >= MIN_PRICE_ORACLE_UNITS_PER_BCH))
	{
		throw(new ErrorWithProblemAndSolutions(
			`Low liquidation price must be at least ${MIN_PRICE_ORACLE_UNITS_PER_BCH} but got ${lowLiquidationPrice}.`,
			'Increase long leverage (reduce short price protection).',
		));
	}

	// In formal validation: [C]low_liquidation_price < [I]start_price
	// Maximum: No instant liquidation
	if(!(lowLiquidationPrice < startPrice))
	{
		throw(new ErrorWithProblemAndSolutions(
			`Low liquidation price must be less than start price (${startPrice}) in order to avoid instant liquidation, but got ${lowLiquidationPrice}.`,
			'Reduce long leverage (increase short price protection).',
		));
	}

	// ---------------------
	// nominalUnitsXSatsPerBch
	// ---------------------

	// Script-Friendly Integer
	validateIsBigInt(nominalUnitsXSatsPerBch, 'nominalUnitsXSatsPerBch');

	// In formal validation: [C]nominal_composite >= SATS_PER_BCH
	// Minimum: effectively, nominal units >= 1
	if(!(nominalUnitsXSatsPerBch >= SATS_PER_BCH))
	{
		throw(new ErrorWithProblemAndSolutions(
			`The compound nominal value must be at least 100 000 000, but got ${nominalUnitsXSatsPerBch}.`,
			'Ensure that nominal units is at least 1.',
		));
	}

	// In formal validation: [C]nominal_composite <= VM_MAX_INT
	// Maximum: max integer of the VM
	if(!(nominalUnitsXSatsPerBch <= SCRIPT_INT_MAX))
	{
		throw(new ErrorWithProblemAndSolutions(
			`The compound nominal value must be at most the maximum allowed script integer (${SCRIPT_INT_MAX}), but got ${nominalUnitsXSatsPerBch}.`,
			'Reduce nominal contract value.',
		));
	}

	// ---------------------
	// satsForNominalUnitsAtHighLiquidation
	// ---------------------

	// Script-Friendly Integer
	validateIsBigInt(satsForNominalUnitsAtHighLiquidation, 'satsForNominalUnitsAtHighLiquidation');

	// In formal validation: [C]sats_for_nominal_units_at_high_liquidation >= 0
	// Minimum: 0 is equivalent to shortLeverage=1, a.k.a. simple hedge; >0 is equivalent to having some amount of short leverage
	if(!(satsForNominalUnitsAtHighLiquidation >= 0))
	{
		throw(new ErrorWithProblemAndSolutions(
			`Sats cost for nominal units at high liquidation must be zero or greater, but got ${satsForNominalUnitsAtHighLiquidation}.`,
			messageForUnactionableErrors,
		));
	}

	// Maximum: MAX_SATS is redundant and untestable due to a tighter requirement established in compound constraints below

	// ---------------------
	// payoutSats
	// ---------------------

	// Script-Friendly Integer
	validateIsBigInt(payoutSats, 'payoutSats');

	// In formal validation: [C]total_sats_in >= DUST
	// Minimum: This constraint descends from the combination requirement that transaction payout requires
	// at least dust, combined with the guarantee that the contract will have an additional dust safety
	// amount. That covers 2x dust payouts in the silliest case.
	if(!(payoutSats >= DUST_LIMIT))
	{
		throw(new ErrorWithProblemAndSolutions(
			`Payout satoshis must be at least the dust limit (${DUST_LIMIT}), but got ${payoutSats}.`,
			'Increase nominal contract value. Reduce long leverage (increase short price protection). Reduce short leverage (increase long price protection).',
		));
	}

	// In formal validation: [C]total_sats_in <= MAX_SATS
	// Maximum: MAX_SATS
	if(!(payoutSats <= MAX_CONTRACT_SATS))
	{
		throw(new ErrorWithProblemAndSolutions(
			`Payout satoshis must be at most the maximum allowed policy amount (${MAX_CONTRACT_SATS}), but got ${payoutSats}.`,
			'Reduce nominal contract value. Increase long leverage (reduce short price protection). Increase short leverage (reduce long price protection).',
		));
	}

	// ---------------------
	// Compound relationships
	// ---------------------

	// In formal validation: [C]nominal_composite/[C]high_liquidation_price >= 1
	// This constraint is from back-propagation of [_sats_for_nominal_units_at_redemption >= 1]
	// i.e. Minimum for the intermediate value _sats_for_nominal_units_at_redemption
	// Note: This correctly and intentionally uses truncated bigint division.
	const extremeLowSatsForNominalUnitsAtRedemption = nominalUnitsXSatsPerBch / highLiquidationPrice;
	if(!(extremeLowSatsForNominalUnitsAtRedemption >= 1))
	{
		throw(new ErrorWithProblemAndSolutions(
			`Unexpectedly found nominal composite (${nominalUnitsXSatsPerBch}) and high liquidation price (${highLiquidationPrice}) that are unable to ensure safe payout.`,
			'Increase nominal contract value. Increase short leverage (reduce long price protection).',
		));
	}

	// In formal validation: [C]nominal_composite/[C]low_liquidation_price <= MAX_SATS
	// This constraint is from back-propagation of [_sats_for_nominal_units_at_redemption <= MAX_SATS]
	// i.e. Maximum for the intermediate value _sats_for_nominal_units_at_redemption
	// Note: This correctly and intentionally uses truncated bigint division.
	const extremeHighSatsForNominalUnitsAtRedemption = nominalUnitsXSatsPerBch / lowLiquidationPrice;
	if(!(extremeHighSatsForNominalUnitsAtRedemption <= MAX_CONTRACT_SATS))
	{
		throw(new ErrorWithProblemAndSolutions(
			`Unexpectedly found nominal composite (${nominalUnitsXSatsPerBch}) and low liquidation price (${lowLiquidationPrice}) that are unable to ensure safe payout.`,
			'Reduce nominal contract value. Increase long leverage (reduce short price protection).',
		));
	}

	// In formal validation: [C]nominal_composite/[C]high_liquidation_price - [C]sats_for_nominal_units_at_high_liquidation >= 0
	// This constraint is from back-propagation of [_unsafe_short_sats_out >= 0]
	// i.e. Minimum for the intermediate value _unsafe_short_sats_out
	// Note: This correctly and intentionally uses truncated bigint division.
	const extremeLowUnsafeShortSatsOut = (nominalUnitsXSatsPerBch / highLiquidationPrice) - satsForNominalUnitsAtHighLiquidation;
	if(!(extremeLowUnsafeShortSatsOut >= 0))
	{
		throw(new ErrorWithProblemAndSolutions(
			`Unexpectedly found nominal composite (${nominalUnitsXSatsPerBch}), high liquidation price (${highLiquidationPrice}), and nominal cost at high liquidation (${satsForNominalUnitsAtHighLiquidation}) that are unable to ensure safe payout.`,
			'Increase nominal contract value.',
		));
	}

	// Maximum: MAX_SATS for the _unsafe_short_sats_out intermediate value _unsafe_short_sats_out is trivially redundant with
	// [_sats_for_nominal_units_at_redemption <= MAX_SATS] above, which lacks the additional subtraction of a positive value.

	// In formal validation: [C]total_sats_in - [C]nominal_composite/[C]low_liquidation_price + [C]sats_for_nominal_units_at_high_liquidation >= 0
	// This constraint is from back-propagation of [_unsafe_long_sats_out >= 0]
	// i.e. Minimum for the intermediate value _unsafe_long_sats_out. There is no corresponding maximum.
	// The compound tests are particular and cover specific cases. This one doesn't have a corresponding max,
	// or rather it's already covered by the other constraints if there is one.
	// Note: This correctly and intentionally uses truncated bigint division.
	const extremeLowUnsafeLongSatsOut = payoutSats - (nominalUnitsXSatsPerBch / lowLiquidationPrice) + satsForNominalUnitsAtHighLiquidation;
	if(!(extremeLowUnsafeLongSatsOut >= 0))
	{
		throw(new ErrorWithProblemAndSolutions(
			`Unexpectedly found payoutSats (${payoutSats}), nominal composite (${nominalUnitsXSatsPerBch}), low liquidation price (${lowLiquidationPrice}), and nominal cost at high liquidation (${satsForNominalUnitsAtHighLiquidation}) that are unable to ensure safe payout.`,
			'Reduce nominal contract value. Increase long leverage (reduce short price protection). Increase short leverage (reduce long price protection).',
		));
	}

	// This is a sanity policy added on top of safety constraints.
	// There is a single division in the AnyHedge contract execution. With any integer division, as the two values become
	// closer to each other, the precision of the result becomes worse. This requirement simply forces a certain distance
	// between the values in that AnyHedge division step.
	// In extreme cases, this requirement can cause create more strict min/max relationships than the safety requirements,
	// which is not really a problem.
	try
	{
		// Test division precision
		await validateIntegerDivisionPrecision(
			nominalUnitsXSatsPerBch,
			highLiquidationPrice,
			MIN_INTEGER_DIVISION_PRECISION_STEPS,
		);
	}
	catch(error: any)
	{
		// If we had a division precision error, describe the problem and potential solutions.
		throw(new ErrorWithProblemAndSolutions(
			`The compound nominal value and high liquidation price are too close to each other: ${error.message}`,
			'Increase nominal value. Increase short leverage (reduce long price protection). Use an oracle with a smaller base unit.',
		));
	}
};
