export const DEFAULT_SERVICE_DOMAIN = 'api.anyhedge.com';
export const DEFAULT_SERVICE_PORT = 443;
export const DEFAULT_SERVICE_SCHEME = 'https';
export const DEFAULT_CONTRACT_VERSION = 'AnyHedge v0.12';
export const CONSTRUCTOR_PARAMETER_COUNT = 13;

// See [derivation](https://bitcoincashresearch.org/t/friday-night-challenge-worst-case-dust/1181) for explanation of this number
export const DUST_LIMIT = BigInt('1332');

export const SATS_PER_BCH = BigInt('100000000');

// This is intended to be used for funding transaction size estimation
export const P2PKH_INPUT_SIZE = BigInt('148');

// With upgrade to bigints, the javascript-specific constant below becomes unnecessary.
// However, the description and constant is left for clarity on the issue.
//
// Since May 2022 BCH upgrade, script int max is 2^63-1. However, javascript integers become unreliable around 53 bits.
// For example, 2^63-1 in javascript results in a number that is *larger* than the correct value.
// The largest javascript integer that is smaller than the actual limit is 9223372036854775000.
// This number must only be used for strict limit tests on numbers that will not be used in calculations.
// Documented in constraints diagram: "X = MAX_INT" (this value is a strictly safer derivative of the diagram constraint)
// export const JAVASCRIPT_FRIENDLY_SCRIPT_INT_HARD_MAX = 9223372036854775000;

// With upgrade to bigints, the javascript-specific constant below becomes unnecessary.
// However, the description and constant is left for clarity on the issue.
//
// Javascript can retain integer precision in calculations up to about 53 bits without bigint.
// It is important for this library to have integer-precision in calculations so that the outcome of contract
// simulations will match the numerical results of the VM. A better solution is to use bigint eventually.
// Therefore, we limit script numbers that will be used in calculations, specifically integer divisions, to 53 bits,
// still a large number > 10^15.
// export const JAVASCRIPT_FRIENDLY_SCRIPT_INT_MAX = 2 ** 53;

// Maximum script integer reference, especially for validation purposes. 2**63 -1
export const SCRIPT_INT_MAX = BigInt('9223372036854775807');

// Default minimum division precision in terms of the number of steps required between (numerator / denominator).
// See util/constraints-validation-utils.ts for a complete explanation.
// It must satisfy these common-sense constraints:
//     Min: X >= 1
//     Max: None. Too-large values will just cause all contracts to fail creation.
export const MIN_INTEGER_DIVISION_PRECISION_STEPS = BigInt('500');

// Maximum satoshis allowed in a contract.
// This is not strictly a safety constraint, but allows for more meaningful reasoning among other safety constraints.
// It must satisfy these safety constraints:
//     Min: X >= DUST
//     Max: X <= some practically reasonable number < 21 million (maximum BCH circulation)
export const MAX_CONTRACT_SATS = BigInt('100000') * SATS_PER_BCH;

// Minimum and maximum (2**31-1) price allowed according to Price Oracle specification:
// https://gitlab.com/GeneralProtocols/priceoracle/specification/-/blob/master/README.md?ref_type=heads#price-messages
export const MIN_PRICE_ORACLE_UNITS_PER_BCH = BigInt('1');
export const MAX_PRICE_ORACLE_UNITS_PER_BCH = BigInt('2147483647');

// Booleans in script are just numbers where 0 is false and other numbers are true.
export const VM_1_AS_TRUE = BigInt('1');
export const VM_FALSE = BigInt('0');
