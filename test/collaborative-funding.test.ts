/* eslint-disable max-nested-callbacks */
/* eslint-disable no-param-reassign */

// Import testing libraries and utilities
import { evaluateProgram, loadDefaultContractData, createProgramP2PKH } from './test-util.js';

// Import Bitcoin Cash related interfaces
import { AuthenticationErrorCommon, binToHex } from '@bitauth/libauth';
import type { Utxo } from 'cashscript';
import { bigIntMax } from '../lib/util/javascript-util.js';

// Import utility function to handle oracle data.
import { OracleData } from '@generalprotocols/price-oracle';

// Import AnyHedge library
import type { ContractCreationParameters, ContractData, UnsignedFundingProposal } from '../lib/index.js';
import { AnyHedgeManager } from '../lib/index.js';
import { DUST_LIMIT, VM_1_AS_TRUE } from '../lib/constants.js';

// Import fixture data
import { TAKER_SIDE, MAKER_SIDE, SHORT_ADDRESS, LONG_ADDRESS, SHORT_PUBKEY, LONG_PUBKEY, SHORT_WIF, LONG_WIF, ORACLE_PUBKEY, ORACLE_WIF, DEFAULT_NOMINAL_UNITS, DEFAULT_START_PRICE, START_TIMESTAMP, DEFAULT_DURATION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_FEE_ADDRESS } from './fixture/constants.js';
import { calculateRequiredFundingSatoshis } from '../lib/util/funding-util.js';

// Load an AnyHedge manager.
const loadContractManager = function(): AnyHedgeManager
{
	// Set up instance of AnyHedgeManager
	const manager = new AnyHedgeManager();

	return manager;
};

interface ProposalFixture
{
	short: UnsignedFundingProposal;
	long: UnsignedFundingProposal;
}

// Generate default funding proposals for a contract
const generateProposalsFor = function(contractData: ContractData): ProposalFixture
{
	// Calculate the total required satoshis
	const totalRequiredSatoshis = calculateRequiredFundingSatoshis(contractData);

	// Have the short pay *only* their input satoshis, while the long pays all the rest (inc fees)
	const { shortInputInSatoshis } = contractData.metadata;
	const longInputSatoshis = totalRequiredSatoshis - shortInputInSatoshis;

	// Create a dummy UTXO with enough value to pay for the short side.
	const shortUtxo: Utxo =
	{
		txid: '0000000000000000000000000000000000000000000000000000000000000001',
		vout: 0,
		satoshis: shortInputInSatoshis,
	};

	// Create a dummy UTXO with enough value to pay for the long side + all fees.
	const longUtxo: Utxo =
	{
		txid: '0000000000000000000000000000000000000000000000000000000000000002',
		vout: 0,
		satoshis: longInputSatoshis,
	};

	// Create two proposal objects from that data
	const proposals: ProposalFixture =
	{
		short:
		{
			contractData: contractData,
			utxo: shortUtxo,
		},
		long:
		{
			contractData: contractData,
			utxo: longUtxo,
		},
	};

	return proposals;
};

describe('completeFundingProposal()', () =>
{
	let manager: AnyHedgeManager;
	let proposalsWithFees: ProposalFixture;
	let proposalsWithoutFees: ProposalFixture;

	beforeEach(async () =>
	{
		// Load a contract manager
		manager = loadContractManager();

		// Load the default contract data
		const contractDataWithoutFee = await loadDefaultContractData(manager);

		// Generate default proposals for contractData that excludes fees
		proposalsWithoutFees = generateProposalsFor(contractDataWithoutFee);

		// Calculate the satoshi for funding as 0.3% of total sats or DUST, whichever is larger.
		const simulatedFeeSatoshis = BigInt(Math.ceil(Number(contractDataWithoutFee.parameters.payoutSats) * 0.003));
		const feeSatoshis = bigIntMax(simulatedFeeSatoshis, DUST_LIMIT);

		// Load contract data that includes fees.
		const contractDataWithFee: ContractData =
		{
			...contractDataWithoutFee,
			fees:
			[
				{
					name: 'test',
					description: '',
					address: DEFAULT_FEE_ADDRESS,
					satoshis: feeSatoshis,
				},
			],
		};

		// Generate default proposals for contractData that includes fees
		proposalsWithFees = generateProposalsFor(contractDataWithFee);
	});

	test('should fail when the proposals do not provide enough satoshis', async () =>
	{
		// Generate signed proposals, intentionally using the "long" UTXO twice, which does not contain enough satoshis
		const signedShortProposal = await manager.signFundingProposal(SHORT_WIF, proposalsWithFees.long);
		const signedLongProposal = await manager.signFundingProposal(LONG_WIF, proposalsWithFees.long);

		// Attempt to complete the funding proposal
		const completeFundingProposalCall = manager.completeFundingProposal(signedShortProposal, signedLongProposal);

		// Expect the call to fail
		await expect(completeFundingProposalCall).rejects.toThrow(/should be EXACTLY the required satoshis/);
	});

	test('should fail when the proposals provide too many satoshis', async () =>
	{
		// Generate signed proposals, intentionally using the "short" UTXO twice, which contains too many satoshis
		const signedShortProposal = await manager.signFundingProposal(SHORT_WIF, proposalsWithFees.short);
		const signedLongProposal = await manager.signFundingProposal(LONG_WIF, proposalsWithFees.short);

		// Attempt to complete the funding proposal
		const completeFundingProposalCall = manager.completeFundingProposal(signedShortProposal, signedLongProposal);

		// Expect the call to fail
		await expect(completeFundingProposalCall).rejects.toThrow(/should be EXACTLY the required satoshis/);
	});

	test('should fail when the proposals are for different contracts', async () =>
	{
		const VALID_ORACLE_MESSAGE = await OracleData.createPriceMessage(Number(START_TIMESTAMP) + 10, 1, 1, Number(DEFAULT_START_PRICE));
		const VALID_ORACLE_SIGNATURE = await OracleData.signMessage(VALID_ORACLE_MESSAGE, ORACLE_WIF);

		// Create a contract with 10 seconds added to the default start timestamp
		const creationParameters: ContractCreationParameters =
		{
			takerSide: TAKER_SIDE,
			makerSide: MAKER_SIDE,
			oraclePublicKey: ORACLE_PUBKEY,
			shortPayoutAddress: SHORT_ADDRESS,
			longPayoutAddress: LONG_ADDRESS,
			nominalUnits: DEFAULT_NOMINAL_UNITS,
			startingOracleMessage: binToHex(VALID_ORACLE_MESSAGE),
			startingOracleSignature: binToHex(VALID_ORACLE_SIGNATURE),
			maturityTimestamp: START_TIMESTAMP + DEFAULT_DURATION,
			isSimpleHedge: VM_1_AS_TRUE,
			highLiquidationPriceMultiplier: DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
			lowLiquidationPriceMultiplier: 1 - DEFAULT_VOLATILITY_PROTECTION,
			enableMutualRedemption: VM_1_AS_TRUE,
			shortMutualRedeemPublicKey: SHORT_PUBKEY,
			longMutualRedeemPublicKey: LONG_PUBKEY,
		};
		const contractData = await manager.createContract(creationParameters);

		// Generate proposals for this non-default contract
		const proposals = generateProposalsFor(contractData);

		// Generate signed proposals, intentionally using two separate contract data.
		const signedShortProposal = await manager.signFundingProposal(SHORT_WIF, proposals.short);
		const signedLongProposal = await manager.signFundingProposal(LONG_WIF, proposalsWithFees.long);

		// Attempt to complete the funding proposal
		const completeFundingProposalCall = manager.completeFundingProposal(signedShortProposal, signedLongProposal);

		// Expect the call to fail
		await expect(completeFundingProposalCall).rejects.toThrow(/Funding proposals do not use the same contract data/);
	});

	test('should fail when proposal is changed after signing', async () =>
	{
		// Generate signed proposals for both the short and the long sides
		const signedShortProposal = await manager.signFundingProposal(SHORT_WIF, proposalsWithFees.short);
		const signedLongProposal = await manager.signFundingProposal(LONG_WIF, proposalsWithFees.long);

		// Shuffle some satoshis around in the provided UTXOs.
		signedShortProposal.utxo.satoshis += BigInt('10');
		signedLongProposal.utxo.satoshis -= BigInt('10');

		// Attempt to complete the funding proposal
		const transactionHex = await manager.completeFundingProposal(signedShortProposal, signedLongProposal);

		// Create Libauth Authentication Programs to test the transaction.
		const shortProgram = await createProgramP2PKH(transactionHex, signedShortProposal.publicKey, signedShortProposal.utxo.satoshis, 0);
		const longProgram = await createProgramP2PKH(transactionHex, signedLongProposal.publicKey, signedLongProposal.utxo.satoshis, 1);

		// Evaluate the programs
		const shortResult = evaluateProgram(shortProgram);
		const longResult = evaluateProgram(longProgram);

		// Check that the short program fails on the signature check
		expect(shortResult.result).toEqual(AuthenticationErrorCommon.nonNullSignatureFailure);
		expect(longResult.result).toEqual(AuthenticationErrorCommon.nonNullSignatureFailure);
	});

	test('should fail when one proposal includes a fee and the other does not', async () =>
	{
		// Generate signed proposals for both the short and the long sides
		// We intentionally use a proposal without fee included for one of the proposals
		const signedShortProposal = await manager.signFundingProposal(SHORT_WIF, proposalsWithoutFees.short);
		const signedLongProposal = await manager.signFundingProposal(LONG_WIF, proposalsWithFees.long);

		// Attempt to complete the funding proposal
		const completeFundingProposalCall = manager.completeFundingProposal(signedShortProposal, signedLongProposal);

		// Expect the call to fail
		await expect(completeFundingProposalCall).rejects.toThrow(/Funding proposals do not use the same contract data/);
	});

	test('should fund successfully when both parties use the same contract data and contribute enough satoshis', async () =>
	{
		// Generate signed proposals for both the short and the long sides
		const signedShortProposal = await manager.signFundingProposal(SHORT_WIF, proposalsWithFees.short);
		const signedLongProposal = await manager.signFundingProposal(LONG_WIF, proposalsWithFees.long);

		// Attempt to complete the funding proposal
		const transactionHex = await manager.completeFundingProposal(signedShortProposal, signedLongProposal);

		// Create Libauth Authentication Programs to test the transaction.
		const shortProgram = await createProgramP2PKH(transactionHex, signedShortProposal.publicKey, signedShortProposal.utxo.satoshis, 0);
		const longProgram = await createProgramP2PKH(transactionHex, signedLongProposal.publicKey, signedLongProposal.utxo.satoshis, 1);

		// Evaluate the programs
		const shortResult = evaluateProgram(shortProgram);
		const longResult = evaluateProgram(longProgram);

		// Check that the programs evaluated successfully
		expect(shortResult.result).toBe(true);
		expect(longResult.result).toBe(true);
	});

	test('should fund successfully when no fee is included', async () =>
	{
		// Generate signed proposals for both the short and the long sides
		const signedShortProposal = await manager.signFundingProposal(SHORT_WIF, proposalsWithoutFees.short);
		const signedLongProposal = await manager.signFundingProposal(LONG_WIF, proposalsWithoutFees.long);

		// Attempt to complete the funding proposal
		const transactionHex = await manager.completeFundingProposal(signedShortProposal, signedLongProposal);

		// Create Libauth Authentication Programs to test the transaction.
		const shortProgram = await createProgramP2PKH(transactionHex, signedShortProposal.publicKey, signedShortProposal.utxo.satoshis, 0);
		const longProgram = await createProgramP2PKH(transactionHex, signedLongProposal.publicKey, signedLongProposal.utxo.satoshis, 1);

		// Evaluate the programs
		const shortResult = evaluateProgram(shortProgram);
		const longResult = evaluateProgram(longProgram);

		// Check that the programs evaluated successfully
		expect(shortResult.result).toBe(true);
		expect(longResult.result).toBe(true);
	});
});
