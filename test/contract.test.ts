/* eslint-disable no-console */
/* eslint-disable max-nested-callbacks */
/* eslint-disable no-param-reassign */

// Import testing libraries and utilities
import { jest } from '@jest/globals';
import { createFakeBroadcastWithFakeLocktime, createProgram, programReturnsExpectedResult } from './test-util.js';

// Import Bitcoin Cash related interfaces
import type { AuthenticationProgramBCH } from '@bitauth/libauth';
import { AuthenticationErrorCommon, binToHex, hexToBin, flattenBinArray } from '@bitauth/libauth';

// Import AnyHedge library
import type { ContractData, ContractAutomatedPayout, ContractCreationParameters, ContractSettlementParameters, AutomatedPayoutParameters } from '../lib/index.js';
import { VM_1_AS_TRUE } from '../lib/constants.js';
import { AnyHedgeManager } from '../lib/index.js';

// Import fixture data
import { TAKER_SIDE, MAKER_SIDE, ORACLE_PUBKEY, ORACLE_WIF, SHORT_ADDRESS, LONG_ADDRESS, SHORT_PUBKEY, LONG_PUBKEY, START_TIMESTAMP, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER, CONTRACT_FUNDING_10M_BCH, MATURITY_TIMESTAMP, DEFAULT_NOMINAL_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_LIQUIDATION_TIMESTAMP, DEFAULT_LIQUIDATION_PRICE, DEFAULT_SHORT_PAYOUT, DEFAULT_LONG_PAYOUT, DEFAULT_DURATION } from './fixture/constants.js';
import { liquidateFixtures, matureFixtures } from './fixture/contract.fixture.js';

// Import price oracle library.
import { OracleData } from '@generalprotocols/price-oracle';

// Load contract manager and swaps out the broadcast function
const loadContractManager = function(fakeLocktime?: number): AnyHedgeManager
{
	// Set up instance of AnyHedgeManager
	const manager = new AnyHedgeManager();

	// Stub the broadcast function to return the built transaction rather than broadcasting it
	// Also sets a provided fake locktime
	const fakeBroadcast = createFakeBroadcastWithFakeLocktime(fakeLocktime);
	jest.spyOn(manager, 'broadcastTransaction').mockImplementation(fakeBroadcast);

	return manager;
};

// Create contract data for specified nominal hedge units, start price and protection
// Uses constant fixture values for all other parameters of manager.create()
const loadContractData = async function(
	manager: AnyHedgeManager,
	nominalUnits: number,
	startPrice: bigint,
	volatilityProtection: number,
	maxPriceIncrease: number,
): Promise<ContractData>
{
	const VALID_ORACLE_MESSAGE = await OracleData.createPriceMessage(Number(START_TIMESTAMP), 1, 1, Number(startPrice));
	const VALID_ORACLE_SIGNATURE = await OracleData.signMessage(VALID_ORACLE_MESSAGE, ORACLE_WIF);

	const creationParameters: ContractCreationParameters =
	{
		takerSide: TAKER_SIDE,
		makerSide: MAKER_SIDE,
		oraclePublicKey: ORACLE_PUBKEY,
		shortPayoutAddress: SHORT_ADDRESS,
		longPayoutAddress: LONG_ADDRESS,
		nominalUnits: nominalUnits,
		startingOracleMessage: binToHex(VALID_ORACLE_MESSAGE),
		startingOracleSignature: binToHex(VALID_ORACLE_SIGNATURE),
		maturityTimestamp: START_TIMESTAMP + DEFAULT_DURATION,
		isSimpleHedge: VM_1_AS_TRUE,
		highLiquidationPriceMultiplier: 1 + maxPriceIncrease,
		lowLiquidationPriceMultiplier: 1 - volatilityProtection,
		enableMutualRedemption: VM_1_AS_TRUE,
		shortMutualRedeemPublicKey: SHORT_PUBKEY,
		longMutualRedeemPublicKey: LONG_PUBKEY,
	};

	const contractData = await manager.createContract(creationParameters);

	return contractData;
};

// Define an interface for the return type of runLiquidate()/runMature()
interface ProgramAndAutomatedSettlement
{
	program: AuthenticationProgramBCH;
	settlement: ContractAutomatedPayout;
}

// Run the liquidate() function for a specified contract with specified settlement message
// and returns a libauth Authentication Program.
// Uses constant fixture values for all other parameters
const runLiquidate = async function(
	manager: AnyHedgeManager,
	contractData: ContractData,
	settlementMessage: Uint8Array,
	settlementSignature: Uint8Array,
	previousMessage: Uint8Array,
	previousSignature: Uint8Array,
): Promise<ProgramAndAutomatedSettlement>
{
	const liquidationParameters: ContractSettlementParameters =
	{
		oraclePublicKey: ORACLE_PUBKEY,
		settlementMessage: binToHex(settlementMessage),
		settlementSignature: binToHex(settlementSignature),
		previousMessage: binToHex(previousMessage),
		previousSignature: binToHex(previousSignature),
		contractFunding: CONTRACT_FUNDING_10M_BCH,
		contractMetadata: contractData.metadata,
		contractParameters: contractData.parameters,
	};

	const settlement = await manager.liquidateContractFunding(liquidationParameters);

	// Create Authentication Program that can be evaluated with libauth
	const program = await createProgram(manager, contractData, settlement.settlementTransactionHash, CONTRACT_FUNDING_10M_BCH.fundingSatoshis);

	return { program, settlement };
};

// Run the mature() function for a specified contract with specified settlement message
// and returns a libauth Authentication Program.
// Uses constant fixture values for all other parameters
const runMature = async function(
	manager: AnyHedgeManager,
	contractData: ContractData,
	settlementMessage: Uint8Array,
	settlementSignature: Uint8Array,
	previousMessage: Uint8Array,
	previousSignature: Uint8Array,
): Promise<ProgramAndAutomatedSettlement>
{
	const maturationParameters: ContractSettlementParameters =
	{
		oraclePublicKey: ORACLE_PUBKEY,
		settlementMessage: binToHex(settlementMessage),
		settlementSignature: binToHex(settlementSignature),
		previousMessage: binToHex(previousMessage),
		previousSignature: binToHex(previousSignature),
		contractFunding: CONTRACT_FUNDING_10M_BCH,
		contractMetadata: contractData.metadata,
		contractParameters: contractData.parameters,
	};

	const settlement = await manager.matureContractFunding(maturationParameters);

	// Create Authentication Program that can be evaluated with libauth
	const program = await createProgram(manager, contractData, settlement.settlementTransactionHash, CONTRACT_FUNDING_10M_BCH.fundingSatoshis);

	return { program, settlement };
};

// Run the payout() function for a specified contract with specified settlement message and
// payout amounts, and returns a libauth Authentication Program.
// Uses constant fixture values for all other parameters
const runPayout = async function(
	manager: AnyHedgeManager,
	contractData: ContractData,
	settlementMessage: Uint8Array,
	settlementSignature: Uint8Array,
	previousMessage: Uint8Array,
	previousSignature: Uint8Array,
	shortPayoutInSatoshis: bigint,
	longPayoutInSatoshis: bigint,
): Promise<AuthenticationProgramBCH>
{
	const payoutParameters: AutomatedPayoutParameters =
	{
		oraclePublicKey: ORACLE_PUBKEY,
		settlementMessage: binToHex(settlementMessage),
		settlementSignature: binToHex(settlementSignature),
		previousMessage: binToHex(previousMessage),
		previousSignature: binToHex(previousSignature),
		shortPayoutSats: shortPayoutInSatoshis,
		longPayoutSats: longPayoutInSatoshis,
		contractFunding: CONTRACT_FUNDING_10M_BCH,
		contractParameters: contractData.parameters,
		minerFeeSats: BigInt('1000'),
	};

	const payoutTxHex = await manager.automatedPayout(payoutParameters);

	// Create Authentication Program that can be evaluated with libauth
	const program = createProgram(manager, contractData, payoutTxHex, CONTRACT_FUNDING_10M_BCH.fundingSatoshis);

	return program;
};

// Specify the type of the runLiquidate() or runMature() functions above, so
// it can be passed into the runFixtureTest() function
type RunMatureOrLiquidateFunction = (
	manager: AnyHedgeManager,
	contractData: ContractData,
	settlementMessage: Uint8Array,
	settlementSignature: Uint8Array,
	previousMessage: Uint8Array,
	previousSignature: Uint8Array,
) => Promise<ProgramAndAutomatedSettlement>;

// Runs liquidation/maturation test for a specific fixture
const runFixtureTest = async function(
	runMatureOrLiquidate: RunMatureOrLiquidateFunction,
	description: string,
	nominalUnits: number,
	startPrice: bigint,
	volatilityProtection: number,
	maxPriceIncrease: number,
	messageTimestamp: bigint,
	settlementPrice: bigint,
	shortPayoutInSatoshis: bigint,
	longPayoutInSatoshis: bigint,
	expectedResult: (string | true),
): Promise<void>
{
	test(description, async () =>
	{
		// Set up test contract
		const manager = loadContractManager();
		const contractData = await loadContractData(manager, nominalUnits, startPrice, volatilityProtection, maxPriceIncrease);

		// Set up test settlement message + signature
		const settlementMessage = await OracleData.createPriceMessage(Number(messageTimestamp), 1, 2, Number(settlementPrice));
		const settlementSignature = await OracleData.signMessage(settlementMessage, ORACLE_WIF);

		// Set up test previous message + signature
		const previousMessage = await OracleData.createPriceMessage(Number(messageTimestamp) - 10, 1, 1, Number(settlementPrice));
		const previousSignature = await OracleData.signMessage(previousMessage, ORACLE_WIF);

		// Execute the test using the payout function (does not perform checks so will not throw preemptively)
		const payoutProgram = await runPayout(
			manager,
			contractData,
			settlementMessage,
			settlementSignature,
			previousMessage,
			previousSignature,
			shortPayoutInSatoshis,
			longPayoutInSatoshis,
		);

		programReturnsExpectedResult(payoutProgram, expectedResult, 'payoutProgram');

		if(expectedResult === true)
		{
			// Execute the test using the liquidate or mature function
			const { program: matureOrLiquidateProgram, settlement } = await runMatureOrLiquidate(
				manager,
				contractData,
				settlementMessage,
				settlementSignature,
				previousMessage,
				previousSignature,
			);

			programReturnsExpectedResult(matureOrLiquidateProgram, expectedResult, 'matureOrLiquidateProgram');

			// Check that the returned settlement contains the expected data
			expect(settlement.shortPayoutInSatoshis).toEqual(shortPayoutInSatoshis);
			expect(settlement.longPayoutInSatoshis).toEqual(longPayoutInSatoshis);
			expect(settlement.settlementMessage).toEqual(binToHex(settlementMessage));
			expect(settlement.settlementSignature).toEqual(binToHex(settlementSignature));
		}
		else
		{
			// Check that liquidate/mature function call fails since it performs checks before sending
			// so it should throw preemptively
			await expect(
				() => runMatureOrLiquidate(manager, contractData, settlementMessage, settlementSignature, previousMessage, previousSignature),
			).rejects.toThrow();
		}
	});
};

// Test all fixture-based liquidation and maturation tests
await Promise.all(liquidateFixtures.map(async (fixture) => runFixtureTest(runLiquidate, ...fixture)));
await Promise.all(matureFixtures.map(async (fixture) => runFixtureTest(runMature, ...fixture)));

// Test additional cases that could not be included in the standardized fixtures
describe('contract tests', () =>
{
	test('should liquidate with default parameters (sanity check)', async () =>
	{
		// Set up test contract
		const manager = loadContractManager();
		const contractData = await loadContractData(manager, DEFAULT_NOMINAL_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER);

		// Set up test oracle data
		const settlementMessage = await OracleData.createPriceMessage(Number(DEFAULT_LIQUIDATION_TIMESTAMP), 1, 2, Number(DEFAULT_LIQUIDATION_PRICE));
		const settlementSignature = await OracleData.signMessage(settlementMessage, ORACLE_WIF);
		const previousMessage = await OracleData.createPriceMessage(Number(DEFAULT_LIQUIDATION_TIMESTAMP), 1, 1, Number(DEFAULT_LIQUIDATION_PRICE));
		const previousSignature = await OracleData.signMessage(previousMessage, ORACLE_WIF);

		// check that liquidate() function does not throw and the program evaluates successfully
		const { program } = await runLiquidate(manager, contractData, settlementMessage, settlementSignature, previousMessage, previousSignature);

		programReturnsExpectedResult(program, true, 'matureOrLiquidateProgram-liquidate');
	});

	test('should mature with default parameters (sanity check)', async () =>
	{
		// Set up test contract
		const manager = loadContractManager();
		const contractData = await loadContractData(manager, DEFAULT_NOMINAL_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER);

		// Set up test oracle data
		const settlementMessage = await OracleData.createPriceMessage(Number(MATURITY_TIMESTAMP), 1, 2, Number(DEFAULT_LIQUIDATION_PRICE));
		const settlementSignature = await OracleData.signMessage(settlementMessage, ORACLE_WIF);
		const previousMessage = await OracleData.createPriceMessage(Number(MATURITY_TIMESTAMP) - 10, 1, 1, Number(DEFAULT_LIQUIDATION_PRICE));
		const previousSignature = await OracleData.signMessage(previousMessage, ORACLE_WIF);

		// check that mature() function does not throw and the program evaluates successfully
		const { program } = await runMature(manager, contractData, settlementMessage, settlementSignature, previousMessage, previousSignature);

		programReturnsExpectedResult(program, true, 'matureOrLiquidateProgram-mature');
	});

	test('should fail when previous message is at or after maturity', async () =>
	{
		// Set up test contract
		const manager = loadContractManager();
		const contractData = await loadContractData(manager, DEFAULT_NOMINAL_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER);

		// Set up test oracle data
		const settlementMessage = await OracleData.createPriceMessage(Number(MATURITY_TIMESTAMP) + 10, 1, 2, Number(DEFAULT_LIQUIDATION_PRICE));
		const settlementSignature = await OracleData.signMessage(settlementMessage, ORACLE_WIF);
		const previousMessage = await OracleData.createPriceMessage(Number(MATURITY_TIMESTAMP), 1, 1, Number(DEFAULT_LIQUIDATION_PRICE));
		const previousSignature = await OracleData.signMessage(previousMessage, ORACLE_WIF);

		// check that mature() function throws as it makes this check preemptively
		await expect(
			() => runMature(manager, contractData, settlementMessage, settlementSignature, previousMessage, previousSignature),
		).rejects.toThrow();

		// Execute the transaction through the payout() function which does not do any checks
		const program = await runPayout(
			manager,
			contractData,
			settlementMessage,
			settlementSignature,
			previousMessage,
			previousSignature,
			DEFAULT_SHORT_PAYOUT,
			DEFAULT_LONG_PAYOUT,
		);

		programReturnsExpectedResult(program, AuthenticationErrorCommon.failedVerify, 'payoutProgram');
	});

	test('should fail when settlement signature does not match settlement message', async () =>
	{
		// Set up test contract
		const manager = loadContractManager();
		const contractData = await loadContractData(manager, DEFAULT_NOMINAL_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER);

		// Intentionally create a signature over a different message than what is used
		// in the transaction, so the datasig check in the contract should fail
		const settlementMessage = await OracleData.createPriceMessage(Number(DEFAULT_LIQUIDATION_TIMESTAMP), 1, 2, Number(DEFAULT_LIQUIDATION_PRICE));
		const differentMessage = await OracleData.createPriceMessage(Number(DEFAULT_LIQUIDATION_TIMESTAMP), 1, 3, Number(DEFAULT_LIQUIDATION_PRICE));
		const settlementSignature = await OracleData.signMessage(differentMessage, ORACLE_WIF);

		// Use the correct signature for the previous message.
		const previousMessage = await OracleData.createPriceMessage(Number(DEFAULT_LIQUIDATION_TIMESTAMP), 1, 1, Number(DEFAULT_LIQUIDATION_PRICE));
		const previousSignature = await OracleData.signMessage(previousMessage, ORACLE_WIF);

		// check that liquidate() function does not throw (because it does not check for signatures),
		// but the transaction still fails when sending it directly
		const { program } = await runLiquidate(manager, contractData, settlementMessage, settlementSignature, previousMessage, previousSignature);

		programReturnsExpectedResult(program, AuthenticationErrorCommon.nonNullSignatureFailure, 'matureOrLiquidateProgram-bad-message-signature');
	});

	test('should fail when previous signature does not match previous message', async () =>
	{
		// Set up test contract
		const manager = loadContractManager();
		const contractData = await loadContractData(manager, DEFAULT_NOMINAL_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER);

		// Intentionally create a signature over a different message than what is used
		// in the transaction, so the datasig check in the contract should fail
		const previousMessage = await OracleData.createPriceMessage(Number(DEFAULT_LIQUIDATION_TIMESTAMP), 1, 1, Number(DEFAULT_LIQUIDATION_PRICE));
		const differentMessage = await OracleData.createPriceMessage(Number(DEFAULT_LIQUIDATION_TIMESTAMP), 1, 3, Number(DEFAULT_LIQUIDATION_PRICE));
		const previousSignature = await OracleData.signMessage(differentMessage, ORACLE_WIF);

		// Use the correct signature for the settlement message.
		const settlementMessage = await OracleData.createPriceMessage(Number(DEFAULT_LIQUIDATION_TIMESTAMP), 1, 2, Number(DEFAULT_LIQUIDATION_PRICE));
		const settlementSignature = await OracleData.signMessage(settlementMessage, ORACLE_WIF);

		// check that liquidate() function does not throw (because it does not check for signatures),
		// but the transaction still fails when sending it directly
		const { program } = await runLiquidate(manager, contractData, settlementMessage, settlementSignature, previousMessage, previousSignature);

		programReturnsExpectedResult(program, AuthenticationErrorCommon.nonNullSignatureFailure, 'matureOrLiquidateProgram-bad-previous-message-signature');
	});

	test('should fail when previous message is not immediately before the message used for settlement', async () =>
	{
		// Set up test contract
		const manager = loadContractManager();
		const contractData = await loadContractData(manager, DEFAULT_NOMINAL_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER);

		// Set up test oracle data where there is an intentional gap between the two messages
		const settlementMessage = await OracleData.createPriceMessage(Number(DEFAULT_LIQUIDATION_TIMESTAMP), 1, 3, Number(DEFAULT_LIQUIDATION_PRICE));
		const settlementSignature = await OracleData.signMessage(settlementMessage, ORACLE_WIF);
		const previousMessage = await OracleData.createPriceMessage(Number(DEFAULT_LIQUIDATION_TIMESTAMP), 1, 1, Number(DEFAULT_LIQUIDATION_PRICE));
		const previousSignature = await OracleData.signMessage(previousMessage, ORACLE_WIF);

		// check that liquidate() function throws as it makes this check preemptively
		await expect(
			() => runLiquidate(manager, contractData, settlementMessage, settlementSignature, previousMessage, previousSignature),
		).rejects.toThrow();

		// Execute the transaction through the payout() function which does not do any checks
		const program = await runPayout(
			manager,
			contractData,
			settlementMessage,
			settlementSignature,
			previousMessage,
			previousSignature,
			DEFAULT_SHORT_PAYOUT,
			DEFAULT_LONG_PAYOUT,
		);

		programReturnsExpectedResult(program, AuthenticationErrorCommon.failedVerify, 'payoutProgram-wrong-previous-message');
	});

	test('should fail when message is not a price message', async () =>
	{
		// Set up test contract
		const manager = loadContractManager();
		const contractData = await loadContractData(manager, DEFAULT_NOMINAL_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER);

		// Create a test oracle message that specifies a negative price sequence, indicating it is a metadata message
		const settlementMessage = await OracleData.createPriceMessage(Number(DEFAULT_LIQUIDATION_TIMESTAMP), 1, -1, Number(DEFAULT_LIQUIDATION_PRICE));

		// Create the rest of the test oracle data as usual
		const settlementSignature = await OracleData.signMessage(settlementMessage, ORACLE_WIF);
		const previousMessage = await OracleData.createPriceMessage(Number(DEFAULT_LIQUIDATION_TIMESTAMP), 1, -2, Number(DEFAULT_LIQUIDATION_PRICE));
		const previousSignature = await OracleData.signMessage(previousMessage, ORACLE_WIF);

		// check that liquidate() function throws as it makes this check preemptively
		await expect(
			() => runLiquidate(manager, contractData, settlementMessage, settlementSignature, previousMessage, previousSignature),
		).rejects.toThrow();

		// Execute the transaction through the payout() function which does not do any checks
		const program = await runPayout(
			manager,
			contractData,
			settlementMessage,
			settlementSignature,
			previousMessage,
			previousSignature,
			DEFAULT_SHORT_PAYOUT,
			DEFAULT_LONG_PAYOUT,
		);

		programReturnsExpectedResult(program, AuthenticationErrorCommon.failedVerify, 'payoutProgram-not-price-message');
	});

	test('should fail when sending incorrect amounts', async () =>
	{
		// Set up test contract
		const manager = loadContractManager();
		const contractData = await loadContractData(manager, DEFAULT_NOMINAL_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER);

		// Set up test oracle data
		const settlementMessage = await OracleData.createPriceMessage(Number(DEFAULT_LIQUIDATION_TIMESTAMP), 1, 2, Number(DEFAULT_LIQUIDATION_PRICE));
		const settlementSignature = await OracleData.signMessage(settlementMessage, ORACLE_WIF);
		const previousMessage = await OracleData.createPriceMessage(Number(DEFAULT_LIQUIDATION_TIMESTAMP), 1, 1, Number(DEFAULT_LIQUIDATION_PRICE));
		const previousSignature = await OracleData.signMessage(previousMessage, ORACLE_WIF);

		// Intentionally set the short payout to an incorrect value
		const incorrectShortPayout = DEFAULT_SHORT_PAYOUT - BigInt('10');

		// Create Authentication Program that can be evaluated with libauth
		const program = await runPayout(
			manager,
			contractData,
			settlementMessage,
			settlementSignature,
			previousMessage,
			previousSignature,
			incorrectShortPayout,
			DEFAULT_LONG_PAYOUT,
		);

		programReturnsExpectedResult(program, AuthenticationErrorCommon.failedVerify, 'payoutProgram-send-incorrect-amounts');
	});

	test('should fail when oracle price is zero', async () =>
	{
		// Set up test contract
		const manager = loadContractManager();
		const contractData = await loadContractData(manager, DEFAULT_NOMINAL_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER);

		// Set up test oracle message
		let settlementMessage = await OracleData.createPriceMessage(Number(DEFAULT_LIQUIDATION_TIMESTAMP), 1, 2, Number(DEFAULT_LIQUIDATION_PRICE));

		// Manually replace the price part with 0
		const ZERO_PRICE = hexToBin('00000000');
		settlementMessage = flattenBinArray([ settlementMessage.slice(0, 12), ZERO_PRICE ]);

		// Set up rest of the test oracle data
		const settlementSignature = await OracleData.signMessage(settlementMessage, ORACLE_WIF);
		const previousMessage = await OracleData.createPriceMessage(Number(DEFAULT_LIQUIDATION_TIMESTAMP), 1, 1, Number(DEFAULT_LIQUIDATION_PRICE));
		const previousSignature = await OracleData.signMessage(previousMessage, ORACLE_WIF);

		// check that liquidate() function throws as it makes this check preemptively
		await expect(
			() => runLiquidate(manager, contractData, settlementMessage, settlementSignature, previousMessage, previousSignature),
		).rejects.toThrow();

		// Execute the transaction through the payout() function which does not do any checks
		const program = await runPayout(
			manager,
			contractData,
			settlementMessage,
			settlementSignature,
			previousMessage,
			previousSignature,
			DEFAULT_SHORT_PAYOUT,
			DEFAULT_LONG_PAYOUT,
		);

		programReturnsExpectedResult(program, AuthenticationErrorCommon.failedVerify, 'payoutProgram-oracle-price-zero');
	});

	test('should fail when oracle price is negative', async () =>
	{
		// Set up test contract
		const manager = loadContractManager();
		// TODO: very slow: 2+ s (ultimately Transaction.build())
		const contractData = await loadContractData(manager, DEFAULT_NOMINAL_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER);

		// Set up test oracle message
		let settlementMessage = await OracleData.createPriceMessage(Number(DEFAULT_LIQUIDATION_TIMESTAMP), 1, 2, Number(DEFAULT_LIQUIDATION_PRICE));

		// Manually replace the price part with a negative value (0xffffffff is negative in Script's number format)
		const NEGATIVE_PRICE = hexToBin('ffffffff');
		settlementMessage = flattenBinArray([ settlementMessage.slice(0, 12), NEGATIVE_PRICE ]);

		// Set up rest of the test oracle data
		const settlementSignature = await OracleData.signMessage(settlementMessage, ORACLE_WIF);
		const previousMessage = await OracleData.createPriceMessage(Number(DEFAULT_LIQUIDATION_TIMESTAMP), 1, 1, Number(DEFAULT_LIQUIDATION_PRICE));
		const previousSignature = await OracleData.signMessage(previousMessage, ORACLE_WIF);

		// check that liquidate() function throws as it makes this check preemptively
		await expect(
			() => runLiquidate(manager, contractData, settlementMessage, settlementSignature, previousMessage, previousSignature),
		).rejects.toThrow();

		// Execute the transaction through the payout() function which does not do any checks
		// TODO: very slow: 2 s (ultimately Transaction.build())
		const program = await runPayout(
			manager,
			contractData,
			settlementMessage,
			settlementSignature,
			previousMessage,
			previousSignature,
			DEFAULT_SHORT_PAYOUT,
			DEFAULT_LONG_PAYOUT,
		);

		programReturnsExpectedResult(program, AuthenticationErrorCommon.failedVerify, 'payoutProgram-oracle-price-negative');
	});

	// This cannot be tested using the library (because it parses the oracle message), but we should test it in the contracts repo.
	test.todo('should fail when oracle timestamp is negative');

	// This cannot be tested using the library (because it enforces single inputs), but we should test it in the contracts repo.
	test.todo('should fail when multiple inputs are used in one transaction');

	// This cannot be tested using the library (because it automatically sends to the correct addresses), but we should test it in the contracts repo.
	test.todo('should fail when sending to incorrect addresses');

	// This cannot be tested using the library (because it automatically uses the correct data), but we should test it in the contracts repo.
	test.todo('should fail when providing incorrect payoutDataHash data');
});
