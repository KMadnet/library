/* eslint-disable max-nested-callbacks */
/* eslint-disable no-param-reassign */

// Import testing libraries and utilities
import { jest } from '@jest/globals';
import { evaluateProgram, createProgram, createFakeBroadcastTransaction, loadDefaultContractData, programReturnsExpectedResult } from './test-util.js';

// Load support for bigint in JSON
import { JSONStringify } from '../lib/util/json-explicit-bigint.js';

// Import Bitcoin Cash related interfaces
import { AuthenticationErrorCommon } from '@bitauth/libauth';

// Import AnyHedge library
import type { SignedTransactionProposal, UnsignedTransactionProposal } from '../lib/index.js';
import { AnyHedgeManager } from '../lib/index.js';
import * as bchUtil from '../lib/util/bitcoincash-util.js';

// Import fixture data
import { SHORT_PUBKEY, LONG_PUBKEY, SHORT_WIF, LONG_WIF, SEVERAL_DIFFERENT_COINS, ORACLE_WIF, CONTRACT_FUNDING_10M_BCH } from './fixture/constants.js';
import { DUST_LIMIT, SATS_PER_BCH, VM_FALSE } from '../lib/constants.js';

// Stub the broadcast function to return the built transaction rather than broadcasting it
const fakeBroadcast = createFakeBroadcastTransaction();
jest.spyOn(bchUtil, 'broadcastTransaction').mockImplementation(fakeBroadcast);

// Load an AnyHedge manager.
const loadContractManager = function(): AnyHedgeManager
{
	// Set up instance of AnyHedgeManager
	const manager = new AnyHedgeManager();

	return manager;
};

const defaultProposal = async function(): Promise<UnsignedTransactionProposal>
{
	// Create a transaction proposal to send 5BCH to short and 10BCH to long
	const proposal: UnsignedTransactionProposal =
	{
		inputs: SEVERAL_DIFFERENT_COINS,
		outputs:
		[
			{ to: bchUtil.encodeCashAddressP2PKH(SHORT_PUBKEY), amount: BigInt('5') * SATS_PER_BCH },
			{ to: bchUtil.encodeCashAddressP2PKH(LONG_PUBKEY), amount: BigInt('10') * SATS_PER_BCH },
		],
	};

	return proposal;
};

describe('completeMutualRedemption()', () =>
{
	test('should redeem when both parties parties sign the same proposal', async () =>
	{
	// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
		const manager = loadContractManager();
		const contractData = await loadDefaultContractData(manager);

		// Generate a transaction proposal
		const proposal = await defaultProposal();

		// Create a signed proposal that includes the short's redemption data.
		const shortProposal = await manager.signMutualArbitraryPayout(SHORT_WIF, proposal, contractData.parameters);

		// Simulate: one party encodes and sends it to the other, who decodes it.
		const encodedShortProposal = JSONStringify(shortProposal);
		const decodedShortProposal = JSONParse(encodedShortProposal);

		// Create a signed proposal that includes the long's redemption data.
		const longProposal = await manager.signMutualArbitraryPayout(LONG_WIF, proposal, contractData.parameters);

		// Complete the mutual redemption by providing both signed proposals and broadcasting the resulting transaction
		const transactionHex = await manager.completeMutualRedemption(decodedShortProposal, longProposal, contractData.parameters);

		// For every input, check that the generated unlock script is able to unlock the lock script
		// eslint-disable-next-line max-nested-callbacks
		await Promise.all(proposal.inputs.map(async (input, i) =>
		{
			const program = await createProgram(manager, contractData, transactionHex, input.satoshis, i);

			programReturnsExpectedResult(program, true);
		}));
	});

	test('should not redeem when a party includes incorrect redemption data in the proposal', async () =>
	{
	// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
		const manager = loadContractManager();
		const contractData = await loadDefaultContractData(manager);

		// Generate a transaction proposal
		const proposal = await defaultProposal();

		// Create a signed proposal that includes the short's redemption data.
		const shortProposal = await manager.signMutualArbitraryPayout(SHORT_WIF, proposal, contractData.parameters);

		// Change the initial proposal so the long signs something different.
		const differentProposal: UnsignedTransactionProposal = { ...proposal, locktime: 100 };

		// Create a signed proposal that includes the long's redemption data.
		const differentSignedProposal = await manager.signMutualArbitraryPayout(LONG_WIF, differentProposal, contractData.parameters);

		// Add the long's redemption data to a copy of the initial proposal.
		// This causes both proposals to have the same transaction details, but incorrect signatures.
		const longProposal: SignedTransactionProposal = { ...proposal, redemptionDataList: differentSignedProposal.redemptionDataList };

		// Complete the mutual redemption by providing both signed proposals and broadcasting the resulting transaction.
		// Note: When live, this should throw an error because the transaction fails,
		//       but since the network behavior is stubbed out, we evaluate the transaction
		//       to assert that the transaction fails.
		const transactionHex = await manager.completeMutualRedemption(shortProposal, longProposal, contractData.parameters);

		// For every input, check that the generated unlock script is unable to unlock the lock script.
		// eslint-disable-next-line max-nested-callbacks
		await Promise.all(proposal.inputs.map(async (input, i) =>
		{
			const program = await createProgram(manager, contractData, transactionHex, input.satoshis, i);

			programReturnsExpectedResult(program, AuthenticationErrorCommon.nonNullSignatureFailure);
		}));
	});

	test('should fail early if proposal\'s details don\'t match', async () =>
	{
		// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
		const manager = loadContractManager();
		const contractData = await loadDefaultContractData(manager);

		// Generate a transaction proposal without redemption data.
		const proposal = await defaultProposal();

		// Create a signed proposal that includes the short's redemption data.
		const shortProposal: SignedTransactionProposal = await manager.signMutualArbitraryPayout(SHORT_WIF, proposal, contractData.parameters);

		// Change the initial proposal slightly.
		const differentProposal: UnsignedTransactionProposal = { ...proposal, locktime: 100 };

		// Create a signed proposal that includes the long's redemption data.
		const longProposal = await manager.signMutualArbitraryPayout(LONG_WIF, differentProposal, contractData.parameters);

		// Mutual redemption cannot be completed because both proposals contain different transaction details.
		await expect(() => manager.completeMutualRedemption(shortProposal, longProposal, contractData.parameters))
			.rejects.toThrowError('The passed proposals cannot be merged because they have different transaction details.');
	});

	test('should fail early if neither party signed the proposal', async () =>
	{
		// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
		const manager = loadContractManager();
		const contractData = await loadDefaultContractData(manager);

		// Generate a transaction proposal without redemption data.
		const proposal = await defaultProposal() as SignedTransactionProposal;

		// Mutual redemption cannot be completed because no redemption data is provided
		await expect(() => manager.completeMutualRedemption(proposal, proposal, contractData.parameters))
			.rejects.toThrowError('Transaction proposal does not include any redemption data');
	});

	test('should fail early if both proposals are signed by the same party', async () =>
	{
		// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
		const manager = loadContractManager();
		const contractData = await loadDefaultContractData(manager);

		// Generate a transaction proposal
		const proposal = await defaultProposal();

		// Create a signed proposal that includes the short's redemption data.
		const shortProposal = await manager.signMutualArbitraryPayout(SHORT_WIF, proposal, contractData.parameters);

		// Mutual redemption cannot be completed because both proposals are signed by the short side
		await expect(() => manager.completeMutualRedemption(shortProposal, shortProposal, contractData.parameters))
			.rejects.toThrowError('Mutual redemption could not successfully be completed');
	});

	test('should redeem when both parties parties sign the same early maturation proposal', async () =>
	{
		// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
		const manager = loadContractManager();
		const contractData = await loadDefaultContractData(manager);

		// Use a settlement price of $200
		const settlementPrice = BigInt('20000');

		const shortMutualEarlyMaturationArguments =
		{
			privateKeyWIF: SHORT_WIF,
			contractFunding: CONTRACT_FUNDING_10M_BCH,
			settlementPrice,
			contractParameters: contractData.parameters,
			contractMetadata: contractData.metadata,
		};

		// Create a signed proposal that includes the short's redemption data.
		const shortProposal = await manager.signMutualEarlyMaturation(shortMutualEarlyMaturationArguments);

		const longMutualEarlyMaturationArguments =
		{
			privateKeyWIF: LONG_WIF,
			contractFunding: CONTRACT_FUNDING_10M_BCH,
			settlementPrice: settlementPrice,
			contractParameters: contractData.parameters,
			contractMetadata: contractData.metadata,
		};

		// Create a signed proposal that includes the long's redemption data.
		const longProposal = await manager.signMutualEarlyMaturation(longMutualEarlyMaturationArguments);

		// Complete the mutual redemption by providing both signed proposals and broadcasting the resulting transaction
		const transactionHex = await manager.completeMutualRedemption(shortProposal, longProposal, contractData.parameters);

		// Check that the generated unlock script is able to unlock the lock script
		const program = await createProgram(manager, contractData, transactionHex, CONTRACT_FUNDING_10M_BCH.fundingSatoshis);

		programReturnsExpectedResult(program, true);
	});

	test('should fail early when both parties parties sign different early maturation proposals', async () =>
	{
		// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
		const manager = loadContractManager();
		const contractData = await loadDefaultContractData(manager);

		// Short uses a settlement price of $200
		const shortSettlementPrice = BigInt('20000');

		// Long uses a settlement price of $300
		const longSettlementPrice = BigInt('30000');

		const shortMutualEarlyMaturationArguments =
		{
			privateKeyWIF: SHORT_WIF,
			contractFunding: CONTRACT_FUNDING_10M_BCH,
			settlementPrice: shortSettlementPrice,
			contractParameters: contractData.parameters,
			contractMetadata: contractData.metadata,
		};

		// Create a signed proposal that includes the short's redemption data.
		const shortProposal = await manager.signMutualEarlyMaturation(shortMutualEarlyMaturationArguments);

		const longMutualEarlyMaturationArguments =
		{
			privateKeyWIF: LONG_WIF,
			contractFunding: CONTRACT_FUNDING_10M_BCH,
			settlementPrice: longSettlementPrice,
			contractParameters: contractData.parameters,
			contractMetadata: contractData.metadata,
		};

		// Create a signed proposal that includes the long's redemption data.
		const longProposal = await manager.signMutualEarlyMaturation(longMutualEarlyMaturationArguments);

		// Mutual redemption cannot be completed because both proposals contain different transaction details.
		await expect(() => manager.completeMutualRedemption(shortProposal, longProposal, contractData.parameters))
			.rejects.toThrowError('The passed proposals cannot be merged because they have different transaction details.');
	});

	test('should redeem when both parties parties sign the same refund proposal', async () =>
	{
		// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
		const manager = loadContractManager();
		const contractData = await loadDefaultContractData(manager);

		// Derive short and long's addresses
		const shortAddress = bchUtil.encodeCashAddressP2PKH(SHORT_PUBKEY);
		const longAddress = bchUtil.encodeCashAddressP2PKH(LONG_PUBKEY);

		// Create a signed proposal that includes the short's redemption data.
		const shortProposal = await manager.signMutualRefund({
			privateKeyWIF: SHORT_WIF,
			contractFunding: CONTRACT_FUNDING_10M_BCH,
			contractParameters: contractData.parameters,
			contractMetadata: contractData.metadata,
			shortRefundAddress: shortAddress,
			longRefundAddress: longAddress,
		});

		// Create a signed proposal that includes the long's redemption data.
		const longProposal = await manager.signMutualRefund(
			{
				privateKeyWIF: LONG_WIF,
				contractFunding: CONTRACT_FUNDING_10M_BCH,
				contractParameters: contractData.parameters,
				contractMetadata: contractData.metadata,
				shortRefundAddress: shortAddress,
				longRefundAddress: longAddress,
			},
		);

		// Complete the mutual redemption by providing both signed proposals and broadcasting the resulting transaction
		const transactionHex = await manager.completeMutualRedemption(shortProposal, longProposal, contractData.parameters);

		// Check that the generated unlock script is able to unlock the lock script
		const program = await createProgram(manager, contractData, transactionHex, CONTRACT_FUNDING_10M_BCH.fundingSatoshis);

		programReturnsExpectedResult(program, true);
	});

	test('should redeem when both parties parties sign the same refund proposal without providing refund addresses', async () =>
	{
		// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
		const manager = loadContractManager();
		const contractData = await loadDefaultContractData(manager);

		// Create a signed proposal that includes the short's redemption data.
		const shortProposal = await manager.signMutualRefund({
			privateKeyWIF: SHORT_WIF,
			contractFunding: CONTRACT_FUNDING_10M_BCH,
			contractParameters: contractData.parameters,
			contractMetadata: contractData.metadata,
		});

		// Create a signed proposal that includes the long's redemption data.
		const longProposal = await manager.signMutualRefund({
			privateKeyWIF: LONG_WIF,
			contractFunding: CONTRACT_FUNDING_10M_BCH,
			contractParameters: contractData.parameters,
			contractMetadata: contractData.metadata,
		});

		// Complete the mutual redemption by providing both signed proposals and broadcasting the resulting transaction
		const transactionHex = await manager.completeMutualRedemption(shortProposal, longProposal, contractData.parameters);

		// Check that the generated unlock script is able to unlock the lock script
		const program = await createProgram(manager, contractData, transactionHex, CONTRACT_FUNDING_10M_BCH.fundingSatoshis);

		programReturnsExpectedResult(program, true);
	});

	test('should fail early when both parties parties sign different refund proposals', async () =>
	{
		// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
		const manager = loadContractManager();
		const contractData = await loadDefaultContractData(manager);

		// Derive short and long's addresses
		const shortAddress = bchUtil.encodeCashAddressP2PKH(SHORT_PUBKEY);
		const longAddress = bchUtil.encodeCashAddressP2PKH(LONG_PUBKEY);

		// Create a signed proposal that includes the short's redemption data.
		const shortProposal = await manager.signMutualRefund({
			privateKeyWIF: SHORT_WIF,
			contractFunding: CONTRACT_FUNDING_10M_BCH,
			contractParameters: contractData.parameters,
			contractMetadata: contractData.metadata,
			shortRefundAddress: shortAddress,
			longRefundAddress: longAddress,
		});

		// Create a signed proposal that includes the long's redemption data, but has the addresses switched around.
		const longProposal = await manager.signMutualRefund({
			privateKeyWIF: LONG_WIF,
			contractFunding: CONTRACT_FUNDING_10M_BCH,
			contractParameters: contractData.parameters,
			contractMetadata: contractData.metadata,
			shortRefundAddress: longAddress,
			longRefundAddress: shortAddress,
		});

		// Mutual redemption cannot be completed because both proposals contain different transaction details.
		await expect(() => manager.completeMutualRedemption(shortProposal, longProposal, contractData.parameters))
			.rejects.toThrowError('The passed proposals cannot be merged because they have different transaction details.');
	});
});

describe('custodialMutualArbitraryPayout()', () =>
{
	test('should redeem', async () =>
	{
		// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
		const manager = loadContractManager();
		const contractData = await loadDefaultContractData(manager);

		// Generate a transaction proposal
		const proposal = await defaultProposal();

		// Create a complete mutual redemption transaction using the custodial function.
		const transactionHex = await manager.custodialMutualArbitraryPayout(SHORT_WIF, LONG_WIF, proposal, contractData.parameters);

		// For every input, check that the generated unlock script is able to unlock the lock script
		// eslint-disable-next-line max-nested-callbacks
		await Promise.all(proposal.inputs.map(async (input, i) =>
		{
			const program = await createProgram(manager, contractData, transactionHex, input.satoshis, i);

			programReturnsExpectedResult(program, true);
		}));
	});
});

describe('custodialMutualEarlyMaturation()', () =>
{
	test('should redeem', async () =>
	{
		// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
		const manager = loadContractManager();
		const contractData = await loadDefaultContractData(manager);

		// Use a settlement price of $200
		const settlementPrice = BigInt('20000');

		const custodialMutualEaryMaturationArguments =
		{
			shortPrivateKeyWIF: SHORT_WIF,
			longPrivateKeyWIF: LONG_WIF,
			contractFunding: CONTRACT_FUNDING_10M_BCH,
			settlementPrice,
			contractParameters: contractData.parameters,
			contractMetadata: contractData.metadata,
		};

		// Create a complete settlement transaction using the custodial function.
		const transactionHex = await manager.custodialMutualEarlyMaturation(custodialMutualEaryMaturationArguments);

		// Check that the generated unlock script is able to unlock the lock script
		const program = await createProgram(manager, contractData, transactionHex, CONTRACT_FUNDING_10M_BCH.fundingSatoshis);

		// store test context for later use

		await expect(evaluateProgram(program)).resolves.toBeTruthy();
	});
});

describe('custodialMutualRefund', () =>
{
	test('should redeem', async () =>
	{
		// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
		const manager = loadContractManager();
		const contractData = await loadDefaultContractData(manager);

		// Derive short and long's addresses
		const shortAddress = bchUtil.encodeCashAddressP2PKH(SHORT_PUBKEY);
		const longAddress = bchUtil.encodeCashAddressP2PKH(LONG_PUBKEY);

		// Create a complete refund transaction using the custodial function.
		const transactionHex = await manager.custodialMutualRefund({
			shortPrivateKeyWIF: SHORT_WIF,
			longPrivateKeyWIF: LONG_WIF,
			contractFunding: CONTRACT_FUNDING_10M_BCH,
			contractParameters: contractData.parameters,
			contractMetadata: contractData.metadata,
			shortRefundAddress: shortAddress,
			longRefundAddress: longAddress,
		});

		// Check that the generated unlock script is able to unlock the lock script
		const program = await createProgram(manager, contractData, transactionHex, CONTRACT_FUNDING_10M_BCH.fundingSatoshis);

		programReturnsExpectedResult(program, true);
	});

	test('should redeem without passing refund addresses', async () =>
	{
		// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
		const manager = loadContractManager();
		const contractData = await loadDefaultContractData(manager);

		// Create a complete refund transaction using the custodial function.
		const transactionHex = await manager.custodialMutualRefund(
			{
				shortPrivateKeyWIF: SHORT_WIF,
				longPrivateKeyWIF: LONG_WIF,
				contractFunding: CONTRACT_FUNDING_10M_BCH,
				contractParameters: contractData.parameters,
				contractMetadata: contractData.metadata,
			},
		);

		// Check that the generated unlock script is able to unlock the lock script
		const program = await createProgram(manager, contractData, transactionHex, CONTRACT_FUNDING_10M_BCH.fundingSatoshis);

		programReturnsExpectedResult(program, true);
	});
});

describe('signMutualArbitraryPayout()', () =>
{
	test('should fail if called with a WIF that does not belong to either party', async () =>
	{
		// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
		const manager = loadContractManager();
		const contractData = await loadDefaultContractData(manager);

		// Generate a transaction proposal.
		const proposal = await defaultProposal();

		// Function should throw because ORACLE_WIF does not belong to either party.
		await expect(() => manager.signMutualArbitraryPayout(ORACLE_WIF, proposal, contractData.parameters))
			.rejects.toThrowError('The passed private key WIF does not belong to either party in the contract');
	});

	test('should fail if proposal includes outputs below DUST amount', async () =>
	{
		// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
		const manager = loadContractManager();
		const contractData = await loadDefaultContractData(manager);

		// Generate a transaction proposal.
		const proposal = await defaultProposal();

		proposal.outputs[0].amount = DUST_LIMIT - BigInt('1');

		// Function should throw because ORACLE_WIF does not belong to either party.
		await expect(() => manager.signMutualArbitraryPayout(ORACLE_WIF, proposal, contractData.parameters))
			.rejects.toThrowError(`One of the outputs in the transaction proposal is below the DUST amount of ${DUST_LIMIT}.`);
	});

	test('should fail if mutual redemption is disabled', async () =>
	{
		// Load AnyHedge Manager and load a contract with dummy data but mutual redemption disabled.
		const manager = loadContractManager();
		const contractData = await loadDefaultContractData(manager, { enableMutualRedemption: VM_FALSE });

		// Generate a transaction proposal.
		const proposal = await defaultProposal();

		// Function should throw because mutual redemption is disabled
		await expect(() => manager.signMutualArbitraryPayout(ORACLE_WIF, proposal, contractData.parameters))
			.rejects.toThrowError('Mutual redemption is disabled for this contract.');
	});
});
