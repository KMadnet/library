/* eslint-disable prefer-regex-literals */
/* eslint-disable line-comment-position */
import { AnyHedgeManager } from '../../lib';
import type { ContractData, ContractSide, ContractCreationParameters } from '../../lib';
import { MAX_CONTRACT_SATS, MAX_PRICE_ORACLE_UNITS_PER_BCH, MIN_INTEGER_DIVISION_PRECISION_STEPS, MIN_PRICE_ORACLE_UNITS_PER_BCH, SATS_PER_BCH, VM_1_AS_TRUE, VM_FALSE } from '../../lib/constants.js';
// Note that these are types but used here as objects for error testing
import { validateIntegerDivisionPrecision } from '../../lib/util/constraints-validation-util.js';

// Import utility function to handle oracle data.
import { OracleData } from '@generalprotocols/price-oracle';

// Import utilities to handle hex encoded values.
import { binToHex } from '@bitauth/libauth';

import { ORACLE_PUBKEY, ORACLE_WIF } from '../fixture/constants.js';

// Note: Throughout the tests, many values have been determined as valid through empirical experimentation.
//       For example, when a value is increased (*1.1) or decreased (*0.99) by a bit in line with the test's intent,
//       the specific adjustment is determined by experimentation until it doesn't trigger tangent constraint violations.
//       When a value is determined analytically, the test will include a specific explanation.

// Sane default contract parameters
const VALID_TAKER_SIDE: ContractSide = 'short';
const VALID_MAKER_SIDE: ContractSide = 'long';
const VALID_TIMESTAMP: bigint = BigInt('1620000000');
const VALID_DURATION: bigint = BigInt('100');
const VALID_PAYOUT_ADDRESS = 'bitcoincash:qpzlruwy4xu5rxjs3z37nsj29y7h59gwvsu4ddp0u4';
const VALID_NOMINAL_UNITS: number = 100000;
const VALID_START_PRICE: bigint = BigInt('1000');
const VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER: number = 1.1;
const VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER: number = 0.9;

// Small and large nominal unit values are useful in several places to avoid violating tangent safety constraints.
const SMALL_NOMINAL_UNITS: number = 10;
const LARGE_NOMINAL_UNITS: number = 1000000000;

// Max nominal units
// Reverse calculate the nominal units needed to get the compound nominal value at the max edge.
// The actual value as calculated by SCRIPT_INT_MAX / SATS_PER_BCH is 92233720368.54775807
// However javascript cannot represent the value so we keep the representation to javascript's limits
const NOMINAL_UNITS_LEADING_TO_COMPOUND_VALUE_AT_SCRIPT_LIMIT: number = 92233720368.54776;

// Extreme multipliers for high liquidation price given the standard start price
const HIGH_LIQUIDATION_MULTIPLIER_FOR_ONE_OVER_VALID_START_PRICE: number = (Number(VALID_START_PRICE) + 1) / Number(VALID_START_PRICE);
const HIGH_LIQUIDATION_MULTIPLIER_FOR_MAX_PRICE_GIVEN_VALID_START_PRICE: number = (Number(MAX_PRICE_ORACLE_UNITS_PER_BCH) / Number(VALID_START_PRICE));

// Extreme multipliers for low liquidation price given the standard start price
const LOW_LIQUIDATION_MULTIPLIER_FOR_MIN_PRICE_GIVEN_VALID_START_PRICE: number = (Number(MIN_PRICE_ORACLE_UNITS_PER_BCH) / Number(VALID_START_PRICE));
const LOW_LIQUIDATION_MULTIPLIER_FOR_ONE_UNDER_VALID_START_PRICE: number = (Number(VALID_START_PRICE) - 1) / Number(VALID_START_PRICE);

// Note: See constraints-validation-util.ts for details on all contract parameter constraints

// Create an instance of the contract manager for access to createContract()
const contractManager = new AnyHedgeManager();

// Test fixture interface for feeding createContract()
interface ContractCreationTestFixture
{
	description: string;
	nominalUnits: number;
	startPrice: bigint;
	highLiquidationPriceMultiplier: number;
	lowLiquidationPriceMultiplier: number;
	isSimpleHedge: bigint;
	expectedPartialErrorMessage: RegExp | string | null;
}

// Note: Fractional values used in these tests, e.g. 0.9, are applied to *floating point intent* parameters which
//       get translated through library code paths into the *integer contract parameters* that we are testing.

// Runs a contract creation test according to the input fixture specifications
const runContractCreationConstraintTest = async function(fixture: ContractCreationTestFixture): Promise<void>
{
	const VALID_ORACLE_MESSAGE = await OracleData.createPriceMessage(Number(VALID_TIMESTAMP), 1, 1, Number(fixture.startPrice));
	const VALID_ORACLE_SIGNATURE = await OracleData.signMessage(VALID_ORACLE_MESSAGE, ORACLE_WIF);

	const creationParameters: ContractCreationParameters =
	{
		takerSide: VALID_TAKER_SIDE,
		makerSide: VALID_MAKER_SIDE,
		oraclePublicKey: ORACLE_PUBKEY,
		shortPayoutAddress: VALID_PAYOUT_ADDRESS,
		longPayoutAddress: VALID_PAYOUT_ADDRESS,
		nominalUnits: fixture.nominalUnits,
		startingOracleMessage: binToHex(VALID_ORACLE_MESSAGE),
		startingOracleSignature: binToHex(VALID_ORACLE_SIGNATURE),
		maturityTimestamp: VALID_TIMESTAMP + VALID_DURATION,
		isSimpleHedge: fixture.isSimpleHedge,
		highLiquidationPriceMultiplier: fixture.highLiquidationPriceMultiplier,
		lowLiquidationPriceMultiplier: fixture.lowLiquidationPriceMultiplier,
		enableMutualRedemption: VM_1_AS_TRUE,
		shortMutualRedeemPublicKey: ORACLE_PUBKEY,
		longMutualRedeemPublicKey: ORACLE_PUBKEY,
	};

	// Set up a function to create the contract repeatably in various contexts
	const test = async function(): Promise<ContractData>
	{
		return contractManager.createContract(creationParameters);
	};

	// Confirm success or error as specified by the fixture
	if(fixture.expectedPartialErrorMessage === null)
	{
		// Confirm that the creation works fine when no error is expected
		await expect(test()).resolves.not.toThrow();
	}
	else
	{
		// Confirm the error message
		await expect(test()).rejects.toThrowError(fixture.expectedPartialErrorMessage);
	}
};

// Reasonable happy path contract should pass for simple hedge
const happyPathFixtureForSimpleHedge: ContractCreationTestFixture = {
	description:                    'simple hedge should succeed with reasonable values within all safety and policy ranges',
	nominalUnits:                   VALID_NOMINAL_UNITS,
	startPrice:                     VALID_START_PRICE,
	highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
	lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
	isSimpleHedge:                  VM_1_AS_TRUE,
	expectedPartialErrorMessage:    null,
};
await runContractCreationConstraintTest(happyPathFixtureForSimpleHedge);

// Reasonable happy path contract should pass for leveraged short
const happyPathFixtureForLeveragedShort: ContractCreationTestFixture = {
	description:                    'leveraged short should succeed with reasonable values within all safety and policy ranges',
	nominalUnits:                   VALID_NOMINAL_UNITS,
	startPrice:                     VALID_START_PRICE,
	highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
	lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
	isSimpleHedge:                  VM_FALSE,
	expectedPartialErrorMessage:    null,
};
await runContractCreationConstraintTest(happyPathFixtureForLeveragedShort);

/*
	High Liquidation Price Multiplier Validation Tests
*/

// Fixtures confirming correct behavior below, at and over each relevant boundary.
const highLiquidationPriceTestFixtures: ContractCreationTestFixture[] =
[
	{
		// -----------------|startPrice --------------|minimum --------|maximum --->
		//           ^ highLiquidationPrice
		description:                    'should fail with high liquidation price below start price',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		// This pushes high liquidation price below the minimum
		highLiquidationPriceMultiplier: 0.9,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		isSimpleHedge:                  VM_1_AS_TRUE,
		expectedPartialErrorMessage:    'High liquidation price must be greater than start price',
	},
	{
		// -----------------|startPrice --------------|minimum --------|maximum --->
		//                  ^ highLiquidationPrice
		description:                    'should fail with high liquidation price at start price',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		// This sets high liquidation price to start price, which is invalid
		highLiquidationPriceMultiplier: 1.0,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		isSimpleHedge:                  VM_1_AS_TRUE,
		expectedPartialErrorMessage:    'High liquidation price must be greater than start price',
	},

	// Minimum is just one step above start price, so there is no space to test between them like this:
	// -----------------|startPrice --------------|minimum --------|maximum --->
	//                                   ^ highLiquidationPrice

	{
		// -----------------|startPrice --------------|minimum --------|maximum --->
		//                                            ^ highLiquidationPrice
		description:                    'should succeed with high liquidation price at minimum boundary',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		// This sets high liquidation price to the minimum valid value
		highLiquidationPriceMultiplier: HIGH_LIQUIDATION_MULTIPLIER_FOR_ONE_OVER_VALID_START_PRICE,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		isSimpleHedge:                  VM_1_AS_TRUE,
		expectedPartialErrorMessage:    null,
	},
	{
		// This is a repeat of the happy path, for completeness
		// -----------------|startPrice --------------|minimum --------|maximum --->
		//                                                      ^ highLiquidationPrice
		description:                    'should succeed with high liquidation price between min/max boundaries',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		// Normal value
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		isSimpleHedge:                  VM_1_AS_TRUE,
		expectedPartialErrorMessage:    null,
	},
	{
		// -----------------|startPrice --------------|minimum --------|maximum --->
		//                                                             ^ highLiquidationPrice
		description:                    'should succeed with high liquidation price at maximum boundary',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		// This sets high liquidation price to the maximum valid value
		highLiquidationPriceMultiplier: HIGH_LIQUIDATION_MULTIPLIER_FOR_MAX_PRICE_GIVEN_VALID_START_PRICE,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		isSimpleHedge:                  VM_1_AS_TRUE,
		expectedPartialErrorMessage:    null,
	},
	{
		// -----------------|startPrice --------------|minimum --------|maximum --->
		//                                                                  ^ highLiquidationPrice
		description:                    'should fail with high liquidation price above maximum boundary',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		// This pushes high liquidation price over the maximum valid value
		highLiquidationPriceMultiplier: 1.1 * HIGH_LIQUIDATION_MULTIPLIER_FOR_MAX_PRICE_GIVEN_VALID_START_PRICE,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		isSimpleHedge:                  VM_1_AS_TRUE,
		expectedPartialErrorMessage:    'High liquidation price must be at most',
	},
];
// Run each test for simple hedge and leveraged short
test.each(highLiquidationPriceTestFixtures)('%s', runContractCreationConstraintTest);
test.each(
	highLiquidationPriceTestFixtures.map((fixture) => ({ ...fixture, isSimpleHedge: VM_FALSE })),
)('%s', runContractCreationConstraintTest);

/*
	Low Liquidation Price Multiplier Validation Tests
*/

// Fixtures confirming correct behavior below, at and over each relevant boundary.
const lowLiquidationPriceTestFixtures: ContractCreationTestFixture[] =
[
	{
		// ----|0|1-------------|maximum --------------|startPrice ----------->
		//     ^ lowLiquidationPrice
		description:                    'should fail with low liquidation price below the minimum boundary',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		// This pushes low liquidation price below the minimum valid value
		lowLiquidationPriceMultiplier:  0.0,
		isSimpleHedge:                  VM_1_AS_TRUE,
		expectedPartialErrorMessage:    'low liquidation price must be greater than zero',
	},
	{
		// ----|0|1-------------|maximum --------------|startPrice ----------->
		//       ^ lowLiquidationPrice
		description:                    'should succeed with low liquidation price at the minimum boundary',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		// This sets low liquidation price to the minimum
		lowLiquidationPriceMultiplier:  LOW_LIQUIDATION_MULTIPLIER_FOR_MIN_PRICE_GIVEN_VALID_START_PRICE,
		isSimpleHedge:                  VM_1_AS_TRUE,
		expectedPartialErrorMessage:    null,
	},
	{
		// This is a repeat of the happy path
		// ----|0|1-------------|maximum --------------|startPrice ----------->
		//                ^ lowLiquidationPrice
		description:                    'should succeed with high liquidation price between min/max boundaries',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		// Normal value
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		isSimpleHedge:                  VM_1_AS_TRUE,
		expectedPartialErrorMessage:    null,
	},
	{
		// ----|0|1-------------|maximum --------------|startPrice ----------->
		//                      ^ lowLiquidationPrice
		description:                    'should succeed with low liquidation price at the maximum boundary, one step below start price',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		// This sets low liquidation price to the maximum valid value
		lowLiquidationPriceMultiplier:  LOW_LIQUIDATION_MULTIPLIER_FOR_ONE_UNDER_VALID_START_PRICE,
		isSimpleHedge:                  VM_1_AS_TRUE,
		expectedPartialErrorMessage:    null,
	},

	// Maximum is just one step below start price, so there is no space to test between them like this:
	// ----|0|1-------------|maximum --------------|startPrice ----------->
	//                                   ^ lowLiquidationPrice

	{
		// ----|0|1-------------|maximum --------------|startPrice ----------->
		//                                             ^ lowLiquidationPrice
		description:                    'should fail low liquidation price at the start price',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		// This sets low liquidation price to start price, which is invalid
		lowLiquidationPriceMultiplier:  1.0,
		isSimpleHedge:                  VM_1_AS_TRUE,
		expectedPartialErrorMessage:    'Low liquidation price must be less than start price',
	},
	{
		// ----|0|1-------------|maximum --------------|startPrice ----------->
		//                                                    ^ lowLiquidationPrice
		description:                    'should fail with low liquidation price over the start price',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		// This pushes low liquidation price even higher than start price
		lowLiquidationPriceMultiplier:  1.1,
		isSimpleHedge:                  VM_1_AS_TRUE,
		expectedPartialErrorMessage:    'Low liquidation price must be less than start price',
	},
];
// Run each test for simple hedge and leveraged short
test.each(lowLiquidationPriceTestFixtures)('%s', runContractCreationConstraintTest);
test.each(
	lowLiquidationPriceTestFixtures.map((fixture) => ({ ...fixture, isSimpleHedge: VM_FALSE })),
)('%s', runContractCreationConstraintTest);

/*
	Nominal Units Validation Tests
*/

// For the extreme nominal values at the high limit, a corresponding high start price is needed
// to avoid the payout sats breaking its own limits
const startPriceForHighNominalUnits: bigint = BigInt(Math.round(0.9 * Number(MAX_PRICE_ORACLE_UNITS_PER_BCH)));

// Fixtures confirming correct behavior below, at and over each relevant boundary.
const compoundNominalValueConstraintsTestFixtures: ContractCreationTestFixture[] =
[
	{
		// ------------|1 ---------------|maxScriptInt ----------->
		//           ^ nominalUnitsXSatsPerBch
		description:                    'should fail with compound nominal value just below the minimum boundary',
		// This pushes compound nominal value under the minimum 1e8
		nominalUnits:                   0.999,
		startPrice:                     VALID_START_PRICE,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		isSimpleHedge:                  VM_1_AS_TRUE,
		expectedPartialErrorMessage:    'compound nominal value must be at least 100 000 000,',
	},
	{
		// ------------|1 ---------------|maxScriptInt ----------->
		//             ^ nominalUnitsXSatsPerBch
		description:                    'should succeed with compound nominal value at the minimum boundary',
		// This sets compound nominal value to the minimum 1e8
		nominalUnits:                   1.0,
		startPrice:                     VALID_START_PRICE,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		isSimpleHedge:                  VM_1_AS_TRUE,
		expectedPartialErrorMessage:    null,
	},
	{
		// ------------|1 ---------------|maxScriptInt ----------->
		//                             ^ nominalUnitsXSatsPerBch
		description:                    'should succeed with compound nominal value just under maximum script integer boundary',
		// This sets compound nominal value just under the maximum, and uses a price that keeps payout from breaking its own boundary
		nominalUnits:                   0.9 * NOMINAL_UNITS_LEADING_TO_COMPOUND_VALUE_AT_SCRIPT_LIMIT,
		startPrice:                     startPriceForHighNominalUnits,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		isSimpleHedge:                  VM_1_AS_TRUE,
		expectedPartialErrorMessage:    null,
	},

	// With javascript and the way the numbers are calculated, it's hard (impossible?) to set a nominal
	// value that results in exactly the max nominal composite boundary. Therefore we test values just under and over it.
	// ------------|0 ---------------|maxSats ----------->
	//                               ^ satsForNominalUnitsAtHighLiquidation

	{
		// ------------|1 ---------------|maxScriptInt ----------->
		//                                 ^ nominalUnitsXSatsPerBch
		description:                    'should fail with compound nominal value just over maximum script integer boundary',
		// This sets compound nominal value just over the maximum, and uses a price that keeps payout from breaking its own boundary
		nominalUnits:                   1.1 * NOMINAL_UNITS_LEADING_TO_COMPOUND_VALUE_AT_SCRIPT_LIMIT,
		startPrice:                     startPriceForHighNominalUnits,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		isSimpleHedge:                  VM_1_AS_TRUE,
		expectedPartialErrorMessage:    'compound nominal value must be at most the maximum allowed script integer',
	},
];
// Run each test for simple hedge and leveraged short
test.each(compoundNominalValueConstraintsTestFixtures)('%s', runContractCreationConstraintTest);
test.each(
	compoundNominalValueConstraintsTestFixtures.map((fixture) => ({ ...fixture, isSimpleHedge: VM_FALSE })),
)('%s', runContractCreationConstraintTest);

/*
	"Cost for Nominal Value at High Liquidation Price" Validation Tests
*/

// Fixtures confirming correct behavior below, at and over each relevant boundary.
const costForCompoundNominalValueAtHighLiquidationConstraintsTestFixtures: ContractCreationTestFixture[] =
[
	// It's not feasible to setup a situation where the cost is calculated below zero, so the test is skipped.
	// ------------|0 ---------------|maxSats ----------->
	//        ^ satsForNominalUnitsAtHighLiquidation

	{
		// 0 cost effectively means "infinite short leverage". in practice, it is achieved by simple hedge setting
		// ------------|0 ---------------|maxSats ----------->
		//             ^ satsForNominalUnitsAtHighLiquidation
		description:                    'should succeed with "cost for compound nominal value at high liquidation" at zero (implies simple hedge)',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		// Simple hedge setting is the only way to achieve high liquidation price of zero:
		isSimpleHedge:                  VM_1_AS_TRUE,
		expectedPartialErrorMessage:    null,
	},
	{
		// Achieving a cost just over the minimum means using an extremely high short leverage
		// Note that even though 0 succeeds, this is now a leveraged short with qualitatively
		// different calculations going on. So it is a sort of minimum boundary for leveraged shorts.
		// ------------|0 ---------------|maxSats ----------->
		//                ^ satsForNominalUnitsAtHighLiquidation
		description:                    'should succeed with "cost for compound nominal value at high liquidation" just over minimum boundary',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		// The minimum high liquidation price corresponds to the maximum short leverage the contract can achieve
		highLiquidationPriceMultiplier: HIGH_LIQUIDATION_MULTIPLIER_FOR_ONE_OVER_VALID_START_PRICE,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		isSimpleHedge:                  VM_FALSE,
		expectedPartialErrorMessage:    null,
	},
	{
		// Achieving a cost at the maximum means using an extremely low short leverage
		// ------------|0 ---------------|maxSats ----------->
		//                               ^ satsForNominalUnitsAtHighLiquidation
		description:                    'should succeed with "cost for compound nominal value at high liquidation" at maximum boundary',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		// The maximum high liquidation price corresponds to the minimum short leverage the contract can achieve
		highLiquidationPriceMultiplier: HIGH_LIQUIDATION_MULTIPLIER_FOR_MAX_PRICE_GIVEN_VALID_START_PRICE,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		isSimpleHedge:                  VM_FALSE,
		expectedPartialErrorMessage:    null,
	},
	{
		// Go slightly over the previously identified boundary
		// ------------|0 ---------------|maxSats ----------->
		//                                 ^ satsForNominalUnitsAtHighLiquidation
		description:                    'should fail with "cost for compound nominal value at high liquidation" just over maximum boundary, but first fails with high liquidation price validation',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		// The maximum high liquidation price corresponds to the minimum short leverage the contract can achieve
		highLiquidationPriceMultiplier: 1.001 * HIGH_LIQUIDATION_MULTIPLIER_FOR_MAX_PRICE_GIVEN_VALID_START_PRICE,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		isSimpleHedge:                  VM_FALSE,
		expectedPartialErrorMessage:    'High liquidation price must be at most',
	},
];
// These tests are already specific to simple hedge / leveraged short so just run the set.
test.each(costForCompoundNominalValueAtHighLiquidationConstraintsTestFixtures)('%s', runContractCreationConstraintTest);

/*
	"Cost for Nominal Value at Redemption Price" Minimum Constraint Validation Tests
*/

// We want to find values that center the relevant set of contract parameters around 1.
//      nominalUnitsXSatsPerBch / highLiquidationPrice >= 1

// In order to get this ratio to 1:
// Small nominal units pushes the compound nominal value down.
// Large high liquidation multiplier pushes the high liquidation price up.
const highLiquidationPriceEqualToCompoundNominalValue: bigint = BigInt(SMALL_NOMINAL_UNITS) * SATS_PER_BCH;
const multiplierToGetHighLiquidationPriceEqualToCompoundNominalValue: number = Number(highLiquidationPriceEqualToCompoundNominalValue) / Number(VALID_START_PRICE);

// Fixtures confirming correct behavior below, at and over each relevant boundary.
const safeMinimumNominalUnitCostAtRedemptionTestFixtures: ContractCreationTestFixture[] =
[
	{
		// ---------|0|1 --------------->
		//          ^ nominalUnitsXSatsPerBch / highLiquidationPrice
		description:                    'should fail with "redemption hedge cost" ratio under minimum constraint boundary',
		// This pushes the ratio just under the safety limit
		nominalUnits:                   0.999 * SMALL_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		// This is the fixed high liquidation price for the target ratio
		highLiquidationPriceMultiplier: multiplierToGetHighLiquidationPriceEqualToCompoundNominalValue,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		isSimpleHedge:                  VM_1_AS_TRUE,
		expectedPartialErrorMessage:    'Unexpectedly found nominal composite (999000000) and high liquidation price (1000000000) that are unable to ensure safe payout',
	},
	{
		// ---------|0|1 --------------->
		//            ^ nominalUnitsXSatsPerBch / highLiquidationPrice
		description:                    'should succeed with "redemption hedge cost" ratio at minimum constraint boundary, but fails due to division precision',
		// This is the nominal units for the target ratio
		nominalUnits:                   SMALL_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		// This is the fixed high liquidation price for the target ratio
		highLiquidationPriceMultiplier: multiplierToGetHighLiquidationPriceEqualToCompoundNominalValue,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		isSimpleHedge:                  VM_1_AS_TRUE,
		expectedPartialErrorMessage:    'The compound nominal value and high liquidation price are too close to each other',
	},
	{
		// ---------|0|1 --------------->
		//                ^ nominalUnitsXSatsPerBch / highLiquidationPrice
		description:                    'should succeed with "redemption hedge cost" ratio over minimum constraint boundary',
		// This pushes the ratio into the safe zone, additionally with enough distance to avoid failing division precision constraint
		nominalUnits:                   Number(MIN_INTEGER_DIVISION_PRECISION_STEPS) * SMALL_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		// This is the fixed high liquidation price for the target ratio
		highLiquidationPriceMultiplier: multiplierToGetHighLiquidationPriceEqualToCompoundNominalValue,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		isSimpleHedge:                  VM_1_AS_TRUE,
		expectedPartialErrorMessage:    null,
	},
];
// Run each test for simple hedge and leveraged short
test.each(safeMinimumNominalUnitCostAtRedemptionTestFixtures)('%s', runContractCreationConstraintTest);
test.each(
	safeMinimumNominalUnitCostAtRedemptionTestFixtures.map((fixture) => ({ ...fixture, isSimpleHedge: VM_FALSE })),
)('%s', runContractCreationConstraintTest);

/*
	"Cost for Nominal Value at Redemption Price" Maximum Constraint Validation Tests
*/

// We want to find values that center the relevant set of contract parameters around MAX_SATS.
//      nominalUnitsXSatsPerBch / lowLiquidationPrice <= MAX_SATS

// In order to get this ratio to MAX_SATS:
// Large nominal units pushes the compound nominal value up.
// Small low liquidation multiplier pushes the low liquidation price down.
const lowLiquidationPriceForMaxSatsRatio: bigint = (BigInt(LARGE_NOMINAL_UNITS) * SATS_PER_BCH) / MAX_CONTRACT_SATS;

// We then work backwards to get a start price that works with these extreme values
const multiplierToGetLowLiquidationPriceForMaxSatsRatio: number = 0.95;
const startPriceValidWithLowLiquidationPrice: bigint = BigInt(Math.round(Number(lowLiquidationPriceForMaxSatsRatio) / multiplierToGetLowLiquidationPriceForMaxSatsRatio));

// Fixtures confirming correct behavior below, at and over each relevant boundary.
// Note that simple hedge and leveraged short get caught up in different errors due to some
// implementation issues. Therefore, both tests are included explicitly instead of running
// the same tests with a replaced isSimpleHedge parameter like other groups of tests.
const safeMaximumNominalUnitCostAtRedemptionTestFixtures: ContractCreationTestFixture[] =
[
	{
		// ---------|MAX_SATS --------------->
		//        ^ nominalUnitsXSatsPerBch / lowLiquidationPrice
		description:                    'should succeed with "redemption hedge cost" ratio under maximum constraint boundary',
		// This pushes the ratio just under the safety limit
		nominalUnits:                   0.999 * LARGE_NOMINAL_UNITS,
		// This is the custom start price, valid with the extreme low liquidation price
		startPrice:                     startPriceValidWithLowLiquidationPrice,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		// This is the fixed low liquidation price for the target ratio
		lowLiquidationPriceMultiplier:  multiplierToGetLowLiquidationPriceForMaxSatsRatio,
		isSimpleHedge:                  VM_1_AS_TRUE,
		expectedPartialErrorMessage:    null,
	},
	{
		// duplicate for leveraged shorts
		description:                    'should succeed with "redemption hedge cost" ratio under maximum constraint boundary',
		nominalUnits:                   0.999 * LARGE_NOMINAL_UNITS,
		startPrice:                     startPriceValidWithLowLiquidationPrice,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier:  multiplierToGetLowLiquidationPriceForMaxSatsRatio,
		isSimpleHedge:                  VM_FALSE,
		expectedPartialErrorMessage:    null,
	},

	{
		// ---------|MAX_SATS --------------->
		//          ^ nominalUnitsXSatsPerBch / lowLiquidationPrice
		description:                    'should succeed with "redemption hedge cost" ratio at maximum constraint boundary',
		// This sets the ratio at the safety limit
		nominalUnits:                   LARGE_NOMINAL_UNITS,
		// This is the custom start price, valid with the extreme low liquidation price
		startPrice:                     startPriceValidWithLowLiquidationPrice,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		// This is the fixed low liquidation price for the target ratio
		lowLiquidationPriceMultiplier:  multiplierToGetLowLiquidationPriceForMaxSatsRatio,
		isSimpleHedge:                  VM_1_AS_TRUE,
		expectedPartialErrorMessage:    null,
	},
	{
		// duplicate for leveraged shorts
		description:                    'should succeed with "redemption hedge cost" ratio at maximum constraint boundary',
		nominalUnits:                   LARGE_NOMINAL_UNITS,
		startPrice:                     startPriceValidWithLowLiquidationPrice,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier:  multiplierToGetLowLiquidationPriceForMaxSatsRatio,
		isSimpleHedge:                  VM_FALSE,
		expectedPartialErrorMessage:    null,
	},

	{
		// ---------|MAX_SATS --------------->
		//             ^ nominalUnitsXSatsPerBch / lowLiquidationPrice
		description:                    'should fail with "redemption hedge cost" ratio just over maximum constraint boundary, but fails on earlier max payout test',
		// This pushes the ratio just over the safety limit
		nominalUnits:                   1.001 * LARGE_NOMINAL_UNITS,
		// This is the custom start price, valid with the extreme low liquidation price
		startPrice:                     startPriceValidWithLowLiquidationPrice,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		// This is the fixed low liquidation price for the target ratio
		lowLiquidationPriceMultiplier:  multiplierToGetLowLiquidationPriceForMaxSatsRatio,
		isSimpleHedge:                  VM_1_AS_TRUE,
		// The payout satoshis test is more strict, so we check for that error message instead
		expectedPartialErrorMessage:    'Payout satoshis must be at most the maximum allowed policy amount',
	},
	{
		// duplicate for leveraged shorts
		// However, leveraged shorts allows the ratio to be tested since, all other things being equal, the payout is smaller for leveraged shorts
		description:                    'should fail with "redemption hedge cost" ratio just over maximum constraint boundary',
		nominalUnits:                   1.001 * LARGE_NOMINAL_UNITS,
		startPrice:                     startPriceValidWithLowLiquidationPrice,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier:  multiplierToGetLowLiquidationPriceForMaxSatsRatio,
		isSimpleHedge:                  VM_FALSE,
		// Verify the actual failure message for this validation
		expectedPartialErrorMessage:    'Unexpectedly found nominal composite (100099999999999984) and low liquidation price (10000) that are unable to ensure safe payout',
	},
];
// These tests are already specific to simple hedge / leveraged short so just run the set.
test.each(safeMaximumNominalUnitCostAtRedemptionTestFixtures)('%s', runContractCreationConstraintTest);

// Note regarding the next two sets of tests:
// It looks odd that we do not test them, but it is an artifact of the fact that we are doing this
// validation in a roundabout way in the first place - not setting contract parameters, but setting intent
// parameters that translate to contract parameters. The translation of those intent parameters precludes
// some arrangements of final contract parameters.

/*
	"Unsafe Short Payout" Minimum Constraint Validation Tests (maximum is trivially true and not included in constraints)
*/

// We want to center the mixed value around 0
//      nominalUnitsXSatsPerBch / highLiquidationPrice - satsForNominalUnitsAtHighLiquidation >= 0

// 1. With leveraged shorts, it is equivalent to the statement:
//      nominalUnitsXSatsPerBch / highLiquidationPrice - nominalUnitsXSatsPerBch / highLiquidationPrice >= 0
//      0 >= 0
//    Which is trivially true and not testable.

// 2. With simple hedge, it is equivalent to the statement:
//      nominalUnitsXSatsPerBch / highLiquidationPrice >= 0
//    Which we have already tested above in "Cost for Nominal Value at Redemption Price" Minimum Constraint Validation Tests.
//    Therefore we skip this weaker test.

/*
	"Unsafe Long Payout" Minimum Constraint Validation Tests (maximum is trivially true and not included in constraints)
*/

// We want to center the mixed value around each other...
//      payoutSats - nominalUnitsXSatsPerBch / lowLiquidationPrice + satsForNominalUnitsAtHighLiquidation >= 0

// 1. With simple hedge, it is equivalent to the statement:
//      payoutSats - nominalUnitsXSatsPerBch / lowLiquidationPrice >= 0
//      payoutSats >= nominalUnitsXSatsPerBch / lowLiquidationPrice
//    Which, when using this library, is equivalent to:
//      payoutSats >= payoutSats
//    Which is trivially true and not testable in a meaningful way since it is the definition of payoutSats in this library.

// 2. With leveraged shorts, it is equivalent to the statement:
//      payoutSats - nominalUnitsXSatsPerBch / lowLiquidationPrice + nominalUnitsXSatsPerBch / highLiquidationPrice >= 0
//    Which, when using this library, is equivalent to:
//      nominalUnitsXSatsPerBch / lowLiquidationPrice - nominalUnitsXSatsPerBch / lowLiquidationPrice + nominalUnitsXSatsPerBch / highLiquidationPrice >= 0
//      nominalUnitsXSatsPerBch / highLiquidationPrice >= 0
//    Which we have already tested above in "Cost for Nominal Value at Redemption Price" Minimum Constraint Validation Tests.
//    Therefore we skip this weaker test.

/*
	Division Precision for compound nominal value divided by high liquidation price
*/

// Given a specific nominal units value, we need a high liquidation price that results in division precision equal to the minimum precision boundary.
// We are starting from intent parameters, so we need to reverse-calculate the intent that results in the desired contract value.
// Additionally we start from a small nominal units value to avoid failing tangent validations.
const highLiquidationPriceAtEdgeOfDivisionSafety: bigint = (BigInt(SMALL_NOMINAL_UNITS) * SATS_PER_BCH) / MIN_INTEGER_DIVISION_PRECISION_STEPS;
const highLiquidationMultiplierAtEdgeOfDivisionSafety: number = Number(highLiquidationPriceAtEdgeOfDivisionSafety) / Number(VALID_START_PRICE);

// Fixtures confirming correct behavior below, at and over each relevant boundary.
const divisionPrecisionTestFixtures: ContractCreationTestFixture[] =
[
	{
		// ---------|highLiquidationPrice ------|+sufficientGap --------------->
		//                                    ^ nominalUnitsXSatsPerBch
		description:                    'should fail when there is not a sufficient gap for division precision',
		// This pushes nominal units just under the limit that ensures division precision.
		nominalUnits:                   0.999 * SMALL_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		// This sets high liquidation price to a precise value that we can shift nominal units around
		highLiquidationPriceMultiplier: highLiquidationMultiplierAtEdgeOfDivisionSafety,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		isSimpleHedge:                  VM_1_AS_TRUE,
		expectedPartialErrorMessage:    'The compound nominal value and high liquidation price are too close to each other',
	},
	{
	// ---------|highLiquidationPrice ------|+sufficientGap --------------->
	//                                      ^ nominalUnitsXSatsPerBch
		description:                    'should succeed at the minimum gap for division precision',
		// This sets nominal units at the limit that ensures division precision.
		nominalUnits:                   SMALL_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		// This sets high liquidation price to a precise value that we can shift nominal units around
		highLiquidationPriceMultiplier: highLiquidationMultiplierAtEdgeOfDivisionSafety,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		isSimpleHedge:                  VM_1_AS_TRUE,
		expectedPartialErrorMessage:    null,
	},
	{
		// ---------|highLiquidationPrice ------|+sufficientGap --------------->
		//                                         ^ nominalUnitsXSatsPerBch
		description:                    'should succeed when there is a sufficient gap for division precision',
		// This pushes nominal units just over the limit that ensures division precision.
		nominalUnits:                   1.001 * SMALL_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		// This sets high liquidation price to a precise value that we can shift nominal units around
		highLiquidationPriceMultiplier: highLiquidationMultiplierAtEdgeOfDivisionSafety,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		isSimpleHedge:                  VM_1_AS_TRUE,
		expectedPartialErrorMessage:    null,
	},
];
// Run each test for simple hedge and leveraged short
test.each(divisionPrecisionTestFixtures)('%s', runContractCreationConstraintTest);
test.each(
	divisionPrecisionTestFixtures.map((fixture) => ({ ...fixture, isSimpleHedge: VM_FALSE })),
)('%s', runContractCreationConstraintTest);

/*
	Tests on the division precision validation itself
*/

// Confirm constraints enforced by division precision, which is used as a part of several higher level validators.
describe('validateIntegerDivisionPrecision()', () =>
{
	const someValidValue = BigInt('1');

	test('should pass when there are at least as many required division steps between numerator and denominator', () =>
	{
		// Confirm integer behavior
		expect(() => validateIntegerDivisionPrecision(BigInt('5000'), BigInt('2'), BigInt('2500')))
			.not.toThrow();
	});

	// Note: Using `1.2 as unknown as bigint` allows us to pass in non-bigint values below to fail strict validation

	test('should fail if numerator is not a positive integer', async () =>
	{
		await expect(() => validateIntegerDivisionPrecision(1.2 as unknown as bigint, someValidValue, someValidValue))
			.rejects.toThrowError('Numerator must be a bigint');
		await expect(() => validateIntegerDivisionPrecision(BigInt('0'), someValidValue, someValidValue))
			.rejects.toThrowError('Numerator must be a positive integer');
		await expect(() => validateIntegerDivisionPrecision(BigInt('-1'), someValidValue, someValidValue))
			.rejects.toThrowError('Numerator must be a positive integer');
	});
	test('should fail if denominator is not a positive integer', async () =>
	{
		await expect(() => validateIntegerDivisionPrecision(someValidValue, 1.2 as unknown as bigint, someValidValue))
			.rejects.toThrowError('Denominator must be a bigint');
		await expect(() => validateIntegerDivisionPrecision(someValidValue, BigInt('0'), someValidValue))
			.rejects.toThrowError('Denominator must be a positive integer');
		await expect(() => validateIntegerDivisionPrecision(someValidValue, BigInt('-1'), someValidValue))
			.rejects.toThrowError('Denominator must be a positive integer');
	});
	test('should fail if division precision steps is not a positive integer', async () =>
	{
		await expect(() => validateIntegerDivisionPrecision(someValidValue, someValidValue, 1.2 as unknown as bigint))
			.rejects.toThrowError('Division precision steps must be a bigint');
		await expect(() => validateIntegerDivisionPrecision(someValidValue, someValidValue, BigInt('0')))
			.rejects.toThrowError('Division precision steps must be a positive integer');
		await expect(() => validateIntegerDivisionPrecision(someValidValue, someValidValue, BigInt('-1')))
			.rejects.toThrowError('Division precision steps must be a positive integer');
	});
	test('should fail if division precision is not sufficient', async () =>
	{
		const numerator = BigInt('100');
		const denominator = BigInt('10');
		const requiredSteps = BigInt('100');
		await expect(() => validateIntegerDivisionPrecision(numerator, denominator, requiredSteps))
			.rejects.toThrowError('The division (100 / 10) must have at most 1.00% error (at least 100 steps). Actual worst case precision is 10.00% (10.0 steps).');
	});
});
