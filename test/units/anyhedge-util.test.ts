/* eslint-disable max-nested-callbacks */
import type { AuthenticationInstruction, AuthenticationInstructionMaybeMalformed, AuthenticationInstructionOperation, AuthenticationInstructionPush, AuthenticationInstructionPushMalformedData, Input, Output, Transaction } from '@bitauth/libauth';
import { binToHex, encodeTransaction, hexToBin, OpcodesBCH } from '@bitauth/libauth';
import type { Utxo } from 'cashscript';
import type { ContractFunding, ParsedSettlementData } from '../../lib/index.js';
import { isParsedMutualRedemptionData, isParsedPayoutData, SettlementType, AnyHedgeManager, SettlementParseError } from '../../lib/index.js';
import { DUST_LIMIT, VM_1_AS_TRUE } from '../../lib/constants.js';
import { calculateTotalSats, confirmParametersNotMalformed, contractCoinToFunding, contractFundingToCoin, contractFundingToOutpoint, decodeParameterAsData, decodeParameterAsInteger, estimatePayoutTransactionFee, extractConstructorParameters, isAnyHedgeRedeemScript, parseSettlementTransaction, parseTransactionOutputs } from '../../lib/util/anyhedge-util.js';
import { loadDefaultContractData } from '../test-util.js';

// Define some test data

// Create the valid redeemscript that the standard payout transaction is based on
/* eslint-disable no-useless-concat */
export const REDEEMSCRIPT_FOR_VALID_PAYOUT_TRANSACTION = ''
	// Other content
	+ '04ec97a46204c096a4620380730202102f'
	// Inserted new parameter (the OP_0 referred to above) between payout sats (415007 -> 0x6551f(1f5506)) and compound nominal (5000000000 -> 0x12a05f200(00f2052a01))
	+ '031f5506' + '00' + '0500f2052a01'
	// Other content
	+ '2102d3c1de9d4bc77d6c3608cbe44d10138c7488e592dc2b1e10a6cf0e92c2ecb0471976a91415d54e1b90d806548f34263afe71695a8a19716388ac1976a914ec4bebcc7842bdc802880e8692d83e9ec41b95d688ac51210374059eb4b0edb9052779a1ef93c76d1715473a9f3d6634135a3c03b82561a015210396dc6749e3bde2c230fb10bb66a444d83318521c449181a6be57123c1257a657'
	// AnyHedge 0.12 code:
	+ '5d79009c637b695d7a7cad5c7a7cad6d6d6d6d6d7551675d7a519dc3519d607a60795779bb5e7a5e79577abb5d79587f77547f75817600a0695d79587f77547f75818c9d5d7a547f75815c799f695c795c7f77817600a0695a79a35979a45d7a547f7581765d7aa2695c7aa2785b7a8b5c7aa5919b6902340576587a537a96587a94a47c577a527994a4c4529d00cc7b9d00cd557a8851cc9d51cd547a8777777768';
/* eslint-enable no-useless-concat */

// Assemble a full valid cashscript Transaction for the standard payout case, including the redeemscript above
// Retrieved from an actual payout transaction of a test contract. Modifications described below for AnyHedge 0.12.
// https://blockchair.com/bitcoin-cash/transaction/7b5a36707fa5319ff0de6d7544da024f14aa146cbd9cf3c930d059e4e172e977
const VALID_PAYOUT_TRANSACTION: Transaction =
{
	inputs:
	[
		{
			outpointIndex: 0,
			outpointTransactionHash: hexToBin('9dc54b5580e65a9a7ca36b698b54b210e427105dd796ad24a8db60c7545010b3'),
			sequenceNumber: 4294967294,
			/* eslint-disable prefer-template */
			unlockingBytecode: hexToBin(
				// This is a modification to the original real transaction, done so that the same transaction used in AnyHedge 0.11
				// with minor modifications can be used to verify that 0.12 behaves exactly the same in the simple hedge case.
				// Original unlocking bytecode: 4007e90abb5e4baf74b15537ef63428a6226d815648bedd807c6ef6561b097a6cbdc773044fbe264749cc5b493ba88aa069ffa422ec150d20a05d8061c8e0254ed10b097a4628f3e01006a3e0100d63e000040754d256b0266f80bae0016704944fc5ef28aa7dfe37035c29ddbbe08b6a68297d13bc6b676d514d58ba07f4affa6d3f834f7d8fea1b5bd1b4f4e132ffbb35cbc10ec97a462903e01006b3e0100d03e0000514d540104ec97a46204c096a4620380730202102f031f55060500f2052a012102d3c1de9d4bc77d6c3608cbe44d10138c7488e592dc2b1e10a6cf0e92c2ecb0471976a91415d54e1b90d806548f34263afe71695a8a19716388ac1976a914ec4bebcc7842bdc802880e8692d83e9ec41b95d688ac51210374059eb4b0edb9052779a1ef93c76d1715473a9f3d6634135a3c03b82561a015210396dc6749e3bde2c230fb10bb66a444d83318521c449181a6be57123c1257a6575c79009c637b695c7a7cad5b7a7cad6d6d6d6d6d51675c7a519dc3519d5f7a5f795779bb5d7a5d79577abb5c79587f77547f75817600a0695c79587f77547f75818c9d5c7a547f75815b799f695b795c7f77817600a0695979a35879a45c7a547f7581765c7aa2695b7aa2785a7a8b5b7aa5919b6902220276587a537a96a47c577a527994a4c4529d00cc7b9d00cd557a8851cc9d51cd547a8777777768
				'4007e90abb5e4baf74b15537ef63428a6226d815648bedd807c6ef6561b097a6cbdc773044fbe264749cc5b493ba88aa069ffa422ec150d20a05d8061c8e0254ed10b097a4628f3e01006a3e0100d63e000040754d256b0266f80bae0016704944fc5ef28aa7dfe37035c29ddbbe08b6a68297d13bc6b676d514d58ba07f4affa6d3f834f7d8fea1b5bd1b4f4e132ffbb35cbc10ec97a462903e01006b3e0100d03e0000514d'
				// Changed redeemscript push from 340 -> 345 to cover 4 additional op codes and 1 additional parameter (in this case 1 byte OP_0) (0x159(5901))
				+ '5901'
				// Redeemscript
				+ REDEEMSCRIPT_FOR_VALID_PAYOUT_TRANSACTION,
			),
			/* eslint-enable prefer-template */
		},
	],
	locktime: 0,
	outputs:
	[
		{
			lockingBytecode: hexToBin('76a914ec4bebcc7842bdc802880e8692d83e9ec41b95d688ac'),
			valueSatoshis: BigInt('310945'),
		},
		{
			lockingBytecode: hexToBin('76a91415d54e1b90d806548f34263afe71695a8a19716388ac'),
			valueSatoshis: BigInt('104062'),
		},
	],
	version: 2,
};

// Create the valid redeemscript that the mutual redemption payout transaction is based on
/* eslint-disable no-useless-concat */
export const REDEEMSCRIPT_FOR_VALID_MUTUAL_REDEMPTION_TRANSACTION = ''
	// Other content
	+ '044fb20863044f69ac620332b30102a420'
	// Inserted new parameter (the OP_0 referred to above) between payout sats (415007 -> 0x6551f(1f5506)) and compound nominal (5000000000 -> 0x12a05f200(00f2052a01))
	+ '03652109' + '00' + '0500f2052a01'
	// Other content
	+ '2102d3c1de9d4bc77d6c3608cbe44d10138c7488e592dc2b1e10a6cf0e92c2ecb0471976a91415d54e1b90d806548f34263afe71695a8a19716388ac1976a914ec4bebcc7842bdc802880e8692d83e9ec41b95d688ac51210374059eb4b0edb9052779a1ef93c76d1715473a9f3d6634135a3c03b82561a015210396dc6749e3bde2c230fb10bb66a444d83318521c449181a6be57123c1257a657'
	// AnyHedge 0.12 code:
	+ '5d79009c637b695d7a7cad5c7a7cad6d6d6d6d6d7551675d7a519dc3519d607a60795779bb5e7a5e79577abb5d79587f77547f75817600a0695d79587f77547f75818c9d5d7a547f75815c799f695c795c7f77817600a0695a79a35979a45d7a547f7581765d7aa2695c7aa2785b7a8b5c7aa5919b6902340576587a537a96587a94a47c577a527994a4c4529d00cc7b9d00cd557a8851cc9d51cd547a8777777768';
/* eslint-enable no-useless-concat */

// Assemble a full valid cashscript Transaction for the mutual redemption payout case
// Retrieved from an actual mutual redemption transaction of a test contract. Modifications described below for AnyHedge 0.12.
// https://blockchair.com/bitcoin-cash/transaction/00691d69cbf0359c1f8ddda2c3897b0a43edf53afa618eef5d1ccde7edf5b825
const VALID_MUTUAL_REDEMPTION_TRANSACTION: Transaction =
{
	inputs:
	[
		{
			outpointIndex: 0,
			outpointTransactionHash: hexToBin('db9775d94f8984fbaedfdca416690065647b079c8a9f454d1abcba1d99a1954c'),
			sequenceNumber: 0,
			/* eslint-disable prefer-template */
			unlockingBytecode: hexToBin(
				// This is a modification to the original real transaction, done so that the same transaction used in AnyHedge 0.11
				// with minor modifications can be used to verify that 0.12 behaves exactly the same in the simple hedge case.
				// Original unlocking bytecode: 4158787bb408d55e1ce0c24f6fa8fa817b79c2e60b65679a67dab13abf599998bd61d3b70ae70b22a77044310c6189ebef3ea8df9177ecce31d3be4068d6295e6741415557d1f27e9a37a702f60f74f3fa7ebd7f3122e40d0aacac41681a766ae79d3b6fd486c8e033d1ad3569011786fcb63c4fd9b3bf61486ccbb36ce76a316c405c41004d5401044fb20863044f69ac620332b30102a420036521090500f2052a012102d3c1de9d4bc77d6c3608cbe44d10138c7488e592dc2b1e10a6cf0e92c2ecb0471976a91415d54e1b90d806548f34263afe71695a8a19716388ac1976a914ec4bebcc7842bdc802880e8692d83e9ec41b95d688ac51210374059eb4b0edb9052779a1ef93c76d1715473a9f3d6634135a3c03b82561a015210396dc6749e3bde2c230fb10bb66a444d83318521c449181a6be57123c1257a6575c79009c637b695c7a7cad5b7a7cad6d6d6d6d6d51675c7a519dc3519d5f7a5f795779bb5d7a5d79577abb5c79587f77547f75817600a0695c79587f77547f75818c9d5c7a547f75815b799f695b795c7f77817600a0695979a35879a45c7a547f7581765c7aa2695b7aa2785a7a8b5b7aa5919b6902220276587a537a96a47c577a527994a4c4529d00cc7b9d00cd557a8851cc9d51cd547a8777777768
				'4158787bb408d55e1ce0c24f6fa8fa817b79c2e60b65679a67dab13abf599998bd61d3b70ae70b22a77044310c6189ebef3ea8df9177ecce31d3be4068d6295e6741415557d1f27e9a37a702f60f74f3fa7ebd7f3122e40d0aacac41681a766ae79d3b6fd486c8e033d1ad3569011786fcb63c4fd9b3bf61486ccbb36ce76a316c405c41004d'
				// Changed lockscript push from 340 -> 345 to cover 4 additional op codes and 1 additional parameter (in this case 1 byte OP_0) (0x159(5901))
				+ '5901'
				// Redeemscript
				+ REDEEMSCRIPT_FOR_VALID_MUTUAL_REDEMPTION_TRANSACTION,
			),
			/* eslint-enable prefer-template */
		},
	],
	locktime: 0,
	outputs:
	[
		{
			lockingBytecode: hexToBin('76a914ec4bebcc7842bdc802880e8692d83e9ec41b95d688ac'),
			valueSatoshis: BigInt('448793'),
		},
		{
			lockingBytecode: hexToBin('76a91415d54e1b90d806548f34263afe71695a8a19716388ac'),
			valueSatoshis: BigInt('149580'),
		},
	],
	version: 2,
};

// Define the expected contract data from the valid payout transaction (not the mutual redemption one)
const SETTLEMENT_DATA_FOR_VALID_PAYOUT_TRANSACTION: ParsedSettlementData =
{
	// This is a fake address re-determined by modifications to the original real transaction,
	// done so that the same transaction used in AnyHedge 0.11 with minor modifications can be used
	// to verify that 0.12 behaves exactly the same in the simple hedge case.
	// Original address: bitcoincash:pq27meje7y4v99tuyypvd37lcyf68jm83guwcyfy9l.
	address: 'bitcoincash:pdnl08sfnaa2g76yznv3zxxyt02e4sztpdfy4m3nydhxjdj9m0nqw99d45v3c',
	funding:
	{
		fundingTransactionHash: '9dc54b5580e65a9a7ca36b698b54b210e427105dd796ad24a8db60c7545010b3',
		fundingOutputIndex: BigInt('0'),
	},
	parameters:
	{
		lowLiquidationPrice: BigInt('12048'),
		highLiquidationPrice: BigInt('160640'),
		startTimestamp: BigInt('1654953664'),
		maturityTimestamp: BigInt('1654953964'),
		oraclePublicKey: '02d3c1de9d4bc77d6c3608cbe44d10138c7488e592dc2b1e10a6cf0e92c2ecb047',
		shortLockScript: '76a914ec4bebcc7842bdc802880e8692d83e9ec41b95d688ac',
		longLockScript: '76a91415d54e1b90d806548f34263afe71695a8a19716388ac',
		shortMutualRedeemPublicKey: '0396dc6749e3bde2c230fb10bb66a444d83318521c449181a6be57123c1257a657',
		longMutualRedeemPublicKey: '0374059eb4b0edb9052779a1ef93c76d1715473a9f3d6634135a3c03b82561a015',
		enableMutualRedemption: VM_1_AS_TRUE,
		nominalUnitsXSatsPerBch: BigInt('5000000000'),
		satsForNominalUnitsAtHighLiquidation: BigInt('0'),
		payoutSats: BigInt('415007'),
	},
	settlement:
	{
		settlementType: SettlementType.MATURATION,
		// Original transaction hash: 7b5a36707fa5319ff0de6d7544da024f14aa146cbd9cf3c930d059e4e172e977
		settlementTransactionHash: '17080423b04bfab22dc1456698ea245fd23d2f9e39f5f8fd3aeecb7c6b1d0835',
		shortPayoutInSatoshis: BigInt('310945'),
		longPayoutInSatoshis: BigInt('104062'),
		settlementMessage: 'ec97a462903e01006b3e0100d03e0000',
		settlementSignature: '754d256b0266f80bae0016704944fc5ef28aa7dfe37035c29ddbbe08b6a68297d13bc6b676d514d58ba07f4affa6d3f834f7d8fea1b5bd1b4f4e132ffbb35cbc',
		previousMessage: 'b097a4628f3e01006a3e0100d63e0000',
		previousSignature: '07e90abb5e4baf74b15537ef63428a6226d815648bedd807c6ef6561b097a6cbdc773044fbe264749cc5b493ba88aa069ffa422ec150d20a05d8061c8e0254ed',
		settlementPrice: BigInt('16080'),
	},
};

// Create some common parameter objects to use in authentication instruction tests.
const VALID_OP_X_PARAMETER: AuthenticationInstructionOperation = { opcode: OpcodesBCH.OP_2 };
const VALID_DATA_PARAMETER: AuthenticationInstructionPush =
{
	// Just need something reasonable. Push a single byte number, not in the 0~16 range that has unique opcodes.
	data: new Uint8Array(hexToBin('0xEE')),
	opcode: OpcodesBCH.OP_PUSHBYTES_1,
};
const MALFORMED_DATA_PARAMETER: AuthenticationInstructionPushMalformedData =
{
	// Just need something representing consistent malformed data. Push single byte op, but data is 2 bytes.
	data: new Uint8Array(hexToBin('1122')),
	opcode: OpcodesBCH.OP_PUSHBYTES_1,
	expectedDataBytes: 4,
	malformed: true,
};
const VALID_DATA_PARAMETER_CANT_BE_NUMBER: AuthenticationInstructionPush =
{
	// Just need something representing consistent malformed numeric push. 65-byte blob that can't be a valid script number.
	data: new Uint8Array(hexToBin('FF'.repeat(65))),
	opcode: OpcodesBCH.OP_PUSHBYTES_65,
};

// Create some common lists of parameters
const VALID_MIXED_PARAMETERS: AuthenticationInstruction[] =
[
	VALID_OP_X_PARAMETER,
	VALID_DATA_PARAMETER,
	VALID_OP_X_PARAMETER,
	VALID_DATA_PARAMETER,
];
const MIXED_PARAMETERS_WITH_INVALID: AuthenticationInstructionMaybeMalformed[] =
[
	...VALID_MIXED_PARAMETERS,
	MALFORMED_DATA_PARAMETER,
];

// Common anyhedge manager
const MANAGER = new AnyHedgeManager();

describe('estimatePayoutTransactionFee()', () =>
{
	test('should estimate payout transaction size', async () =>
	{
		// Set up default test data.
		const contractData = await loadDefaultContractData(MANAGER);

		// Compile the contract for the default contract data.
		const contract = await MANAGER.compileContract(contractData.parameters);

		// Estimate the payout transaction size.
		// Check that the payout transaction size matches the expectations
		// TODO: This is an integration test that is hard to verify. Need for improvement noted in #267

		await expect(estimatePayoutTransactionFee(contract, contractData.parameters, 1.0)).resolves.toEqual(BigInt('636'));
	});
});

// Note: We are exercising parseMutualRedemptionTransaction() and parsePayoutTransaction() through these higher level tests
describe('parseSettlementTransaction()', () =>
{
	test('should throw when transaction contains more than one input', async () =>
	{
		// Duplicate the inputs, so that there are 2 inputs rather than 1.
		const transaction: Transaction = { ...VALID_PAYOUT_TRANSACTION, inputs: [ ...VALID_PAYOUT_TRANSACTION.inputs, ...VALID_PAYOUT_TRANSACTION.inputs ] };

		// Encode the transaction.
		const encodedTransaction = binToHex(encodeTransaction(transaction));

		// Attempt to parse the settlement transaction.
		// Check that the call throws a SettlementParseError.
		await expect(() => parseSettlementTransaction(encodedTransaction)).rejects.toThrowError(SettlementParseError);
	});

	test('should throw when transaction does not contain exactly two outputs', async () =>
	{
		// Only use the first output, so that there is 1 output rather than 2.
		const outputs: Output[] = [ VALID_PAYOUT_TRANSACTION.outputs[0] ];
		const transaction: Transaction = { ...VALID_PAYOUT_TRANSACTION, outputs };

		// Encode the transaction.
		const encodedTransaction = binToHex(encodeTransaction(transaction));

		// Attempt to parse the settlement transaction.
		// Check that the call throws a SettlementParseError.
		await expect(() => parseSettlementTransaction(encodedTransaction)).rejects.toThrowError(SettlementParseError);
	});

	test('should throw when transaction does not contain expected input parameters', async () =>
	{
		// Remove the unlocking bytecode from the transaction input so it does not contain any input parameters.
		const inputs: Input[] = [{ ...VALID_PAYOUT_TRANSACTION.inputs[0], unlockingBytecode: hexToBin('') }];
		const transaction: Transaction = { ...VALID_PAYOUT_TRANSACTION, inputs };

		// Encode the transaction.
		const encodedTransaction = binToHex(encodeTransaction(transaction));

		// Attempt to parse the settlement transaction.
		// Check that the call throws a SettlementParseError.
		await expect(() => parseSettlementTransaction(encodedTransaction)).rejects.toThrowError(SettlementParseError);
	});

	// re-enable with updated transaction content
	test('should successfully parse a mutual redemption transaction', async () =>
	{
		// Make a copy of the settlement transaction.
		const transaction: Transaction = { ...VALID_MUTUAL_REDEMPTION_TRANSACTION };

		// Encode the transaction.
		const encodedTransaction = binToHex(encodeTransaction(transaction));

		// Parse the settlement transaction.
		const parsedSettlementData = await parseSettlementTransaction(encodedTransaction);

		// Define the expected data.
		const expectedSettlementData: ParsedSettlementData =
		{
			// This is a fake address re-determined by modifications to the original real transaction,
			// done so that the same transaction used in AnyHedge 0.11 with minor modifications can be used
			// to verify that 0.12 behaves exactly the same in the simple hedge case.
			// Original address: bitcoincash:ppm5jfz7f2hkvxq7rzcy23wye0qprcq20unt8zyd8r.
			address: 'bitcoincash:p0gked2tg5hukdav7qum45fgwuqdpyq96aqq7gac35gfy5gf9wzhxaakl56tn',
			funding:
			{
				fundingTransactionHash: 'db9775d94f8984fbaedfdca416690065647b079c8a9f454d1abcba1d99a1954c',
				fundingOutputIndex: BigInt('0'),
			},
			parameters:
			{
				lowLiquidationPrice: BigInt('8356'),
				highLiquidationPrice: BigInt('111410'),
				startTimestamp: BigInt('1655466319'),
				maturityTimestamp: BigInt('1661514319'),
				oraclePublicKey: '02d3c1de9d4bc77d6c3608cbe44d10138c7488e592dc2b1e10a6cf0e92c2ecb047',
				shortLockScript: '76a914ec4bebcc7842bdc802880e8692d83e9ec41b95d688ac',
				longLockScript: '76a91415d54e1b90d806548f34263afe71695a8a19716388ac',
				shortMutualRedeemPublicKey: '0396dc6749e3bde2c230fb10bb66a444d83318521c449181a6be57123c1257a657',
				longMutualRedeemPublicKey: '0374059eb4b0edb9052779a1ef93c76d1715473a9f3d6634135a3c03b82561a015',
				enableMutualRedemption: VM_1_AS_TRUE,
				nominalUnitsXSatsPerBch: BigInt('5000000000'),
				satsForNominalUnitsAtHighLiquidation: BigInt('0'),
				payoutSats: BigInt('598373'),
			},
			settlement:
			{
				settlementType: SettlementType.MUTUAL,
				// Original transaction hash: 00691d69cbf0359c1f8ddda2c3897b0a43edf53afa618eef5d1ccde7edf5b825
				settlementTransactionHash: 'fc29746b0f2575accfb5c81a4271d7594cea526af52cc283eed9d9014599c028',
				shortPayoutInSatoshis: BigInt('448793'),
				longPayoutInSatoshis: BigInt('149580'),
			},
		};

		// Check that the actual data matches the expected data.
		expect(isParsedMutualRedemptionData(parsedSettlementData)).toBeTruthy();
		expect(parsedSettlementData).toEqual(expectedSettlementData);
	});

	// re-enable with updated transaction content
	test('should successfully parse a payout transaction', async () =>
	{
		// Make a copy of the settlement transaction.
		const transaction: Transaction = { ...VALID_PAYOUT_TRANSACTION };

		// Encode the transaction.
		const encodedTransaction = binToHex(encodeTransaction(transaction));

		// Parse the settlement transaction.
		const parsedSettlementData = await parseSettlementTransaction(encodedTransaction);

		// Check that the actual data matches the expected data.
		expect(isParsedPayoutData(parsedSettlementData)).toBeTruthy();
		expect(parsedSettlementData).toEqual(SETTLEMENT_DATA_FOR_VALID_PAYOUT_TRANSACTION);
	});

	// For the following tests we need to hand-craft a P2SH transaction that looks like an AnyHedge transaction, but isn't.
	// This is currently not worth the effort as long as the function works correctly for *actual* AnyHedge transactions.
	test.todo('should throw when mutual redemption transaction redeem script does not match AnyHedge');
	test.todo('should throw when payout transaction redeem script does not match AnyHedge');
});

describe('confirmParametersNotMalformed()', () =>
{
	test('passes well formed parameters through with no change', async () =>
	{
		// Process a set of valid parameters, and confirm they are equal to the result.
		const result = confirmParametersNotMalformed(VALID_MIXED_PARAMETERS);
		expect(result).toEqual(VALID_MIXED_PARAMETERS);
	});

	test('throws if any malformed parameters', async () =>
	{
		// Process a set of parameters with an invalid parameter, and confirm the call throws.
		await expect(async () => confirmParametersNotMalformed(MIXED_PARAMETERS_WITH_INVALID))
			.rejects.toThrow('unable to interpret parameter');
	});
});

describe('decodeParameterAsInteger()', () =>
{
	test('returns a bigint from simple OP_X numbers', async () =>
	{
		// Start with every simple OP_X
		const allOpX: number[] =
		[
			OpcodesBCH.OP_0,
			OpcodesBCH.OP_1, OpcodesBCH.OP_2, OpcodesBCH.OP_3, OpcodesBCH.OP_4,
			OpcodesBCH.OP_5, OpcodesBCH.OP_6, OpcodesBCH.OP_7, OpcodesBCH.OP_8,
			OpcodesBCH.OP_9, OpcodesBCH.OP_10, OpcodesBCH.OP_11, OpcodesBCH.OP_12,
			OpcodesBCH.OP_13, OpcodesBCH.OP_14, OpcodesBCH.OP_15, OpcodesBCH.OP_16,
		];

		// Convert each simple OP_X into an AuthenticationInstruction and confirm correct conversion
		for(const opXIndex in allOpX)
		{
			// Get the OP_X value (which is not the same as the value it names.
			const opX = allOpX[opXIndex];

			// Get the BigInt (here, the index is equal to the named value of each OP_X
			const expectedBigInt = BigInt(opXIndex);

			// Form the simple OP_X into an authentication instruction
			const opXParameter: AuthenticationInstructionOperation = { opcode: opX };

			// Convert and confirm equality to expected, with double extra confirmation of actual type
			const result = decodeParameterAsInteger(opXParameter);
			expect(result).toEqual(expectedBigInt);
			expect(typeof result).toEqual('bigint');
		}
	});

	test('throws for parameters that cannot be converted to integer', async () =>
	{
		// Confirm conversion throws
		await expect(async () => decodeParameterAsInteger(VALID_DATA_PARAMETER_CANT_BE_NUMBER))
			.rejects.toThrow('Failed to decode VM Number');
	});
});

describe('decodeParameterAsData()', () =>
{
	test('returns data hex from data push parameters', async () =>
	{
		// Convert the known values and confirm hex equality to the actual data payload
		expect(decodeParameterAsData(VALID_DATA_PARAMETER)).toEqual(binToHex(VALID_DATA_PARAMETER.data));
		expect(decodeParameterAsData(VALID_DATA_PARAMETER_CANT_BE_NUMBER)).toEqual(binToHex(VALID_DATA_PARAMETER_CANT_BE_NUMBER.data));
	});

	test('throws for non data parameters', async () =>
	{
		// Confirm conversion throws
		await expect(async () => decodeParameterAsData(VALID_OP_X_PARAMETER))
			.rejects.toThrow('expected an op_push with data but got');
	});
});

describe('extractConstructorParameters()', () =>
{
	test('extracts expected contract creation parameters from a valid redeemscript', async () =>
	{
		// extract contract creation parameters from valid redeemscript
		const extractedParameters = extractConstructorParameters(hexToBin(REDEEMSCRIPT_FOR_VALID_PAYOUT_TRANSACTION));

		// confirm extracted parameters equal expected parameters
		expect(extractedParameters).toEqual(SETTLEMENT_DATA_FOR_VALID_PAYOUT_TRANSACTION.parameters);
	});

	test('throws for an otherwise valid redeemscript with an invalid value in parameters', async () =>
	{
		// remove one byte of a 3-byte push so that the whole remaining redeemscript will be misinterpreted
		const redeemScriptWithMalformedParameters = REDEEMSCRIPT_FOR_VALID_PAYOUT_TRANSACTION.replace('031f5506', '031f55');

		// confirm throw when extract contract creation parameters from valid redeemscript
		await expect(async () => extractConstructorParameters(hexToBin(redeemScriptWithMalformedParameters)))
			.rejects.toThrow('Failed to decode');
	});
});

describe('calculateTotalSats()', () =>
{
	test('returns the sum representing the total sats that must be put into a contract input', async () =>
	{
		// Get values from a known valid contract
		const knownContractMetadata = (await loadDefaultContractData(MANAGER)).metadata;
		const { shortInputInSatoshis, longInputInSatoshis, minerCostInSatoshis } = knownContractMetadata;

		// Establish the expected value
		const expectedTotalSatsForKnownContract = shortInputInSatoshis + longInputInSatoshis + minerCostInSatoshis + DUST_LIMIT;

		// Confirm the underlying assumptions about values to make double sure this test is meaningful.
		const zero = BigInt('0');
		expect(shortInputInSatoshis).toBeGreaterThan(zero);
		expect(longInputInSatoshis).toBeGreaterThan(zero);
		expect(minerCostInSatoshis).toBeGreaterThan(zero);
		expect(DUST_LIMIT).toBeGreaterThan(zero);

		// Confirm expected total
		expect(calculateTotalSats(knownContractMetadata)).toEqual(expectedTotalSatsForKnownContract);
	});
});

describe('contractCoinToFunding(), contractFundingToCoin(), contractFundingToOutpoint()', () =>
{
	// Matching cashscript-Utxo and anyhedge-Funding
	const MATCHING_PAIR_UTXO: Utxo =
	{
		txid: 'fake_txid',
		vout: 99,
		satoshis: BigInt('7777'),
	};
	const MATCHING_PAIR_FUNDING: ContractFunding =
	{
		fundingTransactionHash: 'fake_txid',
		fundingOutputIndex: BigInt('99'),
		fundingSatoshis: BigInt('7777'),
	};

	test('contractCoinToFunding() converts a valid cashscript Utxo to its equivalent ContractFunding structure', async () =>
	{
		// confirm equality
		expect(contractCoinToFunding(MATCHING_PAIR_UTXO)).toEqual(MATCHING_PAIR_FUNDING);
	});

	test('contractFundingToCoin() converts a valid ContractFunding structure to its equivalent cashscript Utxo', async () =>
	{
		// confirm equality
		expect(contractFundingToCoin(MATCHING_PAIR_FUNDING)).toEqual(MATCHING_PAIR_UTXO);
	});

	test('contractFundingToOutpoint() converts a valid ContractFunding structure to the common outpoint string representation', async () =>
	{
		// confirm equality
		expect(contractFundingToOutpoint(MATCHING_PAIR_FUNDING)).toEqual('fake_txid:99');
	});
});

describe.skip('buildPayoutTransaction()', () =>
{
	// For now, this is tested heavily but indirectly through automatedPayout().
});

describe('isAnyHedgeRedeemScript()', () =>
{
	test('returns true when last data of redeemscript is a push of anyhedge lockscript', async () =>
	{
		expect(isAnyHedgeRedeemScript(REDEEMSCRIPT_FOR_VALID_PAYOUT_TRANSACTION)).toEqual(true);
	});

	test('regression: returns false when push of anyhedge lockscript is in the redeemscript, but not the last data', async () =>
	{
		// Create a redeem script that contains the AnyHedge bytecode, but not at the end.
		const almostAnyHedgeRedeemScript = `${REDEEMSCRIPT_FOR_VALID_PAYOUT_TRANSACTION}ff`;

		// Confirm it is correctly rejected even though it contains the AnyHedge bytecode
		expect(isAnyHedgeRedeemScript(almostAnyHedgeRedeemScript)).toEqual(false);
	});
});

describe('parseTransactionOutputs()', () =>
{
	// Setup some fake data for short
	const SHORT_LOCKSCRIPT_HEX = 'aaaa';
	const SHORT_OUTPUT: Output = { lockingBytecode: hexToBin(SHORT_LOCKSCRIPT_HEX), valueSatoshis: BigInt('1230') };

	// Setup some fake data for long
	const LONG_LOCKSCRIPT_HEX = 'bbbb';
	const LONG_OUTPUT: Output = { lockingBytecode: hexToBin(LONG_LOCKSCRIPT_HEX), valueSatoshis: BigInt('4560') };

	// Setup some fake data for a third party
	const OTHER_OUTPUT: Output = { lockingBytecode: hexToBin('ccc'), valueSatoshis: BigInt('7890') };

	test('returns short and long outputs correctly from a list of outputs', async () =>
	{
		// Parse outputs from short + long
		const outputs = [ SHORT_OUTPUT, OTHER_OUTPUT, LONG_OUTPUT ];
		const { shortPayoutInSatoshis, longPayoutInSatoshis } = parseTransactionOutputs(outputs, SHORT_LOCKSCRIPT_HEX, LONG_LOCKSCRIPT_HEX);

		// Confirm the extracted amounts are correct
		expect(shortPayoutInSatoshis).toEqual(SHORT_OUTPUT.valueSatoshis);
		expect(longPayoutInSatoshis).toEqual(LONG_OUTPUT.valueSatoshis);
	});

	test('throws if each target lockscript does not appear exactly once in the outputs', async () =>
	{
		// Same twice ==> no way to identify which is short vs. long
		const outputWithValue1: Output = { lockingBytecode: hexToBin(SHORT_LOCKSCRIPT_HEX), valueSatoshis: BigInt('1111') };
		const outputWithValue2: Output = { lockingBytecode: hexToBin(SHORT_LOCKSCRIPT_HEX), valueSatoshis: BigInt('2222') };
		const samePartyWithTwoOutputs = [ outputWithValue1, outputWithValue2 ];
		await expect(async () => parseTransactionOutputs(samePartyWithTwoOutputs, SHORT_LOCKSCRIPT_HEX, SHORT_LOCKSCRIPT_HEX))
			.rejects.toThrow('Expected to find exactly one lockscript of each party but found Short:2, Long:2, Other:0');

		// Missing long
		const outputsMissingLong = [ SHORT_OUTPUT, OTHER_OUTPUT ];
		await expect(async () => parseTransactionOutputs(outputsMissingLong, SHORT_LOCKSCRIPT_HEX, LONG_LOCKSCRIPT_HEX))
			.rejects.toThrow('Expected to find exactly one lockscript of each party but found Short:1, Long:0, Other:1');

		// Missing short
		const outputsMissingShort = [ OTHER_OUTPUT, LONG_OUTPUT ];
		await expect(async () => parseTransactionOutputs(outputsMissingShort, SHORT_LOCKSCRIPT_HEX, LONG_LOCKSCRIPT_HEX))
			.rejects.toThrow('Expected to find exactly one lockscript of each party but found Short:0, Long:1, Other:1');

		// Missing both
		const outputsMissingBoth = [ OTHER_OUTPUT, OTHER_OUTPUT ];
		await expect(async () => parseTransactionOutputs(outputsMissingBoth, SHORT_LOCKSCRIPT_HEX, LONG_LOCKSCRIPT_HEX))
			.rejects.toThrow('Expected to find exactly one lockscript of each party but found Short:0, Long:0, Other:2');

		// Only one output
		const onlyOneOutput = [ SHORT_OUTPUT ];
		await expect(async () => parseTransactionOutputs(onlyOneOutput, SHORT_LOCKSCRIPT_HEX, LONG_LOCKSCRIPT_HEX))
			.rejects.toThrow('Expected to find exactly one lockscript of each party but found Short:1, Long:0, Other:0');

		// No outputs
		const noOutputs: Output[] = [];
		await expect(async () => parseTransactionOutputs(noOutputs, SHORT_LOCKSCRIPT_HEX, LONG_LOCKSCRIPT_HEX))
			.rejects.toThrow('Expected to find exactly one lockscript of each party but found Short:0, Long:0, Other:0');
	});
});
