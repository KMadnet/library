import type { UseCase } from '../fixture/use-cases/index.js';
import { useCases } from '../fixture/use-cases/index.js';
import { AnyHedgeManager } from '../../lib/index.js';

// Create an instance of the contract manager.
const contractManager = new AnyHedgeManager();

const testContractCreate = async function(useCase: UseCase): Promise<void>
{
	// Create a new contract.
	const contractData = await contractManager.createContract(useCase.contract.createContract.input);

	// Verify that the contract data matches expectations.
	expect(contractData).toEqual(useCase.contract.createContract.output);
};

const testContractValidate = async function(useCase: UseCase): Promise<void>
{
	// Validate contract address.
	const contractValidity = await contractManager.validateContract(useCase.contract.validateContract.input);

	// Verify that the contract is valid.
	expect(contractValidity).toEqual(useCase.contract.validateContract.output);
};

const testContractSimulateLiquidation = async function(useCase: UseCase): Promise<void>
{
	// Simulate contract outcome.
	// @ts-ignore
	const simulationResults = await contractManager.calculateSettlementOutcome(...useCase.liquidation.calculateMaturationOutcome.input);

	// Verify that the simulation results matches expectations.
	expect(simulationResults).toEqual(useCase.liquidation.calculateMaturationOutcome.output);
};

const testContractSimulateMaturation = async function(useCase: UseCase): Promise<void>
{
	// Simulate contract outcome.
	// @ts-ignore
	const simulationResults = await contractManager.calculateSettlementOutcome(...useCase.maturation.calculateMaturationOutcome.input);

	// Verify that the simulation results matches expectations.
	expect(simulationResults).toEqual(useCase.maturation.calculateMaturationOutcome.output);
};

const testAddingContractFee = async function(useCase: UseCase): Promise<void>
{
	// Set up an empty contract structure with no fees.
	// NOTE: adding a fee does not depend on a contract to exist, it's merely metadata outside of the contract.
	const emptyContractWithNoFees = { fees: [] };

	// Add a fee to a non-existent contract.
	// NOTE: we are deliberately using an invalid contract structure here, and need to disable typescript checks.
	// @ts-ignore
	const contractDataWithFee = await contractManager.addContractFee(emptyContractWithNoFees, useCase.contract.addContractFee.input[1]);

	// Verify that the contract fee matches expectations.
	expect(contractDataWithFee).toEqual(useCase.contract.addContractFee.output);
};

const testAddingDuplicateContractFee = async function(useCase: UseCase): Promise<void>
{
	// Set up an empty contract structure with no fees.
	// NOTE: adding a fee does not depend on a contract to exist, it's merely metadata outside of the contract.
	const emptyContractWithNoFees = { fees: [] };

	// Add a fee to a non-existent contract.
	// NOTE: we are deliberately using an invalid contract structure here, and need to disable typescript checks.
	// @ts-ignore
	const contractDataWithFee = await contractManager.addContractFee(emptyContractWithNoFees, useCase.contract.addContractFee.input[1]);

	await expect(() => contractManager.addContractFee(contractDataWithFee, useCase.contract.addContractFee.input[1])).rejects.toThrow(/a fee for that name and address already exist/);
};

// Set up normal tests.
test('runNormalTests', async (): Promise<void> =>
{
	// For each use case to test..
	for(const useCaseIndex in useCases)
	{
		// .. assign it to the use case global reference.
		const useCase = useCases[useCaseIndex];

		// Test top-level non-stubbed library functions with the current use case.
		await testContractCreate(useCase);
		await testContractValidate(useCase);
		await testContractSimulateLiquidation(useCase);
		await testContractSimulateMaturation(useCase);
		await testAddingContractFee(useCase);
		await testAddingDuplicateContractFee(useCase);
	}
});
