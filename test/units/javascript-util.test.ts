import { bigIntAbs, bigIntMax, bigIntMin } from '../../lib/util/javascript-util.js';

describe('abs min max for bigint', () =>
{
	const KNOWN_BIGINTS =
	[
		BigInt('0'),
		BigInt('-1'),
		BigInt('-100'),
		BigInt('200'),
		BigInt('2'),
	];
	const EXPECTED_ABS_VALUES: Array<bigint> =
	[
		BigInt('0'),
		BigInt('1'),
		BigInt('100'),
		BigInt('200'),
		BigInt('2'),
	];
	const EXPECTED_MAX = BigInt('200');
	const EXPECTED_MIN = BigInt('-100');

	test('bigIntAbs returns correct absolute result for each test value', async () =>
	{
		// Convert all base values and compare to expected
		const results = KNOWN_BIGINTS.map((input: bigint) => bigIntAbs(input));
		expect(results).toEqual(EXPECTED_ABS_VALUES);
	});

	test('bigIntMax returns the max value', async () =>
	{
		// Get the result and confirm it matches expected
		const result = bigIntMax(...KNOWN_BIGINTS);
		expect(result).toEqual(EXPECTED_MAX);
	});

	test('bigIntMax should throw on an empty input', async () =>
	{
		// confirm empty input gets rejected
		await expect(async () => bigIntMax()).rejects.toThrow('max is undefined for an empty sequence');
	});

	test('bigIntMin returns the min value', async () =>
	{
		// Get the result and confirm it matches expected
		const result = bigIntMin(...KNOWN_BIGINTS);
		expect(result).toEqual(EXPECTED_MIN);
	});

	test('bigIntMin should throw on an empty input', async () =>
	{
		// confirm empty input gets rejected
		await expect(async () => bigIntMin()).rejects.toThrow('min is undefined for an empty sequence');
	});
});
