import { REDEEMSCRIPT_FOR_VALID_PAYOUT_TRANSACTION } from './anyhedge-util.test.js';
import { buildLockScriptP2SH32, addressToLockScript, lockScriptToAddress } from '../../lib/util/bitcoincash-util.js';

const KNOWN_REDEEMSCRIPT = REDEEMSCRIPT_FOR_VALID_PAYOUT_TRANSACTION;
const KNOWN_SCRIPTHASH = '67f79e099f7aa47b4414d91118c45bd59ac04b0b524aee33236e693645dbe607';
const KNOWN_LOCKSCRIPT = `aa20${KNOWN_SCRIPTHASH}87`;
const KNOWN_ADDRESS = 'bitcoincash:pdnl08sfnaa2g76yznv3zxxyt02e4sztpdfy4m3nydhxjdj9m0nqw99d45v3c';

describe('buildLockScriptP2SH32()', () =>
{
	test('builds the expected lockscript from given redeemscript', async () =>
	{
		// Get the result of the function and confirm match with known result
		const builtP2sh32ScriptHash = buildLockScriptP2SH32(KNOWN_REDEEMSCRIPT);
		expect(builtP2sh32ScriptHash).toEqual(KNOWN_LOCKSCRIPT);
	});
});

describe('addressToLockScript()', () =>
{
	test('builds the expected lockscript from given address', async () =>
	{
		// Get the result of the function and confirm match with known result
		const extractedLockScript = addressToLockScript(KNOWN_ADDRESS);
		expect(extractedLockScript).toEqual(KNOWN_LOCKSCRIPT);
	});
});

describe('lockScriptToAddress()', () =>
{
	test('builds the expected address from given lockscript', async () =>
	{
		// Get the result of the function and confirm match with known result
		const builtAddress = lockScriptToAddress(KNOWN_LOCKSCRIPT);
		expect(builtAddress).toEqual(KNOWN_ADDRESS);
	});
});
