// Allow line comments so that the ContractFixture definition is more readable
/* eslint-disable line-comment-position, no-inline-comments, @typescript-eslint/comma-dangle */

import { DUST_LIMIT } from '../../lib/constants.js';
import { START_TIMESTAMP, MATURITY_TIMESTAMP, DEFAULT_LIQUIDATION_TIMESTAMP } from './constants';
import { AuthenticationErrorCommon } from '@bitauth/libauth';

// Test cases in this file have been generated using emergent_reason's Python simulation tool and/or spreadsheet
// All cases are simple hedge (short leverage = 1) which is fine as their purpose is about maturity vs. liquidation.
// Short leverage is tested in the use-case fixtures.

export type ContractFixture =
[
	string, // description
	number, // nominalUnits
	bigint, // startPrice
	number, // volatilityProtection
	number, // maxPriceIncrease
	bigint, // messageTimestamp
	bigint, // settlementPrice
	bigint, // shortPayout
	bigint, // longPayout
	(string | true), // expectedResult
];

// These test cases test liquidations using some basic configurations
export const liquidateFixtures: ContractFixture[] =
[
	[ 'should not liquidate when message timestamp is before start timestamp',   100000, BigInt('22222'), 0.25, 10.0, START_TIMESTAMP - BigInt('10'), BigInt('16667'),            DUST_LIMIT, DUST_LIMIT, AuthenticationErrorCommon.failedVerify ],
	[ 'should not liquidate when oracle price equals start price',               100000, BigInt('22222'), 0.25, 10.0,  DEFAULT_LIQUIDATION_TIMESTAMP, BigInt('22222'),            DUST_LIMIT, DUST_LIMIT, AuthenticationErrorCommon.failedVerify ],
	[ 'should not liquidate when oracle price is in range (regular size)',       100000, BigInt('22222'), 0.25, 10.0,  DEFAULT_LIQUIDATION_TIMESTAMP, BigInt('16668'),            DUST_LIMIT, DUST_LIMIT, AuthenticationErrorCommon.failedVerify ],
	[ 'should not liquidate when oracle price is in range (small size)',            100, BigInt('22222'), 0.25, 10.0,  DEFAULT_LIQUIDATION_TIMESTAMP, BigInt('16668'),            DUST_LIMIT, DUST_LIMIT, AuthenticationErrorCommon.failedVerify ],
	[ 'should not liquidate when oracle price is in range (large size',        10000000, BigInt('22222'), 0.25, 10.0,  DEFAULT_LIQUIDATION_TIMESTAMP, BigInt('16668'),            DUST_LIMIT, DUST_LIMIT, AuthenticationErrorCommon.failedVerify ],
	[ 'should not liquidate when oracle price is in range (large protection)',   100000, BigInt('22222'), 0.95, 10.0,  DEFAULT_LIQUIDATION_TIMESTAMP,  BigInt('1112'),            DUST_LIMIT, DUST_LIMIT, AuthenticationErrorCommon.failedVerify ],
	[ 'should not liquidate when oracle price is in range (small protection)',   100000, BigInt('22222'), 0.05, 10.0,  DEFAULT_LIQUIDATION_TIMESTAMP, BigInt('21112'),            DUST_LIMIT, DUST_LIMIT, AuthenticationErrorCommon.failedVerify ],
	[ 'should liquidate when oracle price is below price (regular size)',        100000, BigInt('22222'), 0.25, 10.0,  DEFAULT_LIQUIDATION_TIMESTAMP, BigInt('16667'),   BigInt('599988000'), DUST_LIMIT, true ],
	[ 'should liquidate when oracle price is below price (small size)',             100, BigInt('22222'), 0.25, 10.0,  DEFAULT_LIQUIDATION_TIMESTAMP, BigInt('16667'),      BigInt('599988'), DUST_LIMIT, true ],
	[ 'should liquidate when oracle price is below price (large size)',        10000000, BigInt('22222'), 0.25, 10.0,  DEFAULT_LIQUIDATION_TIMESTAMP, BigInt('16667'), BigInt('59998800023'), DUST_LIMIT, true ],
	[ 'should liquidate when oracle price is below price (large protection)',    100000, BigInt('22222'), 0.95, 10.0,  DEFAULT_LIQUIDATION_TIMESTAMP,  BigInt('1111'),  BigInt('9000900090'), DUST_LIMIT, true ],
	[ 'should liquidate when oracle price is below price (small protection)',    100000, BigInt('22222'), 0.05, 10.0,  DEFAULT_LIQUIDATION_TIMESTAMP, BigInt('21111'),   BigInt('473686703'), DUST_LIMIT, true ],
];

// These test cases test maturation using some basic configurations
export const matureFixtures: ContractFixture[] =
[
	[ 'should not mature when message timestamp is before maturity timestamp', 100000, BigInt('22222'), 0.25, 10.0, MATURITY_TIMESTAMP - BigInt('10'), BigInt('22222'),          DUST_LIMIT,          DUST_LIMIT, AuthenticationErrorCommon.failedVerify ],
	[ 'should mature when oracle timestamp is at maturity timestamp',          100000, BigInt('22222'), 0.25, 10.0,                MATURITY_TIMESTAMP, BigInt('22222'), BigInt('450004500'), BigInt('149983500'), true ],
	[ 'should mature when oracle timestamp is after maturity timestamp',       100000, BigInt('22222'), 0.25, 10.0,  MATURITY_TIMESTAMP + BigInt('5'), BigInt('22222'), BigInt('450004500'), BigInt('149983500'), true ],
];
