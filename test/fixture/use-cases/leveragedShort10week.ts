import { UseCase } from './interfaces.js';
import { DUST_LIMIT, VM_1_AS_TRUE, VM_FALSE } from '../../../lib/constants.js';

const useCase: UseCase =
{
	// The data in this use case example represents a taker short, 1-week, 4x long, 1.111111111111x short, $10 nominal hedge.
	// The initial data was derived from an on-chain contract and then updated manually
	// at different times because of changes to the library and contract
	contract:
	{
		createContract:
		{
			input:
			{
				takerSide: 'short',
				makerSide: 'long',
				oraclePublicKey: '029174c105b4d7be73b0e25b3b204dfab054bd2c12f7d7e38a5de4f4d05decc58f',
				shortPayoutAddress: 'bitcoincash:qq59hv6s3qdjrtyfwfxxldkuj9xsjmx48vrz882knz',
				longPayoutAddress: 'bitcoincash:qpzlruwy4xu5rxjs3z37nsj29y7h59gwvsu4ddp0u4',
				nominalUnits: 1000,
				startingOracleMessage: 'db6409000100000001000000305c0000',
				startingOracleSignature: 'a8744a655a03107fb3b8e46eaee5519899960f258726d4fa6a74b6cbeb9a62efe96e012233cfbefc44378b820eb76bc4ef11e196ae5d47116ed9cbad93c6a818',
				maturityTimestamp: BigInt('6663643'),
				isSimpleHedge: VM_FALSE,
				highLiquidationPriceMultiplier: 10,
				lowLiquidationPriceMultiplier: 0.75,
				enableMutualRedemption: VM_1_AS_TRUE,
				shortMutualRedeemPublicKey: '020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309',
				longMutualRedeemPublicKey: '028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954',
			},
			output:
			{
				// TODO: This address is a simple output of the process. Need to do verification that the generation of it actually works with real transactions.
				address: 'bitcoincash:p0xwq6exddzxf44t9zs8enrrxgp6uc6cky3ks0gpu72647fjdlzru7wtshhvw',
				parameters:
				{
					highLiquidationPrice: BigInt('236000'),
					lowLiquidationPrice: BigInt('17700'),
					startTimestamp: BigInt('615643'),
					maturityTimestamp: BigInt('6663643'),
					oraclePublicKey: '029174c105b4d7be73b0e25b3b204dfab054bd2c12f7d7e38a5de4f4d05decc58f',
					shortLockScript: '76a914285bb350881b21ac89724c6fb6dc914d096cd53b88ac',
					longLockScript: '76a91445f1f1c4a9b9419a5088a3e9c24a293d7a150e6488ac',
					shortMutualRedeemPublicKey: '020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309',
					longMutualRedeemPublicKey: '028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954',
					nominalUnitsXSatsPerBch: BigInt('100000000000'),
					satsForNominalUnitsAtHighLiquidation: BigInt('423728'),
					payoutSats: BigInt('5225989'),
					enableMutualRedemption: VM_1_AS_TRUE,
				},
				metadata:
				{
					takerSide: 'short',
					makerSide: 'long',
					shortPayoutAddress: 'bitcoincash:qq59hv6s3qdjrtyfwfxxldkuj9xsjmx48vrz882knz',
					longPayoutAddress: 'bitcoincash:qpzlruwy4xu5rxjs3z37nsj29y7h59gwvsu4ddp0u4',
					durationInSeconds: BigInt('6048000'),
					highLiquidationPriceMultiplier: 10,
					lowLiquidationPriceMultiplier: 0.75,
					isSimpleHedge: VM_FALSE,
					startingOracleMessage: 'db6409000100000001000000305c0000',
					startingOracleSignature: 'a8744a655a03107fb3b8e46eaee5519899960f258726d4fa6a74b6cbeb9a62efe96e012233cfbefc44378b820eb76bc4ef11e196ae5d47116ed9cbad93c6a818',
					startPrice: BigInt('23600'),
					nominalUnits: 1000,
					shortInputInSatoshis: BigInt('3813560'),
					longInputInSatoshis: BigInt('1412429'),

					// Start and maturity timestamps are small (e.g. 615643) compared to the other contracts (e.g. 1620000000).
					// Other parameters may have varying sizes (such as true/false for booleans).
					// This causes different minimal-encoding to take place, and the contract size changes.
					// For this contract, the size is 635
					minerCostInSatoshis: BigInt('635'),
					shortInputInOracleUnits: 900.0001599999999,
					longInputInOracleUnits: 333.333244,
				},
				fees: [],
				fundings: [],
				version: 'AnyHedge v0.12',
			},
		},
		validateContract:
		{
			input:
			{
				contractAddress: 'bitcoincash:p0xwq6exddzxf44t9zs8enrrxgp6uc6cky3ks0gpu72647fjdlzru7wtshhvw',
				takerSide: 'short',
				makerSide: 'long',
				oraclePublicKey: '029174c105b4d7be73b0e25b3b204dfab054bd2c12f7d7e38a5de4f4d05decc58f',
				shortPayoutAddress: 'bitcoincash:qq59hv6s3qdjrtyfwfxxldkuj9xsjmx48vrz882knz',
				longPayoutAddress: 'bitcoincash:qpzlruwy4xu5rxjs3z37nsj29y7h59gwvsu4ddp0u4',
				nominalUnits: 1000,
				startingOracleMessage: 'db6409000100000001000000305c0000',
				startingOracleSignature: 'a8744a655a03107fb3b8e46eaee5519899960f258726d4fa6a74b6cbeb9a62efe96e012233cfbefc44378b820eb76bc4ef11e196ae5d47116ed9cbad93c6a818',
				maturityTimestamp: BigInt('6663643'),
				isSimpleHedge: VM_FALSE,
				highLiquidationPriceMultiplier: 10,
				lowLiquidationPriceMultiplier: 0.75,
				enableMutualRedemption: VM_1_AS_TRUE,
				shortMutualRedeemPublicKey: '020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309',
				longMutualRedeemPublicKey: '028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954',
			},
			output: true,
		},
		addContractFee:
		{
			input:
			[
				// NOTE: adding a contract fee does not require the full contract data structure, but does require a list for the fees.
				{ fees: [] },
				{
					name: 'test',
					description: 'description of test a fee',
					address: 'bitcoincash:qraaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
					satoshis: BigInt('10000'),
				},
			],
			output:
			{
				fees:
				[
					{
						name: 'test',
						description: 'description of test a fee',
						address: 'bitcoincash:qraaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
						satoshis: BigInt('10000'),
					},
				],
			},
		},
	},
	liquidation:
	{
		calculateMaturationOutcome:
		{
			input:
			[
				{
					highLiquidationPrice: BigInt('236000'),
					lowLiquidationPrice: BigInt('17700'),
					startTimestamp: BigInt('615643'),
					maturityTimestamp: BigInt('6663643'),
					oraclePublicKey: '029174c105b4d7be73b0e25b3b204dfab054bd2c12f7d7e38a5de4f4d05decc58f',
					shortLockScript: '76a914285bb350881b21ac89724c6fb6dc914d096cd53b88ac',
					longLockScript: '76a91445f1f1c4a9b9419a5088a3e9c24a293d7a150e6488ac',
					shortMutualRedeemPublicKey: '020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309',
					longMutualRedeemPublicKey: '028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954',
					nominalUnitsXSatsPerBch: BigInt('100000000000'),
					satsForNominalUnitsAtHighLiquidation: BigInt('423728'),
					payoutSats: BigInt('5225989'),
				},
				// total funding sats
				BigInt('5227321'),
				// oracle price
				BigInt('17500'),
			],
			output:
			{
				shortPayoutSatsSafe: BigInt('5225989'),
				longPayoutSatsSafe: DUST_LIMIT,
				totalPayoutSatsSafe: BigInt('5227321'),
				satsForNominalUnits: BigInt('5649717'),
				minerFeeSats: BigInt('0'),
			},
		},
	},
	maturation:
	{
		calculateMaturationOutcome:
		{
			input:
			[
				{
					highLiquidationPrice: BigInt('236000'),
					lowLiquidationPrice: BigInt('17700'),
					startTimestamp: BigInt('615643'),
					maturityTimestamp: BigInt('6663643'),
					oraclePublicKey: '029174c105b4d7be73b0e25b3b204dfab054bd2c12f7d7e38a5de4f4d05decc58f',
					shortLockScript: '76a914285bb350881b21ac89724c6fb6dc914d096cd53b88ac',
					longLockScript: '76a91445f1f1c4a9b9419a5088a3e9c24a293d7a150e6488ac',
					shortMutualRedeemPublicKey: '020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309',
					longMutualRedeemPublicKey: '028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954',
					nominalUnitsXSatsPerBch: BigInt('100000000000'),
					satsForNominalUnitsAtHighLiquidation: BigInt('423728'),
					payoutSats: BigInt('5225989'),
				},
				// total funding sats
				BigInt('5227321'),
				// oracle price
				BigInt('23500'),
			],
			output:
			{
				shortPayoutSatsSafe: BigInt('3831591'),
				longPayoutSatsSafe: BigInt('1394398'),
				totalPayoutSatsSafe: BigInt('5225989'),
				satsForNominalUnits: BigInt('4255319'),
				minerFeeSats: BigInt('1332'),
			},
		},
	},
};

export default useCase;
