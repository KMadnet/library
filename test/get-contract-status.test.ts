/* eslint-disable max-nested-callbacks */
/* eslint-disable no-param-reassign */
/* eslint-disable import/first */

// Fetch mocks need to be enabled before all other imports
import fetchMock from 'jest-fetch-mock';

fetchMock.enableMocks();

// Import testing libraries and utilities
import { loadDefaultContractData } from './test-util.js';

// Import AnyHedge library
import type { ContractCreationParameters, ContractData } from '../lib/index.js';
import { VM_1_AS_TRUE } from '../lib/constants.js';
import { AnyHedgeManager } from '../lib/index.js';

// Load support for bigint in JSON
import { JSONStringify } from '../lib/util/json-explicit-bigint.js';

describe('getContractStatus()', () =>
{
	let manager: AnyHedgeManager;

	beforeEach(async () =>
	{
		// Reset any 'fetch' mocks
		(fetch as any).resetMocks();

		// Load a contract manager
		manager = new AnyHedgeManager({ authenticationToken: 'DUMMY' });
	});

	test.skip('should fail when the settlement service responds with data for a different address', async () =>
	{
		// Load default contract data
		const contractData = await loadDefaultContractData(manager);

		// Change the contract data very slightly, resulting in a different address
		// TODO: Was this a bug? Duration is not used in contract creation. Is this test actually testing the code?
		const contractCreationParameters: ContractCreationParameters =
		{
			oraclePublicKey: contractData.parameters.oraclePublicKey,
			maturityTimestamp: contractData.parameters.maturityTimestamp + BigInt('10'),
			enableMutualRedemption: contractData.parameters.enableMutualRedemption,
			shortMutualRedeemPublicKey: contractData.parameters.shortMutualRedeemPublicKey,
			longMutualRedeemPublicKey: contractData.parameters.longMutualRedeemPublicKey,
			takerSide: contractData.metadata.takerSide,
			makerSide: contractData.metadata.makerSide,
			nominalUnits: contractData.metadata.nominalUnits,
			startingOracleMessage: contractData.metadata.startingOracleMessage,
			startingOracleSignature: contractData.metadata.startingOracleSignature,
			lowLiquidationPriceMultiplier: contractData.metadata.lowLiquidationPriceMultiplier,
			highLiquidationPriceMultiplier: contractData.metadata.highLiquidationPriceMultiplier,
			isSimpleHedge: VM_1_AS_TRUE,
			shortPayoutAddress: contractData.metadata.shortPayoutAddress,
			longPayoutAddress: contractData.metadata.longPayoutAddress,
		};
		const contractDataForDifferentAddress = await manager.createContract(contractCreationParameters);

		// Make the settlement service respond with contract data for that address
		(fetch as any).mockResponseOnce(JSONStringify(contractDataForDifferentAddress));

		// Attempt to get contract data
		const getContractStatusCall = manager.getContractStatus(contractData.address);

		// Expect the function call to fail and report that the data is for a different contract
		await expect(getContractStatusCall).rejects.toThrow(/Received contract data for contract .+ while requesting contract data for contract/);
	});

	test.skip('should fail when settlement service responds with invalid contract metadata', async () =>
	{
		// Load default contract data
		const contractData = await loadDefaultContractData(manager);

		// Change the contract metadata very slightly, without changing the address, meaning that
		// the contract data is invalid for the linked address
		const differentContractData: ContractData =
		{
			...contractData,
			metadata:
			{
				...contractData.metadata,
				durationInSeconds: contractData.metadata.durationInSeconds + BigInt('10'),
			},
		};

		// Make the settlement service respond with that invalid contract data
		(fetch as any).mockResponseOnce(JSONStringify(differentContractData));

		// Attempt to get contract data
		const getContractStatusCall = manager.getContractStatus(contractData.address);

		// Expect the function call to fail and report that the data is invalid
		await expect(getContractStatusCall).rejects.toThrow(/Invalid contract data received for contract/);
	});

	test.skip('should fail when settlement service responds with invalid contract parameters', async () =>
	{
		// Load default contract data
		const contractData = await loadDefaultContractData(manager);

		// Change the contract parameters very slightly, without changing the address, meaning that
		// the contract data is invalid for the linked address
		const differentContractData: ContractData =
		{
			...contractData,
			parameters:
			{
				...contractData.parameters,
				maturityTimestamp: contractData.parameters.maturityTimestamp + BigInt('10'),
			},
		};

		// Make the settlement service respond with that invalid contract data
		(fetch as any).mockResponseOnce(JSONStringify(differentContractData));

		// Attempt to get contract data
		const getContractStatusCall = manager.getContractStatus(contractData.address);

		// Expect the function call to fail and report that the data is invalid
		await expect(getContractStatusCall).rejects.toThrow(/Invalid contract data received for contract/);
	});

	test.skip('should succeed', async () =>
	{
		// Load default contract data
		const contractData = await loadDefaultContractData(manager);

		// Make the settlement service respond with that contract data
		(fetch as any).mockResponseOnce(JSONStringify(contractData));

		// Get contract data
		const returnedData = await manager.getContractStatus(contractData.address);

		// Expect the returned contract data to match the stored contract data
		expect(returnedData).toEqual(contractData);
	});
});
