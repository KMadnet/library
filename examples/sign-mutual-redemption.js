// Load the AnyHedge library.
import { AnyHedgeManager } from '../build/lib/anyhedge.js';

// Load support for bigint in JSON
import { JSONStringify } from '../build/lib/util/json-explicit-bigint.js';

// Provide the following variables to use this script.

// Set which settlement service to use in this example.
// DEFAULT: https and api.anyhedge.com
const SETTLEMENT_SERVICE_SCHEME = 'https';
const SETTLEMENT_SERVICE_DOMAIN = 'api.anyhedge.com';
const SETTLEMENT_SERVICE_PORT = 443;

// Authentication token to connect to the AnyHedge settlement service.
const AUTHENTICATION_TOKEN = '';

// Contract address for the contract you want to mutually redeem.
const CONTRACT_ADDRESS = '';

// Private key for one of the contract parties.
const PRIVATE_KEY_WIF = '';

// If this is set to a price, the contract will be mutually settled/matured at that price,
// if it is set to undefined, instead the original input satoshi values will be refunded.
// You can use one of the lines below:
// const SETTLEMENT_PRICE = BigInt('23352'); // i.e. $233.52 USD/BCH
const SETTLEMENT_PRICE = undefined; // i.e. refund original inputs

/**
 * Mutually redeem a contract that is registered in the settlement services. If a settlement price
 * is included, a "mutual early maturation" will be signed, if it is omitted a "refund" will be signed.
 * Note that the private key WIF must be the mutual redemption key for one of the two contract parties.
 *
 * @param {string} contractAddress     contract address to mutually redeem.
 * @param {string} privateKeyWIF       private key of one of the contract parties.
 * @param {bigint | undefined} [settlementPrice]   price to use in settlement.
 */
const signMutualRedemption = async function(contractAddress, privateKeyWIF, settlementPrice)
{
	try
	{
		// Load contract manager.
		const manager = new AnyHedgeManager({ serviceDomain: SETTLEMENT_SERVICE_DOMAIN, serviceScheme: SETTLEMENT_SERVICE_SCHEME, servicePort: SETTLEMENT_SERVICE_PORT, authenticationToken: AUTHENTICATION_TOKEN });

		// Retrieve contract data for the contract address.
		const contractData = await manager.getContractStatus(contractAddress, privateKeyWIF);

		// Extract the parameters and metadata for legibility.
		const { parameters: contractParameters } = contractData;
		const { metadata: contractMetadata } = contractData;

		// Take the first contract funding in the list.
		const contractFunding = contractData.fundings[0];

		// Throw an error if no contract funding was found.
		if(typeof contractFunding === 'undefined')
		{
			throw(new Error(`No funding found for contract address ${contractAddress}`));
		}

		let proposal;

		if(typeof settlementPrice !== 'undefined')
		{
			// Perform a mutual early maturation if a settlement price was provided.
			proposal = await manager.signMutualEarlyMaturation({ privateKeyWIF, contractFunding, settlementPrice, contractParameters, contractMetadata });
		}
		else
		{
			// Perform a refund if no settlement price was provided.
			proposal = await manager.signMutualRefund({ privateKeyWIF, contractFunding, contractParameters, contractMetadata });
		}

		// Log the results to the console.
		console.log('Successfully signed proposal, please share this proposal with your counterparty');
		console.log();
		console.log(JSONStringify(proposal, null, 2));
	}
	catch(error)
	{
		// Log the error to the console and exit.
		console.error(error.message);
		process.exit(1);
	}
};

await signMutualRedemption(CONTRACT_ADDRESS, PRIVATE_KEY_WIF, SETTLEMENT_PRICE);
