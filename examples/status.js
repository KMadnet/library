// Load the AnyHedge library.
import { AnyHedgeManager } from '../build/lib/index.js';

// Contract address for the contract you want to get status for.
const CONTRACT_ADDRESS = '';

// Set which settlement service to use in this example.
// DEFAULT: https and api.anyhedge.com
const SETTLEMENT_SERVICE_SCHEME = 'https';
const SETTLEMENT_SERVICE_DOMAIN = 'staging-api.anyhedge.com';
const SETTLEMENT_SERVICE_PORT   = 443;

// To use the automated redemption service, you need to request an authentication token from the service provider.
// Request a token once by running the following command in the terminal:
// curl -d 'name=<Your Name Here>' "https://api.anyhedge.com/api/v2/requestToken"
const AUTHENTICATION_TOKEN = '';

// Create an instance of the AnyHedge manager using the authentication token.
const anyHedgeManager = new AnyHedgeManager({ serviceDomain: SETTLEMENT_SERVICE_DOMAIN, serviceScheme: SETTLEMENT_SERVICE_SCHEME, servicePort: SETTLEMENT_SERVICE_PORT, authenticationToken: AUTHENTICATION_TOKEN });

// Wrap the example code in an async function to allow async/await.
const example = async function()
{
	const contractData = await anyHedgeManager.getContractStatus(CONTRACT_ADDRESS);

	// Output the contract status to the console
	console.log('contract data:');
	console.log(contractData);

	// Settlement details, if any:
	const settlementData = contractData.fundings[0]?.settlement;
	if(settlementData)
	{
		console.log('contract data settlement details:');
		console.log(settlementData);
	}
};

// Run the example code.
await example();
