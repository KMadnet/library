// Load the AnyHedge library.
import { AnyHedgeManager } from '../build/lib/anyhedge.js';

// Load support for bigint in JSON
import { JSONParse } from '../build/lib/util/json-explicit-bigint.js';

// Provide the following variables to use this script.

// Set which settlement service to use in this example.
// DEFAULT: https and api.anyhedge.com
const SETTLEMENT_SERVICE_SCHEME = 'https';
const SETTLEMENT_SERVICE_DOMAIN = 'api.anyhedge.com';
const SETTLEMENT_SERVICE_PORT = 443;

// Authentication token to connect to the AnyHedge settlement service.
const AUTHENTICATION_TOKEN = '';

// Contract address for the contract you want to mutually redeem.
const CONTRACT_ADDRESS = '';

// Private key for one of the contract parties.
const PRIVATE_KEY_WIF = '';

// Paste the proposal you received from your counterparty inside the single quotes like this: '{"inputs":[{....'
const COUNTERPARTY_PROPOSAL_JSON = '';

// Paste the proposal that you generated yourself inside the single quotes like this: '{"inputs":[{....'
const OWN_PROPOSAL_JSON = '';


/**
 * Complete a mutual redemption by combining the generated proposals from both parties.
 *
 * @param {string} token             authentication token to connect to the settlement service.
 * @param {string} contractAddress   contract address to mutually redeem.
 * @param {string} privateKeyWIF     private key of one of the contract parties.
 * @param {object} proposal1         one of the generated proposals.
 * @param {object} proposal2         the other of the generated proposals.
 */
const completeMutualRedemption = async function(token, contractAddress, privateKeyWIF, proposal1, proposal2)
{
	try
	{
		// Load contract manager.
		const manager = new AnyHedgeManager({ serviceDomain: SETTLEMENT_SERVICE_DOMAIN, serviceScheme: SETTLEMENT_SERVICE_SCHEME, servicePort: SETTLEMENT_SERVICE_PORT, authenticationToken: AUTHENTICATION_TOKEN });

		// Retrieve contract data for the contract address.
		const contractData = await manager.getContractStatus(contractAddress, privateKeyWIF);

		// Complete mutual redemption.
		const transactionId = await manager.completeMutualRedemption(proposal1, proposal2, contractData.parameters);

		// Log the results to the console.
		console.log('Successfully completed mutual redemption with this transaction:');
		console.log();
		console.log(transactionId);
	}
	catch(error)
	{
		// Log the error to the console and exit.
		console.error(error.message);
		process.exit(1);
	}
};

const counterpartyProposal = JSONParse(COUNTERPARTY_PROPOSAL_JSON);
const ownProposal = JSONParse(OWN_PROPOSAL_JSON);
await completeMutualRedemption(AUTHENTICATION_TOKEN, CONTRACT_ADDRESS, PRIVATE_KEY_WIF, counterpartyProposal, ownProposal);
